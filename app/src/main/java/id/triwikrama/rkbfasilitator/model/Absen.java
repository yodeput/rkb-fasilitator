package id.triwikrama.rkbfasilitator.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Absen implements Parcelable, Serializable {
    @Expose
    @SerializedName("absen_date")
    private String absen_date;
    @Expose
    @SerializedName("absen_lng")
    private String absen_lng;
    @Expose
    @SerializedName("absen_lat")
    private String absen_lat;
    @Expose
    @SerializedName("absen_photo")
    private String absen_photo;
    @Expose
    @SerializedName("rkb_name")
    private String rkb_name;
    @Expose
    @SerializedName("user_name")
    private String user_name;
    @Expose
    @SerializedName("user_id")
    private String user_id;
    public boolean progress = false;



    public Absen(boolean progress) {
        this.progress = progress;
    }

    public String getAbsen_date() {
        return absen_date;
    }

    public void setAbsen_date(String absen_date) {
        this.absen_date = absen_date;
    }

    public String getAbsen_lng() {
        return absen_lng;
    }

    public void setAbsen_lng(String absen_lng) {
        this.absen_lng = absen_lng;
    }

    public String getAbsen_lat() {
        return absen_lat;
    }

    public void setAbsen_lat(String absen_lat) {
        this.absen_lat = absen_lat;
    }

    public String getAbsen_photo() {
        return absen_photo;
    }

    public void setAbsen_photo(String absen_photo) {
        this.absen_photo = absen_photo;
    }

    public String getRkb_name() {
        return rkb_name;
    }

    public void setRkb_name(String rkb_name) {
        this.rkb_name = rkb_name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.absen_date);
        dest.writeString(this.absen_lng);
        dest.writeString(this.absen_lat);
        dest.writeString(this.absen_photo);
        dest.writeString(this.rkb_name);
        dest.writeString(this.user_name);
        dest.writeString(this.user_id);
        dest.writeByte(this.progress ? (byte) 1 : (byte) 0);
    }

    protected Absen(Parcel in) {
        this.absen_date = in.readString();
        this.absen_lng = in.readString();
        this.absen_lat = in.readString();
        this.absen_photo = in.readString();
        this.rkb_name = in.readString();
        this.user_name = in.readString();
        this.user_id = in.readString();
        this.progress = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Absen> CREATOR = new Parcelable.Creator<Absen>() {
        @Override
        public Absen createFromParcel(Parcel source) {
            return new Absen(source);
        }

        @Override
        public Absen[] newArray(int size) {
            return new Absen[size];
        }
    };
}
