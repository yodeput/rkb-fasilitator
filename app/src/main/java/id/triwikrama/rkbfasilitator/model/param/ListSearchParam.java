package id.triwikrama.rkbfasilitator.model.param;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListSearchParam {
    @Expose
    @SerializedName("search")
    private String search;
    @Expose
    @SerializedName("type")
    private String type;
    @Expose
    @SerializedName("page")
    private String page;
    @Expose
    @SerializedName("ke_tanggal")
    private String ke_tanggal;
    @Expose
    @SerializedName("dari_tanggal")
    private String dari_tanggal;
    @Expose
    @SerializedName("jenis_kegiatan")
    private String jenis_kegiatan;
    @Expose
    @SerializedName("nama_rkb")
    private String nama_rkb;
    @Expose
    @SerializedName("kelas")
    private String kelas;
    @Expose
    @SerializedName("tr")
    private String tr;


    public ListSearchParam() {
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getTr() {
        return tr;
    }

    public void setTr(String tr) {
        this.tr = tr;
    }

    public String getKe_tanggal() {
        return ke_tanggal;
    }

    public void setKe_tanggal(String ke_tanggal) {
        this.ke_tanggal = ke_tanggal;
    }

    public String getDari_tanggal() {
        return dari_tanggal;
    }

    public void setDari_tanggal(String dari_tanggal) {
        this.dari_tanggal = dari_tanggal;
    }

    public String getJenis_kegiatan() {
        return jenis_kegiatan;
    }

    public void setJenis_kegiatan(String jenis_kegiatan) {
        this.jenis_kegiatan = jenis_kegiatan;
    }

    public String getNama_rkb() {
        return nama_rkb;
    }

    public void setNama_rkb(String nama_rkb) {
        this.nama_rkb = nama_rkb;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}
