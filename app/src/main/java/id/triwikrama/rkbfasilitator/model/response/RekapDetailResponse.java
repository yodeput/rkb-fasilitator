package id.triwikrama.rkbfasilitator.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import id.triwikrama.rkbfasilitator.model.RekapDetail;

public class RekapDetailResponse implements Serializable {

    @Expose
    @SerializedName("success")
    private int success;
    @Expose
    @SerializedName("total_kegiatan")
    private String total_kegiatan;
    @Expose
    @SerializedName("rkb_name")
    private String rkb_name;
    @Expose
    @SerializedName("data")
    private List<RekapDetail> data;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getTotal_kegiatan() {
        return total_kegiatan;
    }

    public void setTotal_kegiatan(String total_kegiatan) {
        this.total_kegiatan = total_kegiatan;
    }

    public String getRkb_name() {
        return rkb_name;
    }

    public void setRkb_name(String rkb_name) {
        this.rkb_name = rkb_name;
    }

    public List<RekapDetail> getData() {
        return data;
    }

    public void setData(List<RekapDetail> data) {
        this.data = data;
    }
}
