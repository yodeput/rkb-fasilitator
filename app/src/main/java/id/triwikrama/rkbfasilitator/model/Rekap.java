package id.triwikrama.rkbfasilitator.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Rekap implements Serializable {
    @SerializedName("total_kegiatan")
    private String totalKegiatan;
    @SerializedName("total_adm")
    private String totalAdm;
    @SerializedName("total_event")
    private String totalEvent;
    @SerializedName("total_pelatihan")
    private String totalPelatihan;
    @SerializedName("total_kunjungan")
    private String totalKunjungan;
    @SerializedName("rkb_name")
    private String rkbName;
    @SerializedName("rkb_id")
    private String rkbId;
    @SerializedName("gmv")
    private String gmv;

    public boolean progress = false;

    public Rekap() {
    }

    public Rekap(boolean progress) {
        this.progress = progress;
    }

    public Rekap(String totalKegiatan, String totalAdm, String totalEvent, String totalPelatihan, String totalKunjungan, String rkbName, String rkbId, String gmv) {
        this.totalKegiatan = totalKegiatan;
        this.totalAdm = totalAdm;
        this.totalEvent = totalEvent;
        this.totalPelatihan = totalPelatihan;
        this.totalKunjungan = totalKunjungan;
        this.rkbName = rkbName;
        this.rkbId = rkbId;
        this.gmv = gmv;
    }

    public String getGmv() {
        return gmv;
    }

    public void setGmv(String gmv) {
        this.gmv = gmv;
    }

    public String getTotalKegiatan() {
        return totalKegiatan;
    }

    public void setTotalKegiatan(String totalKegiatan) {
        this.totalKegiatan = totalKegiatan;
    }

    public String getTotalAdm() {
        return totalAdm;
    }

    public void setTotalAdm(String totalAdm) {
        this.totalAdm = totalAdm;
    }

    public String getTotalEvent() {
        return totalEvent;
    }

    public void setTotalEvent(String totalEvent) {
        this.totalEvent = totalEvent;
    }

    public String getTotalPelatihan() {
        return totalPelatihan;
    }

    public void setTotalPelatihan(String totalPelatihan) {
        this.totalPelatihan = totalPelatihan;
    }

    public String getTotalKunjungan() {
        return totalKunjungan;
    }

    public void setTotalKunjungan(String totalKunjungan) {
        this.totalKunjungan = totalKunjungan;
    }

    public String getRkbName() {
        return rkbName;
    }

    public void setRkbName(String rkbName) {
        this.rkbName = rkbName;
    }

    public String getRkbId() {
        return rkbId;
    }

    public void setRkbId(String rkbId) {
        this.rkbId = rkbId;
    }
}
