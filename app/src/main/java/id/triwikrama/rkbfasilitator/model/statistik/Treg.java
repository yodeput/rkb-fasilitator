package id.triwikrama.rkbfasilitator.model.statistik;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Treg {

    @Expose
    @SerializedName("data")
    private List<Perbulan> data;
    @Expose
    @SerializedName("tr")
    private String tr;

    public List<Perbulan> getData() {
        return data;
    }

    public void setData(List<Perbulan> data) {
        this.data = data;
    }

    public String getTr() {
        return tr;
    }

    public void setTr(String tr) {
        this.tr = tr;
    }
}
