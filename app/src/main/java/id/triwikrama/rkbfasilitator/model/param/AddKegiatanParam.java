package id.triwikrama.rkbfasilitator.model.param;

import java.util.List;

import id.triwikrama.rkbfasilitator.model.Photo;

public class AddKegiatanParam {
    private String type;
    private String rkb_id;
    private String user_id;
    private String name;
    private String desc;
    private String lokasi;
    private String lat;
    private String lng;
    private List<Photo> photo;
    private String ticket;
    private String jml_peserta;
    private String instansi;

    public AddKegiatanParam() {
    }

    public AddKegiatanParam(String type, String rkb_id, String user_id, String name, String desc, String lokasi, String lat, String lng, List<Photo> photo, String ticket, String jml_peserta, String instansi) {
        this.type = type;
        this.rkb_id = rkb_id;
        this.user_id = user_id;
        this.name = name;
        this.desc = desc;
        this.lokasi = lokasi;
        this.lat = lat;
        this.lng = lng;
        this.photo = photo;
        this.ticket = ticket;
        this.jml_peserta = jml_peserta;
        this.instansi = instansi;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getJml_peserta() {
        return jml_peserta;
    }

    public void setJml_peserta(String jml_peserta) {
        this.jml_peserta = jml_peserta;
    }

    public String getInstansi() {
        return instansi;
    }

    public void setInstansi(String instansi) {
        this.instansi = instansi;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRkb_id() {
        return rkb_id;
    }

    public void setRkb_id(String rkb_id) {
        this.rkb_id = rkb_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public List<Photo> getPhoto() {
        return photo;
    }

    public void setPhoto(List<Photo> photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "AddKegiatanParam{" +
                "type='" + type + '\'' +
                ", rkb_id='" + rkb_id + '\'' +
                ", user_id='" + user_id + '\'' +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", lokasi='" + lokasi + '\'' +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                ", photo=" + photo +
                '}';
    }
}
