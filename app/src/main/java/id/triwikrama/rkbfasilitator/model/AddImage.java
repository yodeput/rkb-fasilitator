package id.triwikrama.rkbfasilitator.model;

import com.esafirm.imagepicker.model.Image;

public class AddImage {
    Image image;

    public AddImage(){

    }

    public Image getPath() {
        return image;
    }

    public void setPath(Image image) {
        this.image = image;
    }


}
