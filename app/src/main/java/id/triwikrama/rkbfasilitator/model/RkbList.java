package id.triwikrama.rkbfasilitator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RkbList implements Serializable {
    @SerializedName("rkb_kelas")
    private String rkb_kelas;
    @SerializedName("rkb_treg")
    private String rkb_treg;
    @SerializedName("rkb_name")
    private String rkb_name;
    @SerializedName("rkb_id")
    private String rkb_id;
    @SerializedName("rkb_gmv")
    private String rkb_gmv;
    @SerializedName("user")
    private List<Username> user;

    public boolean progress = false;

    public RkbList() {
    }

    public RkbList(boolean progress) {
        this.progress = progress;
    }

    public RkbList(String rkb_kelas, String rkb_treg, String rkb_name, String rkb_id, String rkb_gmv, List<Username> user) {
        this.rkb_kelas = rkb_kelas;
        this.rkb_treg = rkb_treg;
        this.rkb_name = rkb_name;
        this.rkb_id = rkb_id;
        this.rkb_gmv = rkb_gmv;
        this.user = user;
    }

    public String getRkb_kelas() {
        return rkb_kelas;
    }

    public void setRkb_kelas(String rkb_kelas) {
        this.rkb_kelas = rkb_kelas;
    }

    public String getRkb_treg() {
        return rkb_treg;
    }

    public void setRkb_treg(String rkb_treg) {
        this.rkb_treg = rkb_treg;
    }

    public String getRkb_name() {
        return rkb_name;
    }

    public void setRkb_name(String rkb_name) {
        this.rkb_name = rkb_name;
    }

    public String getRkb_id() {
        return rkb_id;
    }

    public void setRkb_id(String rkb_id) {
        this.rkb_id = rkb_id;
    }

    public List<Username> getUser() {
        return user;
    }

    public void setUser(List<Username> user) {
        this.user = user;
    }

    public String getRkb_gmv() {
        return rkb_gmv;
    }

    public void setRkb_gmv(String rkb_gmv) {
        this.rkb_gmv = rkb_gmv;
    }
}
