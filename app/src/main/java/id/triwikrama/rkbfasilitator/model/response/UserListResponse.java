package id.triwikrama.rkbfasilitator.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.triwikrama.rkbfasilitator.model.UserList;

public class UserListResponse {

    @Expose
    @SerializedName("success")
    private int success;
    @Expose
    @SerializedName("jumlah")
    private int jumlah;
    @Expose
    @SerializedName("data")
    private List<UserList> data;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public List<UserList> getData() {
        return data;
    }

    public void setData(List<UserList> data) {
        this.data = data;
    }
}
