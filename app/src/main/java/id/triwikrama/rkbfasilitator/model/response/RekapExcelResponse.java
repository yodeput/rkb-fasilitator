package id.triwikrama.rkbfasilitator.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.triwikrama.rkbfasilitator.model.RekapExcel;

public class RekapExcelResponse {


    @Expose
    @SerializedName("success")
    private int success;
    @Expose
    @SerializedName("data")
    private List<RekapExcel> data;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public List<RekapExcel> getData() {
        return data;
    }

    public void setData(List<RekapExcel> data) {
        this.data = data;
    }
}
