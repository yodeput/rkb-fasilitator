package id.triwikrama.rkbfasilitator.model.statistik;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Jenis {
    @Expose
    @SerializedName("administrasi")
    private String administrasi;
    @Expose
    @SerializedName("event")
    private String event;
    @Expose
    @SerializedName("pelatihan")
    private String pelatihan;
    @Expose
    @SerializedName("kunjungan")
    private String kunjungan;

    public String getAdministrasi() {
        return administrasi;
    }

    public void setAdministrasi(String administrasi) {
        this.administrasi = administrasi;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getPelatihan() {
        return pelatihan;
    }

    public void setPelatihan(String pelatihan) {
        this.pelatihan = pelatihan;
    }

    public String getKunjungan() {
        return kunjungan;
    }

    public void setKunjungan(String kunjungan) {
        this.kunjungan = kunjungan;
    }
}
