package id.triwikrama.rkbfasilitator.model.param;

import com.google.gson.annotations.SerializedName;

public class ProfileParam {
    private String user_id;
    private String bulan;
    private String tahun;
    private String kelas;




    public ProfileParam() {
    }

    public ProfileParam(String user_id) {
        this.user_id = user_id;
    }

    public ProfileParam(String user_id, String bulan, String tahun) {
        this.user_id = user_id;
        this.bulan = bulan;
        this.tahun = tahun;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getBulan() {
        return bulan;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

}
