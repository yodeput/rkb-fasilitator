package id.triwikrama.rkbfasilitator.model;

import android.widget.ScrollView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserList implements Serializable {
    @Expose
    @SerializedName("user_status")
    private String user_status;
    @Expose
    @SerializedName("user_type")
    private String user_type;
    @Expose
    @SerializedName("user_email")
    private String user_email;
    @Expose
    @SerializedName("user_alamat")
    private String user_alamat;
    @Expose
    @SerializedName("user_notelp")
    private String user_notelp;
    @Expose
    @SerializedName("user_tanggallahir")
    private String user_tanggallahir;
    @Expose
    @SerializedName("user_tempatlahir")
    private String user_tempatlahir;
    @Expose
    @SerializedName("user_tingkat")
    private String user_tingkat;
    @Expose
    @SerializedName("user_pendidikan")
    private String user_pendidikan;
    @Expose
    @SerializedName("user_name")
    private String user_name;
    @Expose
    @SerializedName("user_id")
    private String user_id;
    @Expose
    @SerializedName("rkb_name")
    private String rkb_name;


    public boolean progress = false;


    public UserList() {
    }

    public UserList(boolean progress) {
        this.progress = progress;
    }

    public UserList(String user_status, String user_type, String user_email, String user_alamat, String user_notelp, String user_tanggallahir, String user_tempatlahir, String user_tingkat, String user_pendidikan, String user_name, String user_id) {
        this.user_status = user_status;
        this.user_type = user_type;
        this.user_email = user_email;
        this.user_alamat = user_alamat;
        this.user_notelp = user_notelp;
        this.user_tanggallahir = user_tanggallahir;
        this.user_tempatlahir = user_tempatlahir;
        this.user_tingkat = user_tingkat;
        this.user_pendidikan = user_pendidikan;
        this.user_name = user_name;
        this.user_id = user_id;
    }


    public String getRkb_name() {
        return rkb_name;
    }

    public void setRkb_name(String rkb_name) {
        this.rkb_name = rkb_name;
    }

    public String getUser_status() {
        return user_status;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_alamat() {
        return user_alamat;
    }

    public void setUser_alamat(String user_alamat) {
        this.user_alamat = user_alamat;
    }

    public String getUser_notelp() {
        return user_notelp;
    }

    public void setUser_notelp(String user_notelp) {
        this.user_notelp = user_notelp;
    }

    public String getUser_tanggallahir() {
        return user_tanggallahir;
    }

    public void setUser_tanggallahir(String user_tanggallahir) {
        this.user_tanggallahir = user_tanggallahir;
    }

    public String getUser_tempatlahir() {
        return user_tempatlahir;
    }

    public void setUser_tempatlahir(String user_tempatlahir) {
        this.user_tempatlahir = user_tempatlahir;
    }

    public String getUser_tingkat() {
        return user_tingkat;
    }

    public void setUser_tingkat(String user_tingkat) {
        this.user_tingkat = user_tingkat;
    }

    public String getUser_pendidikan() {
        return user_pendidikan;
    }

    public void setUser_pendidikan(String user_pendidikan) {
        this.user_pendidikan = user_pendidikan;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
