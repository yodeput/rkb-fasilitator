package id.triwikrama.rkbfasilitator.model;

public class Home {

    private String user_type;
    private String user_name;
    private String user_photo;
    private String rkb_id;
    private String user_id;
    private String total_adm;
    private String total_event;
    private String total_pelatihan;
    private String total_kunjungan;

    public Home(String user_type, String user_name, String user_photo, String rkb_id, String user_id, String total_adm, String total_event, String total_pelatihan, String total_kunjungan) {
        this.user_type = user_type;
        this.user_name = user_name;
        this.user_photo = user_photo;
        this.rkb_id = rkb_id;
        this.user_id = user_id;
        this.total_adm = total_adm;
        this.total_event = total_event;
        this.total_pelatihan = total_pelatihan;
        this.total_kunjungan = total_kunjungan;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_photo() {
        return user_photo;
    }

    public void setUser_photo(String user_photo) {
        this.user_photo = user_photo;
    }

    public String getRkb_id() {
        return rkb_id;
    }

    public void setRkb_id(String rkb_id) {
        this.rkb_id = rkb_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTotal_adm() {
        return total_adm;
    }

    public void setTotal_adm(String total_adm) {
        this.total_adm = total_adm;
    }

    public String getTotal_event() {
        return total_event;
    }

    public void setTotal_event(String total_event) {
        this.total_event = total_event;
    }

    public String getTotal_pelatihan() {
        return total_pelatihan;
    }

    public void setTotal_pelatihan(String total_pelatihan) {
        this.total_pelatihan = total_pelatihan;
    }

    public String getTotal_kunjungan() {
        return total_kunjungan;
    }

    public void setTotal_kunjungan(String total_kunjungan) {
        this.total_kunjungan = total_kunjungan;
    }
}
