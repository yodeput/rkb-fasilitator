package id.triwikrama.rkbfasilitator.model;

import java.io.Serializable;

public class Profile implements Serializable {
    private String user_alamat;
    private String user_email;
    private String user_name;
    private String rkb_name;
    private String user_notelp;
    private String rkb_id;
    private String user_pendidikan;
    private String user_tingkat;
    private String user_tempatlahir;
    private String user_id;
    private String user_usia;
    private String user_photo;
    private String user_tanggallahir;

    public String getUser_alamat ()
    {
        return user_alamat;
    }

    public void setUser_alamat (String user_alamat)
    {
        this.user_alamat = user_alamat;
    }

    public String getUser_email ()
    {
        return user_email;
    }

    public void setUser_email (String user_email)
    {
        this.user_email = user_email;
    }

    public String getUser_name ()
    {
        return user_name;
    }

    public void setUser_name (String user_name)
    {
        this.user_name = user_name;
    }

    public String getRkb_name ()
    {
        return rkb_name;
    }

    public void setRkb_name (String rkb_name)
    {
        this.rkb_name = rkb_name;
    }

    public String getUser_notelp ()
    {
        return user_notelp;
    }

    public void setUser_notelp (String user_notelp)
    {
        this.user_notelp = user_notelp;
    }

    public String getRkb_id ()
    {
        return rkb_id;
    }

    public void setRkb_id (String rkb_id)
    {
        this.rkb_id = rkb_id;
    }

    public String getUser_pendidikan ()
    {
        return user_pendidikan;
    }

    public void setUser_pendidikan (String user_pendidikan)
    {
        this.user_pendidikan = user_pendidikan;
    }

    public String getUser_tingkat ()
    {
        return user_tingkat;
    }

    public void setUser_tingkat (String user_tingkat)
    {
        this.user_tingkat = user_tingkat;
    }

    public String getUser_tempatlahir ()
    {
        return user_tempatlahir;
    }

    public void setUser_tempatlahir (String user_tempatlahir)
    {
        this.user_tempatlahir = user_tempatlahir;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getUser_usia ()
    {
        return user_usia;
    }

    public void setUser_usia (String user_usia)
    {
        this.user_usia = user_usia;
    }

    public String getUser_photo ()
    {
        return user_photo;
    }

    public void setUser_photo (String user_photo)
    {
        this.user_photo = user_photo;
    }

    public String getUser_tanggallahir ()
    {
        return user_tanggallahir;
    }

    public void setUser_tanggallahir (String user_tanggallahir)
    {
        this.user_tanggallahir = user_tanggallahir;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "user_alamat='" + user_alamat + '\'' +
                ", user_email='" + user_email + '\'' +
                ", user_name='" + user_name + '\'' +
                ", rkb_name='" + rkb_name + '\'' +
                ", user_notelp='" + user_notelp + '\'' +
                ", rkb_id='" + rkb_id + '\'' +
                ", user_pendidikan='" + user_pendidikan + '\'' +
                ", user_tingkat='" + user_tingkat + '\'' +
                ", user_tempatlahir='" + user_tempatlahir + '\'' +
                ", user_id='" + user_id + '\'' +
                ", user_usia='" + user_usia + '\'' +
                ", user_photo='" + user_photo + '\'' +
                ", user_tanggallahir='" + user_tanggallahir + '\'' +
                '}';
    }
}
