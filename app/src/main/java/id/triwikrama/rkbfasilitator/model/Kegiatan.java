package id.triwikrama.rkbfasilitator.model;

import java.io.Serializable;
import java.security.PrivateKey;
import java.util.List;

public class Kegiatan implements Serializable {

    private String kegiatan_id;
    private String kegiatan_photo;
    private String kegiatan_name;
    private String kegiatan_date;
    private String kegiatan;
    private String rkb_name;
    private String rkb_treg;
    private String rkb_kelas;
    private String user_name;

    public boolean progress = false;

    public Kegiatan() {
    }

    public Kegiatan(boolean progress) {
        this.progress = progress;
    }

    public Kegiatan(String kegiatan_id, String kegiatan_photo, String kegiatan_name, String kegiatan_date, String kegiatan, String rkb_name, String rkb_treg, String rkb_kelas) {
        this.kegiatan_id = kegiatan_id;
        this.kegiatan_photo = kegiatan_photo;
        this.kegiatan_name = kegiatan_name;
        this.kegiatan_date = kegiatan_date;
        this.kegiatan = kegiatan;
        this.rkb_name = rkb_name;
        this.rkb_treg = rkb_treg;
        this.rkb_kelas = rkb_kelas;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getKegiatan_id() {
        return kegiatan_id;
    }

    public void setKegiatan_id(String kegiatan_id) {
        this.kegiatan_id = kegiatan_id;
    }

    public String getKegiatan_name() {
        return kegiatan_name;
    }

    public void setKegiatan_name(String kegiatan_name) {
        this.kegiatan_name = kegiatan_name;
    }

    public String getKegiatan_date() {
        return kegiatan_date;
    }

    public void setKegiatan_date(String kegiatan_date) {
        this.kegiatan_date = kegiatan_date;
    }

    public String getKegiatan_photo() {
        return kegiatan_photo;
    }

    public void setKegiatan_photo(String kegiatan_photo) {
        this.kegiatan_photo = kegiatan_photo;
    }

    public String getKegiatan() {
        return kegiatan;
    }

    public void setKegiatan(String kegiatan) {
        this.kegiatan = kegiatan;
    }

    public String getRkb_name() {
        return rkb_name;
    }

    public void setRkb_name(String rkb_name) {
        this.rkb_name = rkb_name;
    }

    public String getRkb_treg() {
        return rkb_treg;
    }

    public void setRkb_treg(String rkb_treg) {
        this.rkb_treg = rkb_treg;
    }

    public String getRkb_kelas() {
        return rkb_kelas;
    }

    public void setRkb_kelas(String rkb_kelas) {
        this.rkb_kelas = rkb_kelas;
    }
}
