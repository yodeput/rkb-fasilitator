package id.triwikrama.rkbfasilitator.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.triwikrama.rkbfasilitator.model.Kegiatan;

public class ListResponse {
    @SerializedName("success")
    Integer success;
    @SerializedName("message")
    String message;
    @SerializedName("data")
    List<Kegiatan> kegiatans;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Kegiatan> getKegiatans() {
        return kegiatans;
    }

    public void setKegiatans(List<Kegiatan> kegiatans) {
        this.kegiatans = kegiatans;
    }
}
