package id.triwikrama.rkbfasilitator.model;

public class Photo
{
    private String gambar;

    public Photo() {
    }

    public Photo(String gambar) {
        this.gambar = gambar;
    }

    public String getGambar ()
    {
        return gambar;
    }

    public void setGambar (String gambar)
    {
        this.gambar = gambar;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [gambar = "+gambar+"]";
    }
}
