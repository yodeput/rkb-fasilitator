package id.triwikrama.rkbfasilitator.model.statistik;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TregResponse {

    @Expose
    @SerializedName("success")
    private int success;
    @Expose
    @SerializedName("data")
    private List<Treg> data;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public List<Treg> getData() {
        return data;
    }

    public void setData(List<Treg> data) {
        this.data = data;
    }
}
