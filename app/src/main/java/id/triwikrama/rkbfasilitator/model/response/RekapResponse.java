package id.triwikrama.rkbfasilitator.model.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import id.triwikrama.rkbfasilitator.model.Rekap;


public class RekapResponse implements Serializable {


    @SerializedName("success")
    private int success;
    @SerializedName("jumlah_kegiatan")
    private String jumlahKegiatan;
    @SerializedName("data")
    private List<Rekap> rekap;

    public RekapResponse() {
    }

    public RekapResponse(int success, String jumlahKegiatan, List<Rekap> rekap) {
        this.success = success;
        this.jumlahKegiatan = jumlahKegiatan;
        this.rekap = rekap;
    }

    public List<Rekap> getRekap() {
        return rekap;
    }

    public void setRekap(List<Rekap> rekap) {
        this.rekap = rekap;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getJumlahKegiatan() {
        return jumlahKegiatan;
    }

    public void setJumlahKegiatan(String jumlahKegiatan) {
        this.jumlahKegiatan = jumlahKegiatan;
    }

}

