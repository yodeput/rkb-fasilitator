package id.triwikrama.rkbfasilitator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Leaderboard {

    @Expose
    @SerializedName("gmv")
    private String gmv;
    @Expose
    @SerializedName("total")
    private float total;
    @Expose
    @SerializedName("admin")
    private String admin;
    @Expose
    @SerializedName("event")
    private String event;
    @Expose
    @SerializedName("pel")
    private String pel;
    @Expose
    @SerializedName("kunj")
    private String kunj;
    @Expose
    @SerializedName("jumlah_kegiatan")
    private String jumlah_kegiatan;
    @Expose
    @SerializedName("rkb_name")
    private String rkb_name;
    @Expose
    @SerializedName("user_name")
    private String user_name;
    @Expose
    @SerializedName("ranking")
    private int ranking;
    @Expose
    @SerializedName("user_photo")
    private String user_photo;


    public String getUser_photo() {
        return user_photo;
    }

    public void setUser_photo(String user_photo) {
        this.user_photo = user_photo;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getGmv() {
        return gmv;
    }

    public void setGmv(String gmv) {
        this.gmv = gmv;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getPel() {
        return pel;
    }

    public void setPel(String pel) {
        this.pel = pel;
    }

    public String getKunj() {
        return kunj;
    }

    public void setKunj(String kunj) {
        this.kunj = kunj;
    }

    public String getJumlah_kegiatan() {
        return jumlah_kegiatan;
    }

    public void setJumlah_kegiatan(String jumlah_kegiatan) {
        this.jumlah_kegiatan = jumlah_kegiatan;
    }

    public String getRkb_name() {
        return rkb_name;
    }

    public void setRkb_name(String rkb_name) {
        this.rkb_name = rkb_name;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }
}
