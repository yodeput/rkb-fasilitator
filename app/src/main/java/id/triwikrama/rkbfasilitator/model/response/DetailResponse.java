package id.triwikrama.rkbfasilitator.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.triwikrama.rkbfasilitator.model.KegiatanDetail;

public class DetailResponse {

    @SerializedName("success")
    Integer success;
    @SerializedName("message")
    String message;
    @SerializedName("data")
    List<KegiatanDetail> detailList;

    public DetailResponse(Integer success, String message, List<KegiatanDetail> detailList) {
        this.success = success;
        this.message = message;
        this.detailList = detailList;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<KegiatanDetail> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<KegiatanDetail> detailList) {
        this.detailList = detailList;
    }

    @Override
    public String toString() {
        return "DetailResponse{" +
                "success=" + success +
                ", message='" + message + '\'' +
                ", detailList=" + detailList +
                '}';
    }
}
