package id.triwikrama.rkbfasilitator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RekapDetail {
    @Expose
    @SerializedName("administrasi")
    private String administrasi;
    @Expose
    @SerializedName("event")
    private String event;
    @Expose
    @SerializedName("pelatihan")
    private String pelatihan;
    @Expose
    @SerializedName("kunjungan")
    private String kunjungan;
    @Expose
    @SerializedName("tanggal")
    private String tanggal;

    public ArrayList<String> getList(){
        ArrayList<String> test = new ArrayList<>();
        test.add(tanggal);
        test.add(kunjungan);
        test.add(pelatihan);
        test.add(event);
        test.add(administrasi);
        return test;
    }

    public String getAdministrasi() {
        return administrasi;
    }

    public void setAdministrasi(String administrasi) {
        this.administrasi = administrasi;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getPelatihan() {
        return pelatihan;
    }

    public void setPelatihan(String pelatihan) {
        this.pelatihan = pelatihan;
    }

    public String getKunjungan() {
        return kunjungan;
    }

    public void setKunjungan(String kunjungan) {
        this.kunjungan = kunjungan;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
