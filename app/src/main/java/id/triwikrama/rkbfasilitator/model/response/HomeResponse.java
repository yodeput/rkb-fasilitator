package id.triwikrama.rkbfasilitator.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.triwikrama.rkbfasilitator.model.Home;

public class HomeResponse {
    @SerializedName("success")
    Integer success;
    @SerializedName("message")
    String message;
    @SerializedName("version")
    String version;
    @SerializedName("versioncode")
    String versioncode;
    @SerializedName("menu_active")
    Boolean menu_active;
    @SerializedName("data")
    List<Home> home;

    public HomeResponse(Integer success, String message, Boolean menu_active, List<Home> home) {
        this.success = success;
        this.message = message;
        this.menu_active = menu_active;
        this.home = home;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Home> getHome() {
        return home;
    }

    public void setHome(List<Home> home) {
        this.home = home;
    }

    public Boolean getMenu_active() {
        return menu_active;
    }

    public void setMenu_active(Boolean menu_active) {
        this.menu_active = menu_active;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersioncode() {
        return versioncode;
    }

    public void setVersioncode(String versioncode) {
        this.versioncode = versioncode;
    }
}
