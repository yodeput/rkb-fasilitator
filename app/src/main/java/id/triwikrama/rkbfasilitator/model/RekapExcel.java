package id.triwikrama.rkbfasilitator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RekapExcel {
    @Expose
    @SerializedName("name_file")
    private String name_file;

    public String getName_file() {
        return name_file;
    }

    public void setName_file(String name_file) {
        this.name_file = name_file;
    }
}
