package id.triwikrama.rkbfasilitator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Username implements Serializable {

    @SerializedName("user_name")
    private String user_name;

    public Username() {
    }

    public Username(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
}
