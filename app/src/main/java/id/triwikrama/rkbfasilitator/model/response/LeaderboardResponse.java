package id.triwikrama.rkbfasilitator.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import id.triwikrama.rkbfasilitator.model.Leaderboard;

public class LeaderboardResponse implements Serializable {
    @Expose
    @SerializedName("success")
    Integer success;
    @Expose
    @SerializedName("message")
    String message;
    @Expose
    @SerializedName("ranking_saya")
    private int ranking_saya;
    @Expose
    @SerializedName("data")
    private List<Leaderboard> data;


    public LeaderboardResponse() {
    }

    public LeaderboardResponse(Integer success, String message, int ranking_saya, List<Leaderboard> data) {
        this.success = success;
        this.message = message;
        this.ranking_saya = ranking_saya;
        this.data = data;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getRanking_saya() {
        return ranking_saya;
    }

    public void setRanking_saya(int ranking_saya) {
        this.ranking_saya = ranking_saya;
    }

    public List<Leaderboard> getData() {
        return data;
    }

    public void setData(List<Leaderboard> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "LeaderboardResponse{" +
                "success=" + success +
                ", message='" + message + '\'' +
                ", ranking_saya=" + ranking_saya +
                ", data=" + data +
                '}';
    }
}
