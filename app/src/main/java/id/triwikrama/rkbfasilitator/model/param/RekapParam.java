package id.triwikrama.rkbfasilitator.model.param;

import com.google.gson.annotations.SerializedName;

public class RekapParam {
    @SerializedName("page")
    private String page;
    @SerializedName("tahun")
    private String tahun;
    @SerializedName("bulan")
    private String bulan;
    @SerializedName("rkb_id")
    private String rkb_id;
    @SerializedName("kelas")
    private String kelas;

    public RekapParam() {
    }

    public RekapParam(String page, String tahun, String bulan) {
        this.page = page;
        this.tahun = tahun;
        this.bulan = bulan;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getRkb_id() {
        return rkb_id;
    }

    public void setRkb_id(String rkb_id) {
        this.rkb_id = rkb_id;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getBulan() {
        return bulan;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }
}
