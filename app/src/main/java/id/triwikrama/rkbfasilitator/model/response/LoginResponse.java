package id.triwikrama.rkbfasilitator.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.triwikrama.rkbfasilitator.model.User;

public class LoginResponse {
    @SerializedName("success")
    Integer success;
    @SerializedName("message")
    String message;
    @SerializedName("data")
    List<User> user;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<User> getUser() {
        return user;
    }

    public void setUser(List<User> user) {
        this.user = user;
    }
}
