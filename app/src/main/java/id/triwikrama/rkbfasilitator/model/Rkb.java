package id.triwikrama.rkbfasilitator.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rkb {

    @Expose
    @SerializedName("rkb_kelas")
    private String rkb_kelas;
    @Expose
    @SerializedName("rkb_treg")
    private String rkb_treg;
    @Expose
    @SerializedName("rkb_name")
    private String rkb_name;
    @Expose
    @SerializedName("rkb_id")
    private String rkb_id;

    public Rkb() {
    }

    public Rkb(String rkb_kelas, String rkb_treg, String rkb_name, String rkb_id) {
        this.rkb_kelas = rkb_kelas;
        this.rkb_treg = rkb_treg;
        this.rkb_name = rkb_name;
        this.rkb_id = rkb_id;
    }

    public String getRkb_kelas() {
        return rkb_kelas;
    }

    public void setRkb_kelas(String rkb_kelas) {
        this.rkb_kelas = rkb_kelas;
    }

    public String getRkb_treg() {
        return rkb_treg;
    }

    public void setRkb_treg(String rkb_treg) {
        this.rkb_treg = rkb_treg;
    }

    public String getRkb_name() {
        return rkb_name;
    }

    public void setRkb_name(String rkb_name) {
        this.rkb_name = rkb_name;
    }

    public String getRkb_id() {
        return rkb_id;
    }

    public void setRkb_id(String rkb_id) {
        this.rkb_id = rkb_id;
    }
}
