package id.triwikrama.rkbfasilitator.model.param;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TanggalParam {

    @Expose
    @SerializedName("tanggal")
    private String tanggal;
    @SerializedName("page")
    private String page;
    @SerializedName("status")
    private String status;

    public TanggalParam() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public TanggalParam(String tanggal, String page) {
        this.tanggal = tanggal;
        this.page = page;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
