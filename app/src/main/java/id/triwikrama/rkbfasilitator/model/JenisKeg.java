package id.triwikrama.rkbfasilitator.model;

public class JenisKeg {
    private String id;
    private String nama;

    public JenisKeg() {
    }

    public JenisKeg(String id, String nama) {
        this.id = id;
        this.nama = nama;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
