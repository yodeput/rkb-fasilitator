package id.triwikrama.rkbfasilitator.model.statistik;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Kelas {
    @Expose
    @SerializedName("kelas_c")
    private String kelas_c;
    @Expose
    @SerializedName("kelas_b")
    private String kelas_b;
    @Expose
    @SerializedName("kelas_a")
    private String kelas_a;

    public String getKelas_c() {
        return kelas_c;
    }

    public void setKelas_c(String kelas_c) {
        this.kelas_c = kelas_c;
    }

    public String getKelas_b() {
        return kelas_b;
    }

    public void setKelas_b(String kelas_b) {
        this.kelas_b = kelas_b;
    }

    public String getKelas_a() {
        return kelas_a;
    }

    public void setKelas_a(String kelas_a) {
        this.kelas_a = kelas_a;
    }
}
