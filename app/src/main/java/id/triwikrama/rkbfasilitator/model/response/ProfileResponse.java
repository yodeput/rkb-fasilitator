package id.triwikrama.rkbfasilitator.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.triwikrama.rkbfasilitator.model.Profile;

public class ProfileResponse {

    @SerializedName("success")
    Integer success;
    @SerializedName("message")
    String message;
    @SerializedName("data")
    List<Profile> profile;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Profile> getProfile() {
        return profile;
    }

    public void setProfile(List<Profile> profile) {
        this.profile = profile;
    }

    public ProfileResponse(Integer success, String message, List<Profile> profile) {

        this.success = success;
        this.message = message;
        this.profile = profile;
    }
}
