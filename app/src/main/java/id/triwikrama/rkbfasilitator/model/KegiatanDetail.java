package id.triwikrama.rkbfasilitator.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import retrofit2.converter.scalars.ScalarsConverterFactory;

public class KegiatanDetail implements Serializable {

    @SerializedName("kegiatan_id")
    private String kegiatan_id;
    @SerializedName("kegiatan_name")
    private String kegiatan_name;
    @SerializedName("kegiatan_date")
    private String kegiatan_date;
    @SerializedName("kegiatan_desc")
    private String kegiatan_desc;
    @SerializedName("kegiatan_lokasi")
    private String kegiatan_lokasi;
    @SerializedName("kegiatan_type")
    private String kegiatan_type;
    @SerializedName("instansi_terkait")
    private String instansi_terkait;
    @SerializedName("kegiatan_lat")
    private String kegiatan_lat;
    @SerializedName("kegiatan_lng")
    private String kegiatan_lng;
    @SerializedName("kegiatan_time")
    private String kegiatan_time;
     @SerializedName("jml_peserta")
    private String jml_peserta;
    @SerializedName("photo")
    private List<KegiatanPhoto> photos;

    public KegiatanDetail(String kegiatan_id, String kegiatan_name, String kegiatan_date, String kegiatan_desc, String kegiatan_lokasi, String kegiatan_type, String instansi_terkait, String kegiatan_lat, String kegiatan_lng, String kegiatan_time, String jml_peserta, List<KegiatanPhoto> photos) {
        this.kegiatan_id = kegiatan_id;
        this.kegiatan_name = kegiatan_name;
        this.kegiatan_date = kegiatan_date;
        this.kegiatan_desc = kegiatan_desc;
        this.kegiatan_lokasi = kegiatan_lokasi;
        this.kegiatan_type = kegiatan_type;
        this.instansi_terkait = instansi_terkait;
        this.kegiatan_lat = kegiatan_lat;
        this.kegiatan_lng = kegiatan_lng;
        this.kegiatan_time = kegiatan_time;
        this.jml_peserta = jml_peserta;
        this.photos = photos;
    }

    public String getJml_peserta() {
        return jml_peserta;
    }

    public void setJml_peserta(String jml_peserta) {
        this.jml_peserta = jml_peserta;
    }

    public String getKegiatan_type() {
        return kegiatan_type;
    }

    public void setKegiatan_type(String kegiatan_type) {
        this.kegiatan_type = kegiatan_type;
    }

    public String getKegiatan_id() {
        return kegiatan_id;
    }

    public void setKegiatan_id(String kegiatan_id) {
        this.kegiatan_id = kegiatan_id;
    }

    public String getKegiatan_name() {
        return kegiatan_name;
    }

    public void setKegiatan_name(String kegiatan_name) {
        this.kegiatan_name = kegiatan_name;
    }

    public String getKegiatan_date() {
        return kegiatan_date;
    }

    public void setKegiatan_date(String kegiatan_date) {
        this.kegiatan_date = kegiatan_date;
    }

    public String getKegiatan_desc() {
        return kegiatan_desc;
    }

    public void setKegiatan_desc(String kegiatan_desc) {
        this.kegiatan_desc = kegiatan_desc;
    }

    public String getKegiatan_lokasi() {
        return kegiatan_lokasi;
    }

    public void setKegiatan_lokasi(String kegiatan_lokasi) {
        this.kegiatan_lokasi = kegiatan_lokasi;
    }

    public String getInstansi_terkait() {
        return instansi_terkait;
    }

    public void setInstansi_terkait(String instansi_terkait) {
        this.instansi_terkait = instansi_terkait;
    }

    public String getKegiatan_lat() {
        return kegiatan_lat;
    }

    public void setKegiatan_lat(String kegiatan_lat) {
        this.kegiatan_lat = kegiatan_lat;
    }

    public String getKegiatan_lng() {
        return kegiatan_lng;
    }

    public void setKegiatan_lng(String kegiatan_lng) {
        this.kegiatan_lng = kegiatan_lng;
    }

    public String getKegiatan_time() {
        return kegiatan_time;
    }

    public void setKegiatan_time(String kegiatan_time) {
        this.kegiatan_time = kegiatan_time;
    }

    public List<KegiatanPhoto> getPhotos() {
        return photos;
    }

    public void setPhotos(List<KegiatanPhoto> photos) {
        this.photos = photos;
    }

    @Override
    public String toString() {
        return "KegiatanDetail{" +
                "kegiatan_id='" + kegiatan_id + '\'' +
                ", kegiatan_name='" + kegiatan_name + '\'' +
                ", kegiatan_date='" + kegiatan_date + '\'' +
                ", kegiatan_desc='" + kegiatan_desc + '\'' +
                ", kegiatan_lokasi='" + kegiatan_lokasi + '\'' +
                ", instansi_terkait='" + instansi_terkait + '\'' +
                ", kegiatan_lat='" + kegiatan_lat + '\'' +
                ", kegiatan_lng='" + kegiatan_lng + '\'' +
                ", kegiatan_time='" + kegiatan_time + '\'' +
                ", photos=" + photos +
                '}';
    }
}
