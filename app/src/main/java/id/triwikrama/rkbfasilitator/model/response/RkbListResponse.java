package id.triwikrama.rkbfasilitator.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.triwikrama.rkbfasilitator.model.RkbList;
import id.triwikrama.rkbfasilitator.model.UserList;

public class RkbListResponse {

    @Expose
    @SerializedName("success")
    private int success;
    @Expose
    @SerializedName("jumlah")
    private int jumlah;
    @Expose
    @SerializedName("data")
    private List<RkbList> data;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public List<RkbList> getData() {
        return data;
    }

    public void setData(List<RkbList> data) {
        this.data = data;
    }
}
