package id.triwikrama.rkbfasilitator.model.param;

public class DetailParam {
    private String kegiatan_id;
    private String type;

    public DetailParam() {
    }

    public DetailParam(String kegiatan_id) {
        this.kegiatan_id = kegiatan_id;
    }

    public String getKegiatan_id() {
        return kegiatan_id;
    }

    public void setKegiatan_id(String kegiatan_id) {
        this.kegiatan_id = kegiatan_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
