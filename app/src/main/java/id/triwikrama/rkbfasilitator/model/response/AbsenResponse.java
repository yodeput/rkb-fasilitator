package id.triwikrama.rkbfasilitator.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import id.triwikrama.rkbfasilitator.model.Absen;

public class AbsenResponse {


    @Expose
    @SerializedName("success")
    private int success;
    @Expose
    @SerializedName("total_data")
    private String total_data;


    @Expose
    @SerializedName("data")
    private ArrayList<Absen> data;

    public String getTotal_data() {
        return total_data;
    }

    public void setTotal_data(String total_data) {
        this.total_data = total_data;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public ArrayList<Absen> getData() {
        return data;
    }

    public void setData(ArrayList<Absen> data) {
        this.data = data;
    }
}
