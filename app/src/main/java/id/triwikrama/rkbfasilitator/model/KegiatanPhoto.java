package id.triwikrama.rkbfasilitator.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.esafirm.imagepicker.model.Image;

import java.io.Serializable;

public class KegiatanPhoto implements Serializable {
    private String kegiatan_photo;

    public KegiatanPhoto() {
    }

    public KegiatanPhoto(String kegiatan_photo) {
        this.kegiatan_photo = kegiatan_photo;
    }

    public String getKegiatan_photo() {
        return kegiatan_photo;
    }

    public void setKegiatan_photo(String kegiatan_photo) {
        this.kegiatan_photo = kegiatan_photo;
    }

    /**/


}
