package id.triwikrama.rkbfasilitator.model.statistik;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class KelasResponse {

    @Expose
    @SerializedName("success")
    private int success;
    @Expose
    @SerializedName("data")
    private List<Kelas> data;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public List<Kelas> getData() {
        return data;
    }

    public void setData(List<Kelas> data) {
        this.data = data;
    }
}
