package id.triwikrama.rkbfasilitator.model.param;

public class EditPhotoParam {

    private String user_id;
    private String photo;

    public EditPhotoParam(String user_id, String photo) {
        this.user_id = user_id;
        this.photo = photo;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
