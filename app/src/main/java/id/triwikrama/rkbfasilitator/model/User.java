package id.triwikrama.rkbfasilitator.model;

public class User {
    private String user_id;
    private String rkb_id;
    private String user_name;
    private String user_type;
    private String user_photo;
    private String user_token;
    private String user_kelas;

    public User() {
    }

    public String getUser_kelas() {
        return user_kelas;
    }

    public void setUser_kelas(String user_kelas) {
        this.user_kelas = user_kelas;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRkb_id() {
        return rkb_id;
    }

    public void setRkb_id(String rkb_id) {
        this.rkb_id = rkb_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getUser_photo() {
        return user_photo;
    }

    public void setUser_photo(String user_photo) {
        this.user_photo = user_photo;
    }

    public String getUser_token() {
        return user_token;
    }

    public void setUser_token(String user_token) {
        this.user_token = user_token;
    }
}
