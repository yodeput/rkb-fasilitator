package id.triwikrama.rkbfasilitator.model.param;

import java.util.List;

import id.triwikrama.rkbfasilitator.model.Photo;

public class AbsenParam {
    private String user_id;
    private String lat;
    private String lng;
    private String photo;

    public AbsenParam() {
    }

    public AbsenParam(String user_id, String lat, String lng, String photo) {
        this.user_id = user_id;
        this.lat = lat;
        this.lng = lng;
        this.photo = photo;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
