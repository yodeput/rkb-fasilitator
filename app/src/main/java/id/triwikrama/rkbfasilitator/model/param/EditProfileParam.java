package id.triwikrama.rkbfasilitator.model.param;

public class EditProfileParam {
    private String user_id;
    private String name;
    private String alamat;
    private String notelp;
    private String tempatlahir;
    private String tanggallahir;
    private String pendidikan;
    private String tingkat;

    public EditProfileParam() {
    }

    public EditProfileParam(String user_id, String name, String alamat, String notelp, String tempatlahir, String tanggallahir, String pendidikan, String tingkat) {
        this.user_id = user_id;
        this.name = name;
        this.alamat = alamat;
        this.notelp = notelp;
        this.tempatlahir = tempatlahir;
        this.tanggallahir = tanggallahir;
        this.pendidikan = pendidikan;
        this.tingkat = tingkat;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNotelp() {
        return notelp;
    }

    public void setNotelp(String notelp) {
        this.notelp = notelp;
    }

    public String getTempatlahir() {
        return tempatlahir;
    }

    public void setTempatlahir(String tempatlahir) {
        this.tempatlahir = tempatlahir;
    }

    public String getTanggallahir() {
        return tanggallahir;
    }

    public void setTanggallahir(String tanggallahir) {
        this.tanggallahir = tanggallahir;
    }

    public String getPendidikan() {
        return pendidikan;
    }

    public void setPendidikan(String pendidikan) {
        this.pendidikan = pendidikan;
    }

    public String getTingkat() {
        return tingkat;
    }

    public void setTingkat(String tingkat) {
        this.tingkat = tingkat;
    }

    @Override
    public String toString() {
        return "EditProfileParam{" +
                "user_id='" + user_id + '\'' +
                ", name='" + name + '\'' +
                ", alamat='" + alamat + '\'' +
                ", notelp='" + notelp + '\'' +
                ", tempatlahir='" + tempatlahir + '\'' +
                ", tanggallahir='" + tanggallahir + '\'' +
                ", pendidikan='" + pendidikan + '\'' +
                ", tingkat='" + tingkat + '\'' +
                '}';
    }
}
