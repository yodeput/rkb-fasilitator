package id.triwikrama.rkbfasilitator;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.motion.widget.MotionLayout;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.mikhaellopez.circularimageview.CircularImageView;


import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.triwikrama.rkbfasilitator.activity.AbsenActivity;
import id.triwikrama.rkbfasilitator.activity.LeaderboardTabbedActivity;
import id.triwikrama.rkbfasilitator.activity.admin.MainActivityAdmin;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.activity.LeaderboardActivity;
import id.triwikrama.rkbfasilitator.activity.ListDataActivity;
import id.triwikrama.rkbfasilitator.activity.ProfileActivity;
import id.triwikrama.rkbfasilitator.model.Home;
import id.triwikrama.rkbfasilitator.model.Profile;
import id.triwikrama.rkbfasilitator.model.param.LoginParam;
import id.triwikrama.rkbfasilitator.model.param.ProfileParam;
import id.triwikrama.rkbfasilitator.model.response.HomeResponse;
import id.triwikrama.rkbfasilitator.model.response.LoginResponse;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import id.triwikrama.rkbfasilitator.utils.MotionAnim;
import id.triwikrama.rkbfasilitator.utils.PrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class MainActivity extends AppCompatActivity {

    @BindColor(R.color.black)
    int blackColor;
    //START & LOGIN VIEW
    @BindView(R.id.motionLayout)
    MotionLayout motionLayout;
    @BindView(R.id.formLogin)
    LinearLayout formLogin;
    //HOME VIEW
    @BindView(R.id.imgDot)
    ImageView imgDot;
    @BindView(R.id.txtTanggal)
    TextView txtTanggal;
    @BindView(R.id.img_logo)
    ImageView imgLogo;
    @BindView(R.id.img_bg)
    ImageView img_bg;
    @BindView(R.id.img_loading)
    ImageView imgLoading;
    @BindView(R.id.txtWelcome)
    TextView txtWelcome;
    @BindView(R.id.ll_txt)
    LinearLayout ll_txt;
    @BindView(R.id.txtStarting)
    TextView txtStarting;
    @BindView(R.id.txtRkb)
    TextView txtRkb;
    @BindView(R.id.txtVersion)
    TextView txtVersion;
    @BindView(R.id.txtNama)
    TextView txtNama;

    @BindView(R.id.editEmail)
    AppCompatEditText editEmail;
    @BindView(R.id.editPassword)
    AppCompatEditText editPassword;

    @BindView(R.id.txtWelcome2)
    TextView txtWelcome2;
    @BindView(R.id.imgAvatar)
    CircularImageView imgAvatar;
    @BindViews({R.id.txtKunjungan, R.id.txtPelatihan, R.id.txtEvent, R.id.txtAdmin})
    List<TextView> txtValue;
    private String loading, appVersionCode, serverVersionCode;
    private int i = 0;
    //1=admin;2=fasilitator
    private MotionAnim anim;
    private int dotDuration = 1000;
    private Boolean isLogin = false;
    private Boolean doubleBackToExitPressedOnce = false;
    private Boolean doubleClickAbsen = false;
    private Boolean isClickable = true;
    private Intent intent;
    private Boolean menuActive=false;
    private Dialog dialog;

    private User userData;
    private CustomDialog cd;
    private DatabaseHelper db;
    private PrefManager pref;
    private Profile profileData;
    private NetworkService mNetworkService;

    @BindViews({R.id.butKunjungan, R.id.butPelatihan, R.id.butEvent, R.id.butAdmin})
    List<CardView> buttonsUser;

    @OnClick({R.id.butKunjungan, R.id.butPelatihan, R.id.butEvent, R.id.butAdmin,
            R.id.butLeaderboard, R.id.butLogout, R.id.imgAvatar})
    public void setViewOnClickEvent(View view) {
        switch (view.getId()) {
            case R.id.butKunjungan:
                if(menuActive) {
                    intent = new Intent(this, ListDataActivity.class);
                    intent.putExtra("src", getString(R.string.menu_kunjungan));
                    startActivityForResult(intent,300);
                    overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
                } else {
                    cd.toastWarning("Anda belum melakukan Absen hari ini");
                }
                break;
            case R.id.butPelatihan:
                if(menuActive) {
                    intent = new Intent(this, ListDataActivity.class);
                    intent.putExtra("src", getString(R.string.menu_pelatihan));
                    startActivityForResult(intent,300);
                    overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
                } else {
                    cd.toastWarning("Anda belum melakukan Absen hari ini");
                }

                break;
            case R.id.butEvent:
                if(menuActive) {
                    intent = new Intent(this, ListDataActivity.class);
                    intent.putExtra("src", getString(R.string.menu_event));
                    startActivityForResult(intent,300);
                    overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
                } else {
                    cd.toastWarning("Anda belum melakukan Absen hari ini");
                }

                break;
            case R.id.butAdmin:
                if(menuActive) {
                    intent = new Intent(this, ListDataActivity.class);
                    intent.putExtra("src", getString(R.string.menu_admin));
                    startActivityForResult(intent,300);
                    overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
                } else {
                    cd.toastWarning("Anda belum melakukan Absen hari ini");
                }

                break;
            case R.id.butLeaderboard:
                startActivity(new Intent(this, LeaderboardTabbedActivity.class));
                overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
                break;
            case R.id.butLogout:
                cd.dialog_exit(motionLayout);
                break;
            case R.id.imgAvatar:
                intent = new Intent(this, ProfileActivity.class);
                //intent.putExtra("data", profileData);
                startActivity(intent);
                overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
                break;
        }
    }

    @OnClick(R.id.ll_tanggal) void absensi(){
                if(db.getUserType()==2){
                    if(!menuActive) {
                        intent = new Intent(MainActivity.this, AbsenActivity.class);
                        intent.putExtra("src", "Absen RKB");
                        startActivityForResult(intent,300);
                        overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
                    } else {
                        cd.toastSuccess("Anda sudah melakukan Absen hari ini");
                    }

                }
    }

    @OnClick(R.id.butLogin)
    void login() {
        if ((getInstance().isEmpty(editEmail)) || (getInstance().isEmpty(editPassword))) {
            YoYo.with(Techniques.Bounce)
                    .duration(200)
                    .repeat(2)
                    .playOn(editEmail);
            YoYo.with(Techniques.Bounce)
                    .duration(200)
                    .repeat(2)
                    .playOn(editPassword);
            cd.toastError("Isi Username & Password");
        } else {
            if (isClickable) {
                reqLogin();


            }
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initClass();
        initView();

        //email/pass harcoded
        //editLogin.get(0).setText("adly@email.com");
        //editLogin.get(1).setText("123456");

//        editLogin.get(0).setText("admin@email.com");
//        editLogin.get(1).setText("123456");
    }

    void initClass() {
        anim = new MotionAnim(this);
        cd = new CustomDialog(this);
        pref = new PrefManager(this);
        db = new DatabaseHelper(this);
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
        if (db.getCount() > 0) {
            isLogin = true;
        }
    }

    void initView() {
        getInstance().setLightStatusBar(this);
        addPoint();
        appVersionCode = getInstance().appVersionCode();
        txtVersion.setText("Version " + getInstance().appVersion());

        RotateAnimation rotate = new RotateAnimation(0, 1000, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(5000);
        rotate.setInterpolator(new LinearInterpolator());
        imgLoading.startAnimation(rotate);
        txtTanggal.setText(Html.fromHtml(getInstance().getDateTime()));
        img_bg.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==300){
            if(resultCode==RESULT_OK){
                reqHomeData();
            }
        }
    }

    private void reqLogin() {
        cd.show_p_Dialog();
        isClickable = false;
        Log.e("reqLogin", "true");

        String email = editEmail.getText().toString();
        String password = editPassword.getText().toString();
        Log.e("email", email);
        Log.e("password", password);


        Call<LoginResponse> login = mNetworkService.Login(new LoginParam(email, password));
        login.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                cd.hide_p_Dialog();
                isClickable = true;
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        userData = response.body().getUser().get(0);
                        db.addUser(userData);
                        if(db.getUserType()==1){
                            Intent i = new Intent(MainActivity.this, MainActivityAdmin.class);
                            i.putExtra("src", "login");
                            startActivity(i);
                            finish();
                        } else {
                            loginToHome();
                        }
                    } else {
                        cd.oneButtonDialog(response.body().getMessage(), R.drawable.ic_user, R.color.merah);
                    }
                } else {
                    cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

                cd.hide_p_Dialog();
                isClickable = true;
            }
        });
    }

    void loginTrue() {
        imgLoading.animate().alpha(0.0f);
        ll_txt.animate().alpha(0.0f);
        txtVersion.animate().alpha(0.0f);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        anim.loginTrue(motionLayout);
        userData = db.getUser();
        cd.show_p_Dialog();
    }

    void loginToHome() {
        txtVersion.animate().alpha(0.0f);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        anim.loginToHome(motionLayout);
        reqHomeData();
        cd.show_p_Dialog();
    }

    void reqHomeData() {
        //cd.show_p_Dialog();
        Call<HomeResponse> homeData = mNetworkService.homeData(db.getUser().getUser_token(), new ProfileParam(db.getUser().getUser_id()));
        homeData.enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        menuActive = response.body().getMenu_active();
                        serverVersionCode = response.body().getVersioncode();
                        initHome(response.body().getHome().get(0));
                    } else {
                        cd.toastError("Terjadi kesalahan pada koneksi");
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }
            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {

            }
        });
    }

    void initHome(Home home) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        initPermission();
        int appVC = Integer.parseInt(appVersionCode);
        //appVC = 191001;
        int serverVC = Integer.parseInt(serverVersionCode);
        if(serverVC-1 > appVC){
            dialogUpdate();
        }
        if(db.getUser().getUser_type().equals("1")){
            txtNama.setText(home.getUser_name());
        } else if(db.getUser().getUser_type().equals("2")){
            txtNama.setText(home.getUser_name());
        }


        txtValue.get(0).setText(home.getTotal_kunjungan());
        txtValue.get(1).setText(home.getTotal_pelatihan());
        txtValue.get(2).setText(home.getTotal_event());
        txtValue.get(3).setText(home.getTotal_adm());

        RequestOptions options;
        /*if (userData.getGender().contains("laki")) {
            options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.avatar_man)
                    .error(R.drawable.avatar_man);
        } else {
            options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.avatar_girl)
                    .error(R.drawable.avatar_girl);
        }*/
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.avatar_man)
                .error(R.drawable.avatar_man);
        Glide.with(imgAvatar)
                .load(getInstance().urlFixer(home.getUser_photo()))
                .centerCrop()
                .apply(options)
                .into(imgAvatar);
        if(menuActive){
            butEnabled();
        } else {
            butDisabled();
        }
        img_bg.setVisibility(View.VISIBLE);
        cd.hide_p_Dialog();
    }

    public void dialogUpdate() {
        if(dialog==null||!dialog.isShowing()) {
            dialog = new Dialog(MainActivity.this, R.style.DialogSlideAnim);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_one_button);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER_HORIZONTAL;
            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setAttributes(lp);


            dialog.setCanceledOnTouchOutside(false);
            dialog.setOnKeyListener(new Dialog.OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface arg0, int keyCode,
                                     KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        finish();
                    }
                    return true;
                }
            });
            Drawable img = MainActivity.this.getResources().getDrawable(R.drawable.ic_close);
            int color = MainActivity.this.getResources().getColor(R.color.red_600);
            ((LinearLayout) dialog.findViewById(R.id.header_dialog)).setBackgroundColor(color);
            ((ImageView) dialog.findViewById(R.id.img_dialog)).setImageDrawable(img);
            ((TextView) dialog.findViewById(R.id.txt_message)).setText("Terdapat permbaharuan Aplikasi !\nSilahkan perbarui untuk dapat melanjutkan");
            ((Button) dialog.findViewById(R.id.bt_positive)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dialog.dismiss();
                    final String appPackageName = "id.triwikrama.rkbfasilitator";//getPackageName();
                    // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }

                }
            });
        }
    }

    private void butDisabled(){
        for(int i=0;i<buttonsUser.size();i++) {
            buttonsUser.get(i).setCardBackgroundColor(getResources().getColor(R.color.grey_200));
        }
        imgDot.setImageDrawable(getResources().getDrawable(R.drawable.ic_dot_red));
    }
    private void butEnabled(){
        for(int i=0;i<buttonsUser.size();i++) {
            buttonsUser.get(i).setCardBackgroundColor(getResources().getColor(R.color.white));
        }
        imgDot.setImageDrawable(getResources().getDrawable(R.drawable.ic_dot_green));
    }

    void animtoLogin() {
        imgLoading.animate().alpha(0.0f);
        ll_txt.animate().alpha(0.0f);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        anim.toLogin(motionLayout);
    }


    void setBlackText(TextView v) {
        v.setTextColor(blackColor);
    }

    void addPoint() {
        final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                if (isLogin) {
                    if(db.getUserType()==2) {
                        reqHomeData();
                    }
                }
                loading = txtRkb.getText().toString();
                loading = loading + " .";
                txtRkb.setText(loading);
                i++;

                if (i < 4) {
                    handler.postDelayed(this, dotDuration);
                } else {
                    if (isLogin) {
                        if(db.getUserType()==1){
                            Intent i = new Intent(MainActivity.this, MainActivityAdmin.class);
                            i.putExtra("src", "loading");
                            startActivity(i);
                            finish();
                        } else {

                            loginTrue();
                        }
                    } else {
                        animtoLogin();
                    }
                }
            }
        };
        handler.post(r);

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        cd.toastWarning("Tekan kembali untuk keluar");

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void initPermission() {

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.CAMERA
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
            }
        }).check();
    }
}
