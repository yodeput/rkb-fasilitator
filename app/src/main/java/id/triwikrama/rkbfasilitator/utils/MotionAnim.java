package id.triwikrama.rkbfasilitator.utils;

import android.app.Application;
import android.content.Context;
import androidx.constraintlayout.motion.widget.MotionLayout;
import id.triwikrama.rkbfasilitator.R;


public class MotionAnim extends Application {
    Context mContext;
    public MotionAnim(Context context){
        this.mContext = context;
    }

    public void toLogin(MotionLayout motionLayout){
        motionLayout.loadLayoutDescription(R.xml.scene_startup);
        motionLayout.setTransition(R.id.start,R.id.end);
        motionLayout.transitionToStart();
        motionLayout.transitionToEnd();
    }

    public void loginTrue(MotionLayout motionLayout){
        motionLayout.loadLayoutDescription(R.xml.startup_to_home);
        motionLayout.setTransition(R.id.start,R.id.toHome);
        motionLayout.transitionToStart();
        motionLayout.transitionToEnd();
    }

    public void loginToHome(MotionLayout motionLayout){
        motionLayout.loadLayoutDescription(R.xml.scene_to_home);
        motionLayout.setTransition(R.id.start,R.id.toHome);
        motionLayout.transitionToStart();
        motionLayout.transitionToEnd();
    }

    public void logout(MotionLayout motionLayout){
        motionLayout.loadLayoutDescription(R.xml.scene_logout);
        motionLayout.setTransition(R.id.start,R.id.toHome);
        motionLayout.transitionToStart();
        motionLayout.transitionToEnd();
    }

}
