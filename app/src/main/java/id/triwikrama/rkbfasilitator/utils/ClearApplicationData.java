package id.triwikrama.rkbfasilitator.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.File;

import id.triwikrama.rkbfasilitator.MainActivity;

import static id.triwikrama.rkbfasilitator.utils.PrefManager.PREF_NAME;

public class ClearApplicationData extends AsyncTask<Void, String, String> {

    Context mContext;
    ProgressDialog mDialog;

    public ClearApplicationData(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = new ProgressDialog(mContext); //Change MainActivity as per your activity
        mDialog.setMessage("RekapExcel is clearing...");
        mDialog.setCancelable(false);
        //mDialog.show();
    }


    protected String doInBackground(Void... urls) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.apply();
        File cacheDir = mContext.getCacheDir();
        File appDir = new File(cacheDir.getParent());
        deleteRecursive(appDir);
        return "";
    }

    protected void onPostExecute(String result) {
        //mDialog.dismiss();
        //Toast.makeText(mContext, "RekapExcel is cleared.", Toast.LENGTH_SHORT).show();
        mContext.startActivity(new Intent(mContext, MainActivity.class));
        ((Activity)mContext).finish();
    }


    public void deleteRecursive(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                deleteRecursive(child);
            }
        }
        fileOrDirectory.delete();
    }
}


