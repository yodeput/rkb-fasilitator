package id.triwikrama.rkbfasilitator.utils;


/*
 * ******************************************************
 *  * Copyright (c) 2019. All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Created by: Yogi Dewansyah<yodeput@gmail.com>
 *  ******************************************************
 */

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.constraintlayout.motion.widget.MotionLayout;

import com.google.android.material.textfield.TextInputEditText;
import com.shashank.sony.fancytoastlib.FancyToast;

import java.io.File;

import id.triwikrama.rkbfasilitator.MainActivity;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.activity.admin.ListRkbActivityAdmin;
import id.triwikrama.rkbfasilitator.model.param.EditPwdParam;

import static android.app.Activity.RESULT_OK;
import static id.triwikrama.rkbfasilitator.utils.DatabaseHelper.DB_NAME;


public class CustomDialog extends Application {
    Context mContext;
    Dialog p_dialog = null;
    Dialog dialog = null;
    Handler handler;
    MotionAnim motionAnim;
    PrefManager pref;

    public CustomDialog(Context context) {
        this.mContext = context;
        init_p_dialog();
        handler = new Handler();
        pref = new PrefManager(mContext);
        motionAnim = new MotionAnim(mContext);
    }

    final String GetString(int string) {
        return mContext.getString(string);
    }

    ///TOAST
    public void toastWarning(String msg) {
        FancyToast.makeText(mContext, msg, FancyToast.LENGTH_LONG, FancyToast.WARNING, false).show();
    }

    public void toastError(String msg) {
        FancyToast.makeText(mContext, msg, FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
    }

    public void toastSuccess(String msg) {
        FancyToast.makeText(mContext, msg, FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show();
    }

    public void init_p_dialog() {
        if(p_dialog==null||!p_dialog.isShowing()) {
            p_dialog = new Dialog(mContext);
            p_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // b// efore
            p_dialog.setContentView(R.layout.dialog_progress);
            p_dialog.setCanceledOnTouchOutside(false);
            p_dialog.setOnKeyListener(new Dialog.OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface arg0, int keyCode,
                                     KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                    }
                    return true;
                }
            });

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(p_dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER_HORIZONTAL;
            p_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            p_dialog.getWindow().setAttributes(lp);
            ProgressBar progressBar = (ProgressBar) p_dialog.findViewById(R.id.progress);
          /*  ChasingDots mChasingDotsDrawable = new ChasingDots();
            mChasingDotsDrawable.setColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        progressBar.setIndeterminateDrawable(mChasingDotsDrawable);*/
        }
    }

    public void show_p_Dialog() {
        if (!p_dialog.isShowing())
            p_dialog.show();
    }

    public void hide_p_Dialog() {
        if (p_dialog.isShowing())
            p_dialog.dismiss();
    }

    public void oneButtonDialog(String message, int img_header, int color_header) {
        if(dialog==null||!dialog.isShowing()) {
            dialog = new Dialog(mContext, R.style.DialogSlideAnim);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // b// efore
            dialog.setContentView(R.layout.dialog_one_button);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER_HORIZONTAL;
            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setAttributes(lp);


            dialog.setCanceledOnTouchOutside(false);
            dialog.setOnKeyListener(new Dialog.OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface arg0, int keyCode,
                                     KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                    }
                    return true;
                }
            });
            Drawable img = mContext.getResources().getDrawable(img_header);
            int color = mContext.getResources().getColor(color_header);

            ((LinearLayout) dialog.findViewById(R.id.header_dialog)).setBackgroundColor(color);
            ((ImageView) dialog.findViewById(R.id.img_dialog)).setImageDrawable(img);
            ((TextView) dialog.findViewById(R.id.txt_message)).setText(message);
            ((Button) dialog.findViewById(R.id.bt_positive)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }
    }

    public void oneButtonDialog_finish(String message, int img_header, int color_header) {
        if(dialog==null||!dialog.isShowing()) {
            dialog = new Dialog(mContext, R.style.DialogSlideAnim);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_one_button);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER_HORIZONTAL;
            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setAttributes(lp);


            dialog.setCanceledOnTouchOutside(false);
            dialog.setOnKeyListener(new Dialog.OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface arg0, int keyCode,
                                     KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                    }
                    return true;
                }
            });
            Drawable img = mContext.getResources().getDrawable(img_header);
            int color = mContext.getResources().getColor(color_header);

            ((LinearLayout) dialog.findViewById(R.id.header_dialog)).setBackgroundColor(color);
            ((ImageView) dialog.findViewById(R.id.img_dialog)).setImageDrawable(img);
            ((TextView) dialog.findViewById(R.id.txt_message)).setText(message);
            ((Button) dialog.findViewById(R.id.bt_positive)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Intent i = new Intent();
                    ((Activity) mContext).setResult(RESULT_OK,i);
                    ((Activity) mContext).finish();

                }
            });
        }
    }

    public void dialog_logout() {
        if(dialog==null||!dialog.isShowing()) {
            dialog = new Dialog(mContext, R.style.DialogSlideAnim);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_two_button);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER_HORIZONTAL;
            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setAttributes(lp);


            dialog.setCanceledOnTouchOutside(false);
            dialog.setOnKeyListener(new Dialog.OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface arg0, int keyCode,
                                     KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                    }
                    return true;
                }
            });

            ((TextView) dialog.findViewById(R.id.txt_message)).setText(GetString(R.string.text_logout));
            ((Button) dialog.findViewById(R.id.bt_positive)).setText(GetString(R.string.but_logout));
            ((Button) dialog.findViewById(R.id.bt_positive)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    show_p_Dialog();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            hide_p_Dialog();
                            ((Activity) mContext).finish();
                        }
                    }, 1000);


                }
            });
            ((Button) dialog.findViewById(R.id.bt_negative)).setText(GetString(R.string.but_nlogout));
            ((Button) dialog.findViewById(R.id.bt_negative)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }
    }

    public void dialog_exit(MotionLayout motionLayout) {
        if(dialog==null||!dialog.isShowing()) {
            dialog = new Dialog(mContext, R.style.DialogSlideAnim);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_two_button);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER_HORIZONTAL;
            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setAttributes(lp);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setOnKeyListener(new Dialog.OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface arg0, int keyCode,
                                     KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                    }
                    return true;
                }
            });

            ((TextView) dialog.findViewById(R.id.txt_message)).setText(GetString(R.string.text_logout));
            ((Button) dialog.findViewById(R.id.bt_positive)).setText(GetString(R.string.but_logout));
            ((Button) dialog.findViewById(R.id.bt_positive)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    show_p_Dialog();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            hide_p_Dialog();
                            //motionAnim.logout(motionLayout);
                            //pref.clearData();
                            //mContext.deleteDatabase(DB_NAME);
                            //Apps.getInstance().deleteAppData();
                            new ClearApplicationData(mContext).execute();
                        }
                    }, 1000);


                }
            });
            ((Button) dialog.findViewById(R.id.bt_negative)).setText(GetString(R.string.but_nlogout));
            ((Button) dialog.findViewById(R.id.bt_negative)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }
    }

    private void deleteData(){
        File chache = mContext.getCacheDir();
        File appDir = chache.getParentFile();
        if (appDir.exists()) {
           File[] child = appDir.listFiles();
           for(int i=0;i<child.length;i++){
               child = appDir.listFiles();
               if(child.length>0) {
                   child[i].delete();
               } else {
                  startActivity(new Intent(mContext, MainActivity.class));
                   ((Activity)mContext).finish();
               }
           }
        }

    }

    public void dialog_exit2() {
        if(dialog==null||!dialog.isShowing()) {
            dialog = new Dialog(mContext, R.style.DialogSlideAnim);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_two_button);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER_HORIZONTAL;
            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setAttributes(lp);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setOnKeyListener(new Dialog.OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface arg0, int keyCode,
                                     KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                    }
                    return true;
                }
            });

            ((TextView) dialog.findViewById(R.id.txt_message)).setText(GetString(R.string.text_logout));
            ((Button) dialog.findViewById(R.id.bt_positive)).setText(GetString(R.string.but_logout));
            ((Button) dialog.findViewById(R.id.bt_positive)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    show_p_Dialog();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            hide_p_Dialog();
                            //pref.clearData();
                            //mContext.deleteDatabase(DB_NAME);
                            new ClearApplicationData(mContext).execute();
                        }
                    }, 1000);


                }
            });
            ((Button) dialog.findViewById(R.id.bt_negative)).setText(GetString(R.string.but_nlogout));
            ((Button) dialog.findViewById(R.id.bt_negative)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }
    }




}


