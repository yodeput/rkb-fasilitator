package id.triwikrama.rkbfasilitator.utils;

/*
 * ******************************************************
 *  * Copyright (c) 2019. All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Created by: Yogi Dewansyah<yodeput@gmail.com>
 *  ******************************************************
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PrefManager {
    public static final String PREF_NAME = "pref";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";


    SharedPreferences pref;
    Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void clearData(){
        editor.clear().commit();
    }

    public void setLogin(boolean isLoggedIn) {
        editor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);
        editor.commit();
    }


    public void addPref(String tag, String value){
        editor.putString(tag, value);
        editor.commit();
    }

    public String getPref(String tag){
        return pref.getString(tag, "");
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public void setPINWrong(Boolean pin) {
        editor.putBoolean("PINWrong", pin);
    }



}
