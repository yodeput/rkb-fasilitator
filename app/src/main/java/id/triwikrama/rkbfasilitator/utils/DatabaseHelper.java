package id.triwikrama.rkbfasilitator.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import id.triwikrama.rkbfasilitator.model.User;


public class  DatabaseHelper extends SQLiteOpenHelper {

    Context mContext;
    private String user_id;
    private String rkb_id;
    private String user_name;
    private String user_type;
    private String user_photo;
    private String user_token;
    public static final String DB_NAME = "rkbFasilitator";
    public static final String TABLE_USER = "user";
    private static final String LOG = "DatabaseHelper";
    private static final int DB_VERSION = 1;
    private static final String KEY_ID = "id";
    private static final String KEY_USERID = "user_id";
    private static final String KEY_RKBID = "rkb_id";
    private static final String KEY_USERNAME = "user_name";
    private static final String KEY_USERTYPE = "user_type";
    private static final String KEY_PHOTO= "user_photo";
    private static final String KEY_TOKEN = "user_token";
    private static final String KEY_KELAS = "user_kelas";
    private static final String KEY_DT = "date_time";

    private static final String CREATE_TABLE_USER = "CREATE TABLE "
            + TABLE_USER + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_USERNAME + " TEXT,"
            + KEY_USERID + " TEXT,"
            + KEY_RKBID + " TEXT,"
            + KEY_USERTYPE + " TEXT,"
            + KEY_PHOTO + " TEXT,"
            + KEY_TOKEN + " TEXT,"
            + KEY_KELAS + " TEXT,"
            + KEY_DT + " TEXT" + ")";


    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE_USER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);

        onCreate(db);
    }

    public void clearData(){

    }


    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_USERID,user.getUser_id() );
        values.put(KEY_RKBID,user.getRkb_id() );
        values.put(KEY_USERNAME,user.getUser_name() );
        values.put(KEY_USERTYPE,user.getUser_type() );
        values.put(KEY_PHOTO,user.getUser_photo() );
        values.put(KEY_TOKEN,user.getUser_token() );
        values.put(KEY_KELAS,user.getUser_kelas() );
        values.put(KEY_DT,getDateTime());
        db.insert(TABLE_USER, null, values);
        db.close();
    }


    public User getUser() {
        User u = new User();
        String query = "SELECT * FROM " + TABLE_USER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {
                u.setUser_id(c.getString(c.getColumnIndex(KEY_USERID)));
                u.setRkb_id(c.getString(c.getColumnIndex(KEY_RKBID)));
                u.setUser_name(c.getString(c.getColumnIndex(KEY_USERNAME)));
                u.setUser_type(c.getString(c.getColumnIndex(KEY_USERTYPE)));
                u.setUser_photo(c.getString(c.getColumnIndex(KEY_PHOTO)));
                u.setUser_kelas(c.getString(c.getColumnIndex(KEY_KELAS)));
                u.setUser_token(c.getString(c.getColumnIndex(KEY_TOKEN)));
            } while (c.moveToNext());
        }
        db.close();
        return u;
    }

    public long getCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, TABLE_USER);
        db.close();
        return count;
    }

    public String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public String getDateShort() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd MMMM yyyy", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public int getUserType(){
        String type = getUser().getUser_type();
        switch (type){
            case "1":
                return 1;
            case"2":
                return 2;
        }
        return 2;
    }

    public String getTicket(){
        String ticket="U";
        String query = "SELECT * FROM " + TABLE_USER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {
                ticket = ticket+c.getString(c.getColumnIndex(KEY_USERID));

            } while (c.moveToNext());
        }
        db.close();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyyMMddHHmmss", Locale.getDefault());
        Date date = new Date();
        ticket = ticket+"D"+dateFormat.format(date);
        return ticket;
    }
}
