package id.triwikrama.rkbfasilitator.networking;

import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.param.AbsenParam;
import id.triwikrama.rkbfasilitator.model.param.AddKegiatanParam;
import id.triwikrama.rkbfasilitator.model.param.AddRkb;
import id.triwikrama.rkbfasilitator.model.param.AddUser;
import id.triwikrama.rkbfasilitator.model.param.DetailParam;
import id.triwikrama.rkbfasilitator.model.param.EditPhotoParam;
import id.triwikrama.rkbfasilitator.model.param.EditProfileParam;
import id.triwikrama.rkbfasilitator.model.param.EditPwdParam;
import id.triwikrama.rkbfasilitator.model.param.ListSearchParam;
import id.triwikrama.rkbfasilitator.model.param.ProfileParam;
import id.triwikrama.rkbfasilitator.model.param.ListFilterParam;
import id.triwikrama.rkbfasilitator.model.param.RekapParam;
import id.triwikrama.rkbfasilitator.model.param.TanggalParam;
import id.triwikrama.rkbfasilitator.model.response.AbsenResponse;
import id.triwikrama.rkbfasilitator.model.response.GlobalResponse;
import id.triwikrama.rkbfasilitator.model.response.DetailResponse;
import id.triwikrama.rkbfasilitator.model.response.HomeResponse;
import id.triwikrama.rkbfasilitator.model.response.LeaderboardResponse;
import id.triwikrama.rkbfasilitator.model.response.ListResponse;
import id.triwikrama.rkbfasilitator.model.param.LoginParam;
import id.triwikrama.rkbfasilitator.model.response.LoginResponse;
import id.triwikrama.rkbfasilitator.model.response.ProfileResponse;
import id.triwikrama.rkbfasilitator.model.response.RekapDetailResponse;
import id.triwikrama.rkbfasilitator.model.response.RekapExcelResponse;
import id.triwikrama.rkbfasilitator.model.response.RekapResponse;
import id.triwikrama.rkbfasilitator.model.response.RkbListResponse;
import id.triwikrama.rkbfasilitator.model.response.UserListResponse;
import id.triwikrama.rkbfasilitator.model.statistik.JenisResponse;
import id.triwikrama.rkbfasilitator.model.statistik.KelasResponse;
import id.triwikrama.rkbfasilitator.model.statistik.PerbulanResponse;
import id.triwikrama.rkbfasilitator.model.statistik.TregResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface NetworkService {

    @Headers("Content-Type: application/json")
    @POST("api/auth/login.php")
    Call<LoginResponse> Login(@Body LoginParam body);

    @POST("api/home.php")
    @Headers("Content-Type: application/json")
    Call<HomeResponse> homeData(@Header("Token") String authorization, @Body ProfileParam body);

    //PROFILE
    @POST("api/profil/view.php")
    @Headers("Content-Type: application/json")
    Call<ProfileResponse> userProfile(@Header("Token") String authorization, @Body ProfileParam body);

    @POST("api/profil/update.php")
    @Headers("Content-Type: application/json")
    Call<GlobalResponse> editProfile(@Header("Token") String authorization, @Body EditProfileParam body);

    @POST("api/profil/password.php")
    @Headers("Content-Type: application/json")
    Call<GlobalResponse> editPassword(@Header("Token") String authorization, @Body EditPwdParam body);

    @POST("api/profil/photo.php")
    @Headers("Content-Type: application/json")
    Call<GlobalResponse> editPhoto(@Header("Token") String authorization, @Body EditPhotoParam body);

    //LEADERBOARD
    @POST("api/statistik/board_user.php")
    @Headers("Content-Type: application/json")
    Call<LeaderboardResponse> leaderboard_user(@Header("Token") String authorization, @Body ProfileParam body);

    @POST("api/statistik/board_rkb.php")
    @Headers("Content-Type: application/json")
    Call<LeaderboardResponse> leaderboard_rkb(@Header("Token") String authorization, @Body RekapParam body);


    //KEGIATAN
    @POST("api/kegiatan/list.php")
    @Headers("Content-Type: application/json")
    Call<ListResponse> listKegiatan(@Header("Token") String authorization, @Body ListSearchParam body);

    @POST("api/kegiatan/list.php")
    @Headers("Content-Type: application/json")
    Call<ListResponse> listFilter(@Header("Token") String authorization, @Body ListFilterParam body);

    @POST("api/kegiatan/list.php")
    @Headers("Content-Type: application/json")
    Call<ListResponse> listSearch(@Header("Token") String authorization, @Body ListSearchParam body);

    @POST("api/kegiatan/add.php")
    @Headers("Content-Type: application/json")
    Call<GlobalResponse> addKegiatan(@Header("Token") String authorization, @Body AddKegiatanParam body);

    @POST("api/kegiatan/detail.php")
    @Headers("Content-Type: application/json")
    Call<DetailResponse> detailKegiatan(@Header("Token") String authorization, @Body DetailParam body);

    @POST("api/kegiatan/edit.php")
    @Headers("Content-Type: application/json")
    Call<GlobalResponse> editKegiatan(@Header("Token") String authorization, @Body DetailParam body);

    @POST("api/kegiatan/del.php")
    @Headers("Content-Type: application/json")
    Call<GlobalResponse> deleteKegiatan(@Header("Token") String authorization, @Body DetailParam body);
    //KEGIATAN

    @POST("api/user/list.php")
    @Headers("Content-Type: application/json")
    Call<UserListResponse> listUser(@Header("Token") String authorization, @Body ListSearchParam body);

    @POST("api/user/add.php")
    @Headers("Content-Type: application/json")
    Call<GlobalResponse> addUserAdmin(@Header("Token") String authorization, @Body AddUser bpdy);

    @POST("api/user/add.php")
    @Headers("Content-Type: application/json")
    Call<GlobalResponse> addUser(@Header("Token") String authorization, @Body AddUser bpdy);

    @POST("api/user/edit.php")
    @Headers("Content-Type: application/json")
    Call<GlobalResponse> editUserAdmin(@Header("Token") String authorization, @Body AddUser bpdy);

    @POST("api/user/edit.php")
    @Headers("Content-Type: application/json")
    Call<GlobalResponse> editUser(@Header("Token") String authorization, @Body AddUser bpdy);

    @POST("api/user/del.php")
    @Headers("Content-Type: application/json")
    Call<GlobalResponse> deleteUser(@Header("Token") String authorization, @Body User body);

    //RKB
    @POST("api/rkb/list.php")
    @Headers("Content-Type: application/json")
    Call<RkbListResponse> listRkb(@Header("Token") String authorization, @Body ListSearchParam body);

    @POST("api/rkb/add.php")
    @Headers("Content-Type: application/json")
    Call<GlobalResponse> addRkb(@Header("Token") String authorization, @Body AddRkb body);

    @POST("api/rkb/edit.php")
    @Headers("Content-Type: application/json")
    Call<GlobalResponse> editRkb(@Header("Token") String authorization, @Body AddRkb body);

    @POST("api/rkb/del.php")
    @Headers("Content-Type: application/json")
    Call<GlobalResponse> deleteRkb(@Header("Token") String authorization, @Body AddRkb body);


    //ABSEN
    @POST("api/absen/add.php")
    @Headers("Content-Type: application/json")
    Call<GlobalResponse> absenFasilitator(@Header("Token") String authorization, @Body AbsenParam body);


    //REKAP
    @POST("api/rekap/list.php")
    Call<RekapResponse> rekapList(@Header("Token") String authorization, @Body RekapParam body);

    @POST("api/rekap/detail.php")
    Call<RekapDetailResponse> rekapDetail(@Header("Token") String authorization, @Body RekapParam body);

    @POST("api/rekap/excel.php")
    Call<RekapExcelResponse> rekapExcel(@Header("Token") String authorization, @Body RekapParam body);

    //STAT
    @POST("api/statistik/jenis.php")
    Call<JenisResponse> statJenis(@Header("Token") String authorization, @Body RekapParam body);

    @POST("api/statistik/kelas.php")
    Call<KelasResponse> statKelas(@Header("Token") String authorization, @Body RekapParam body);

    @POST("api/statistik/kegiatan_perbulan.php")
    Call<PerbulanResponse> statPerbulan(@Header("Token") String authorization, @Body RekapParam body);

    @POST("api/statistik/treg.php")
    Call<TregResponse> statTreg(@Header("Token") String authorization, @Body RekapParam body);

    @POST("api/absen/list.php")
    Call<AbsenResponse> absenList(@Header("Token") String authorization, @Body TanggalParam body);



}
