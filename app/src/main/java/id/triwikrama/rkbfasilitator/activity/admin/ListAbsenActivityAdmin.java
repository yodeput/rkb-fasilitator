package id.triwikrama.rkbfasilitator.activity.admin;

import android.app.DatePickerDialog;
import android.app.Dialog;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import androidx.viewpager.widget.ViewPager;


import com.google.android.material.tabs.TabLayout;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.adapter.ListAbsenAdapter;
import id.triwikrama.rkbfasilitator.adapter.ViewPagerAdapter;
import id.triwikrama.rkbfasilitator.fragment.AbsenBelumFragment;
import id.triwikrama.rkbfasilitator.fragment.AbsenSudahFragment;
import id.triwikrama.rkbfasilitator.fragment.DetailAbsenFragment;
import id.triwikrama.rkbfasilitator.fragment.LeaderboardFragmentAdmin;
import id.triwikrama.rkbfasilitator.fragment.LeaderboardRkbFragment;
import id.triwikrama.rkbfasilitator.fragment.StatMenuFragment;
import id.triwikrama.rkbfasilitator.model.Absen;
import id.triwikrama.rkbfasilitator.model.Profile;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.param.TanggalParam;
import id.triwikrama.rkbfasilitator.model.response.AbsenResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListAbsenActivityAdmin extends AppCompatActivity {

    private DatabaseHelper db;
    private CustomDialog cd;
    private Profile profileData;
    private NetworkService mNetworkService;

    //private Toolbar toolbar;
    private TabLayout tabs;
    private ViewPager viewPager;
    private Calendar calendar;
    private int year, month, day;
    private String stringFilter;
    private TanggalParam param = new TanggalParam();

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.frame)
    FrameLayout frame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absen_admin);
        ButterKnife.bind(this);
        //toolbar = (Toolbar) findViewById(R.id.toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabs = (TabLayout) findViewById(R.id.tabs);
        initClass();
        initToolbar();
        initViewPager();

        DateFormat dateNow = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();

        stringFilter = dateNow.format(date);
        param.setTanggal(stringFilter);
        param.setPage("1");
        reqData(param);
    }

    void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("List Absen");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("List Absen");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }

    void initViewPager() {
        //tabs.setupWithViewPager(viewPager);

        tabs.addTab(tabs.newTab().setText("SUDAH"));
        tabs.addTab(tabs.newTab().setText("BELUM"));
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    reqDataSudah(param);
                } else {
                    reqDataBelum(param);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_calendar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuCalendar:
                showDialog(999);
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    String date = Integer.toString(arg3);
                    //String month = getInstance().getMonthNameByInt(arg2);

                    String month = Integer.toString(arg2 + 1);
                    if (arg2 + 1 < 10) {
                        month = "0" + Integer.toString(arg2 + 1);
                    }

                    String year = Integer.toString(arg1);

                    stringFilter = year + "-" + month + "-" + date;
                    param.setTanggal(stringFilter);
                    param.setPage("1");
                    reqData(param);
                    if (tabs.getSelectedTabPosition() == 0) {
                        reqDataSudah(param);
                    } else {
                        reqDataBelum(param);
                    }
                    //reqData(param);
                }
            };

    void initClass() {
        cd = new CustomDialog(this);
        db = new DatabaseHelper(this);
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
    }

    private void reqDataBelum(TanggalParam param) {
        param.setStatus("B");
        cd.show_p_Dialog();
        Call<AbsenResponse> reqData = mNetworkService.absenList(db.getUser().getUser_token(), param);
        reqData.enqueue(new Callback<AbsenResponse>() {
            @Override
            public void onResponse(Call<AbsenResponse> call, Response<AbsenResponse> response) {
                cd.hide_p_Dialog();
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        AbsenBelumFragment newFragment = new AbsenBelumFragment();
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("data", response.body().getData());
                        bundle.putString("tgl", param.getTanggal());
                        newFragment.setArguments(bundle);
                        getSupportFragmentManager().beginTransaction().replace(R.id.frame, newFragment).commit();
                        tabs.getTabAt(1).setText("BELUM (" + response.body().getTotal_data()+")");
                    } else {
                        ArrayList<Absen> data = new ArrayList<>();
                        AbsenBelumFragment newFragment = new AbsenBelumFragment();
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("data", data);
                        bundle.putString("tgl", param.getTanggal());
                        newFragment.setArguments(bundle);
                        getSupportFragmentManager().beginTransaction().replace(R.id.frame, newFragment).commit();

                        tabs.getTabAt(1).setText("BELUM (0)");
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }

            }

            @Override
            public void onFailure(Call<AbsenResponse> call, Throwable t) {
                cd.hide_p_Dialog();
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }

    private void reqDataSudah(TanggalParam param) {
        param.setStatus("S");
        cd.show_p_Dialog();
        Call<AbsenResponse> reqData = mNetworkService.absenList(db.getUser().getUser_token(), param);
        reqData.enqueue(new Callback<AbsenResponse>() {
            @Override
            public void onResponse(Call<AbsenResponse> call, Response<AbsenResponse> response) {
                cd.hide_p_Dialog();
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        AbsenSudahFragment newFragment = new AbsenSudahFragment();
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("data", response.body().getData());
                        bundle.putString("tgl", param.getTanggal());
                        newFragment.setArguments(bundle);
                        getSupportFragmentManager().beginTransaction().replace(R.id.frame, newFragment).commit();


                        tabs.getTabAt(0).setText("SUDAH (" + response.body().getTotal_data()+")");
                    } else {
                        AbsenSudahFragment newFragment = new AbsenSudahFragment();
                        Bundle bundle = new Bundle();
                        ArrayList<Absen> data = new ArrayList<>();
                        bundle.putParcelableArrayList("data", data);
                        bundle.putString("tgl", param.getTanggal());
                        newFragment.setArguments(bundle);
                        getSupportFragmentManager().beginTransaction().replace(R.id.frame, newFragment).commit();
                        tabs.getTabAt(0).setText("SUDAH (0)");
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }

            }

            @Override
            public void onFailure(Call<AbsenResponse> call, Throwable t) {
                cd.hide_p_Dialog();
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }

    private void reqData(TanggalParam param) {
        param.setStatus("B");
        Call<AbsenResponse> reqData = mNetworkService.absenList(db.getUser().getUser_token(), param);
        reqData.enqueue(new Callback<AbsenResponse>() {
            @Override
            public void onResponse(Call<AbsenResponse> call, Response<AbsenResponse> response) {
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        tabs.getTabAt(1).setText("BELUM (" + response.body().getTotal_data()+")");
                    } else {
                        tabs.getTabAt(1).setText("BELUM (0)");
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }

            }

            @Override
            public void onFailure(Call<AbsenResponse> call, Throwable t) {
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });

        param.setStatus("S");
        Call<AbsenResponse> reqData2 = mNetworkService.absenList(db.getUser().getUser_token(), param);
        reqData2.enqueue(new Callback<AbsenResponse>() {
            @Override
            public void onResponse(Call<AbsenResponse> call, Response<AbsenResponse> response) {
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                       tabs.getTabAt(0).setText("SUDAH (" + response.body().getTotal_data()+")");
                    } else {
                        AbsenSudahFragment newFragment = new AbsenSudahFragment();
                        Bundle bundle = new Bundle();
                        ArrayList<Absen> data = new ArrayList<>();
                        bundle.putParcelableArrayList("data", data);
                        bundle.putString("tgl", param.getTanggal());
                        newFragment.setArguments(bundle);
                        getSupportFragmentManager().beginTransaction().replace(R.id.frame, newFragment).commit();
                        tabs.getTabAt(0).setText("SUDAH (0)");
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }

            }

            @Override
            public void onFailure(Call<AbsenResponse> call, Throwable t) {
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }
}
