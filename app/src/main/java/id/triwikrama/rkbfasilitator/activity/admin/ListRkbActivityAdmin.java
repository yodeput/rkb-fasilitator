package id.triwikrama.rkbfasilitator.activity.admin;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.activity.EntryActivity;
import id.triwikrama.rkbfasilitator.adapter.ListKegiatanAdapter;
import id.triwikrama.rkbfasilitator.adapter.ListRkbAdapter;
import id.triwikrama.rkbfasilitator.fragment.DetailFragment;
import id.triwikrama.rkbfasilitator.fragment.DetailRkbFragment;
import id.triwikrama.rkbfasilitator.fragment.MyDialogCloseListener;
import id.triwikrama.rkbfasilitator.model.JenisKeg;
import id.triwikrama.rkbfasilitator.model.Kegiatan;
import id.triwikrama.rkbfasilitator.model.RkbList;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.Username;
import id.triwikrama.rkbfasilitator.model.param.ListFilterParam;
import id.triwikrama.rkbfasilitator.model.param.ListSearchParam;
import id.triwikrama.rkbfasilitator.model.response.ListResponse;
import id.triwikrama.rkbfasilitator.model.response.RkbListResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.Apps;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class ListRkbActivityAdmin extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.shimmer)
    ShimmerFrameLayout shimmer;
    @BindView(R.id.recylerview)
    RecyclerView recyclerView;
    @BindView(R.id.search_view)
    FloatingSearchView searchView;
    @BindView(R.id.txtDataKosong)
    TextView txtDataKosong;
    @BindView(R.id.fab_add)
    FloatingActionButton fab_add;

    private String src;
    private User userData;
    private NetworkService mNetworkService;
    private CustomDialog cd;
    private DatabaseHelper db;
    private final AlertDialog.Builder builder = null;

    private ListRkbAdapter mAdapter;
    private List<RkbList> rkbLists = new ArrayList<>();
    private ArrayList<String> kelas =  new ArrayList<>();
    private ArrayList<String> tr =  new ArrayList<>();
    private ListSearchParam param = new ListSearchParam();
    private String stringFilter,title="Pilih RKB";
    private int spinner1Pos = 0, spinner2Pos = 0;
    private DetailRkbFragment detailRkbFragment=null;
    private int logPage=1;

    @OnClick(R.id.fab_add)
    void addData() {
        Intent i = new Intent(this, AddRkbActivity.class);
        i.putExtra("src", "Add RKB");
        i.putStringArrayListExtra("kelas", kelas);
        i.putStringArrayListExtra("tr", tr);
        startActivity(i);
        overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_list);
        ButterKnife.bind(this);
        initClass();


        Intent intent = getIntent();
        src = intent.getStringExtra("src");
        fab_add.hide();
        if((src.equals("user"))||(src.equals("stat"))){
            title="Pilih RKB";
        } else if(src.equals("main")) {
            fab_add.show();
            title= "Daftar RKB";
        }
        initToolbar();
        param.setPage("1");
        param.setSearch("");
        param.setKelas("");
        param.setTr("");

        searchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {

            }

            @Override
            public void onSearchAction(String currentQuery) {
            param.setSearch(currentQuery);
            param.setPage("1");
            reqData(param);
            }
        });
        searchView.setOnClearSearchActionListener(new FloatingSearchView.OnClearSearchActionListener() {
            @Override
            public void onClearSearchClicked() {
                param.setPage("1");
                param.setSearch("");
                reqData(param);
            }
        });

        for(int i=0;i<7;i++){
            int trCount=i+1;
            tr.add("TR"+trCount);
        }

        kelas.add("A");
        kelas.add("B");
        kelas.add("C");


    }

    void initClass() {
        cd = new CustomDialog(this);
        db = new DatabaseHelper(this);
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
        userData = db.getUser();
    }

    private void reqData(ListSearchParam param) {
        logPage=1;
        shimmer.setVisibility(View.VISIBLE);
        shimmer.startShimmer();
        Call<RkbListResponse> reqData =  mNetworkService.listRkb(userData.getUser_token(), param);
        reqData.enqueue(new Callback<RkbListResponse>() {
            @Override
            public void onResponse(Call<RkbListResponse> call, Response<RkbListResponse> response) {
                rkbLists.clear();
                shimmer.stopShimmer();
                shimmer.setVisibility(View.GONE);
                txtDataKosong.setVisibility(View.GONE);
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        recyclerView.setVisibility(View.VISIBLE);
                        rkbLists.addAll(response.body().getData());
                        generateList(rkbLists);
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        txtDataKosong.setVisibility(View.VISIBLE);
                        txtDataKosong.setText("Data tidak ditemukan");
                        shimmer.setVisibility(View.GONE);
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }

            }

            @Override
            public void onFailure(Call<RkbListResponse> call, Throwable t) {
                shimmer.stopShimmer();
                shimmer.setVisibility(View.GONE);
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }


    private void generateList(List<RkbList> rkbLists) {

        if(src.equals("stat")){
            fab_add.hide();
            RkbList rkbAll = new RkbList();
            rkbAll.setRkb_id("all");
            rkbAll.setRkb_name("All");
            rkbAll.setRkb_treg("");
            rkbAll.setRkb_kelas("");
            List<Username> usernameList = new ArrayList<>();
            Username user = new Username();
            user.setUser_name("");
            usernameList.add(user);
            rkbAll.setUser(usernameList);
            rkbLists.add(0,rkbAll);
        }

        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setHasFixedSize(true);
        mAdapter = new ListRkbAdapter(this, rkbLists.size(), rkbLists);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new ListRkbAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RkbList obj, int position) {
                if((src.equals("addUser"))||(src.equals("stat"))){
                    Intent i = new Intent(ListRkbActivityAdmin.this, AddUserActivity.class);
                    i.putExtra("src", "addUser");
                    i.putExtra("data", obj);
                    ListRkbActivityAdmin.this.setResult(RESULT_OK,i);
                    finish();
                } else if(src.equals("listKegiatan")){
                    Intent i = new Intent(ListRkbActivityAdmin.this, AddUserActivity.class);
                    i.putExtra("src", "listKegiatan");
                    i.putExtra("data", obj);
                    ListRkbActivityAdmin.this.setResult(RESULT_OK,i);
                    finish();
                } else {
                    if (detailRkbFragment == null || !detailRkbFragment.isAdded()) {
                        detailRkbFragment = new DetailRkbFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("src", src);
                        bundle.putSerializable("data", obj);
                        bundle.putStringArrayList("kelas", kelas);
                        bundle.putStringArrayList("tr", tr);
                        detailRkbFragment.setArguments(bundle);
                        detailRkbFragment.show(getSupportFragmentManager(), detailRkbFragment.getTag());
                        MyDialogCloseListener closeListener = new MyDialogCloseListener() {
                            @Override
                            public void handleDialogClose(DialogInterface dialog) {
                                reqData(param);
                            }
                        };
                        detailRkbFragment.DismissListener(closeListener);
                    }
                }
            }
        });
        if(rkbLists.size()>7) {
            mAdapter.setOnLoadMoreListener(new ListRkbAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore(int current_page) {
                    logPage++;
                    nextPage(logPage);
                }
            });
        }
    }

    void nextPage(int current_page){
        if(current_page>1) {
            shimmer.setVisibility(View.GONE);
            mAdapter.setLoading();
            param.setPage(Integer.toString(current_page));
            reqNextData(param);
        }
    }

    private void reqNextData(ListSearchParam param) {
        Call<RkbListResponse> reqData =  mNetworkService.listRkb(userData.getUser_token(), param);
        reqData.enqueue(new Callback<RkbListResponse>() {
            @Override
            public void onResponse(Call<RkbListResponse> call, Response<RkbListResponse> response) {
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        mAdapter.insertData(response.body().getData());
                    } else {
                        mAdapter.setLoaded();
                        txtDataKosong.setVisibility(View.GONE);
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }

            }

            @Override
            public void onFailure(Call<RkbListResponse> call, Throwable t) {
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }

    void initToolbar() {
        txtDataKosong.setVisibility(View.GONE);
        this.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(title);
        if (getSupportActionBar() != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.data_filter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menuFilter:
                dialog_filter();
                break;
            default:
                break;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
    }

    @Override
    protected void onResume() {
        reqData(param);
        super.onResume();


    }

    public void dialog_filter() {
        if (builder == null) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.DialogSlideAnim);
            final View viewInflated = LayoutInflater.from(this).inflate(R.layout.dialog_filter_rkb,
                    (ViewGroup) this.findViewById(android.R.id.content), false);

            final MaterialSpinner spinnerKelas = viewInflated.findViewById(R.id.spinnerKelas);
            final MaterialSpinner spinnerTr = viewInflated.findViewById(R.id.spinnerTr);
            Button but_filter = viewInflated.findViewById(R.id.but_filter);
            Button but_clear_filter = viewInflated.findViewById(R.id.but_clear_filter);
            ImageButton but_close = viewInflated.findViewById(R.id.bt_close);
            builder.setOnKeyListener(new Dialog.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface arg0, int keyCode,
                                     KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    }
                    return true;
                }
            });

            Gson gson = new Gson();
            Type type = new TypeToken<List<JenisKeg>>() {}.getType();
            List<JenisKeg> jenisKegs = gson.fromJson(Apps.getInstance().readJenisKeg(), type);

            ArrayList<String> penjamiArray = new ArrayList<>();
            for (int i = 0; i < jenisKegs.size(); i++) {
                penjamiArray.add(jenisKegs.get(i).getNama());
                Log.e("wkwkwkwk", "ah" + jenisKegs.get(i).getNama());

            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, kelas);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerKelas.setAdapter(adapter);
            Log.e("wkwkwkwk","kelas:  "+spinner1Pos);
            spinnerKelas.setSelection(kelas.indexOf(param.getKelas()) + 1);
            spinnerKelas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    Log.e("wkwkwkwk",":  "+i);
                    if (spinner1Pos == i) {
                        return; //do nothing
                    }
                    spinner1Pos = i;

                    if (spinner1Pos == -1) {
                        param.setKelas("");
                    } else {
                      param.setKelas(kelas.get(i));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tr);
            adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerTr.setAdapter(adapter2);
            //spinnerTr.setSelection(tr.indexOf(param.getTr()) + 1);

            spinnerTr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    if (spinner2Pos == i) {
                        return; //do nothing
                    }
                    spinner2Pos = i;

                    if (spinner2Pos == -1) {
                        stringFilter="";
                    } else {
                        if (stringFilter.contains("kelas:")) {
                            param.setTr("");
                        } else {
                            param.setTr(tr.get(i));
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });


            builder.setView(viewInflated);
            final AlertDialog dialog_alert = builder.create();
            dialog_alert.show();
            dialog_alert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager.LayoutParams wmlp = dialog_alert.getWindow().getAttributes();
            wmlp.gravity = Gravity.CENTER_HORIZONTAL;

            but_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog_alert.dismiss();

                }
            });


            but_filter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog_alert.dismiss();
                    param.setSearch(searchView.getQuery());
                    param.setPage("1");
                    reqData(param);
                }
            });
            but_clear_filter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    searchView.setSearchText("");
                    param = new ListSearchParam();
                    param.setPage("1");
                    reqData(param);
                    dialog_alert.dismiss();
                }
            });
        }
    }

}
