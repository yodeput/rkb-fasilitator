package id.triwikrama.rkbfasilitator.activity.admin;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.triwikrama.rkbfasilitator.MainActivity;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.activity.LeaderboardActivity;
import id.triwikrama.rkbfasilitator.activity.ListDataActivity;
import id.triwikrama.rkbfasilitator.activity.ProfileActivity;
import id.triwikrama.rkbfasilitator.activity.admin.stat.MenuStatisticActivity;
import id.triwikrama.rkbfasilitator.model.Home;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.param.ProfileParam;
import id.triwikrama.rkbfasilitator.model.response.HomeResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import id.triwikrama.rkbfasilitator.utils.MotionAnim;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class MainActivityAdmin extends AppCompatActivity {

    @BindColor(R.color.black)
    int blackColor;
    //HOME VIEW
    @BindView(R.id.txtTanggal)
    TextView txtTanggal;
    @BindView(R.id.img_logo)
    ImageView imgLogo;
    @BindView(R.id.txtNama)
    TextView txtNama;
    @BindView(R.id.txtWelcome2)
    TextView txtWelcome2;
    @BindView(R.id.imgAvatar)
    CircularImageView imgAvatar;
    @BindViews({R.id.txtKunjungan, R.id.txtPelatihan, R.id.txtEvent, R.id.txtAdmin})
    List<TextView> txtValue;
    private String loading, appVersionCode, serverVersionCode;
    private int i = 0;
    private MotionAnim anim;
    private int dotDuration = 1000;
    private Boolean isLogin=false;
    private Boolean doubleBackToExitPressedOnce = false;
    private Boolean isClickable = true;
    private Intent intent;
    private Dialog dialog;

    private User userData;
    private CustomDialog cd;
    private DatabaseHelper db;
    private NetworkService mNetworkService;

    @OnClick(R.id.txtTanggal) void absen(){
        startActivity(new Intent(this, ListAbsenActivityAdmin.class));
        overridePendingTransition(R.anim.anim_slide_in_right,R.anim.anim_slide_out_left);
    }

    @OnClick({R.id.butKunjungan, R.id.butRkb, R.id.butUser, R.id.butStatistic,
            R.id.butRekap, R.id.butLogout, R.id.imgAvatar})
    public void setViewOnClickEvent(View view) {
        switch (view.getId()) {
            case R.id.butKunjungan:
                intent = new Intent(this, ListKegiatanActivityAdmin.class);
                intent.putExtra("src", getString(R.string.menu_admin0));
                startActivityForResult(intent,300);
                overridePendingTransition(R.anim.anim_slide_in_right,R.anim.anim_slide_out_left);
                break;
            case R.id.butRkb:
                intent = new Intent(this, ListRkbActivityAdmin.class);
                intent.putExtra("src", "main");
                startActivityForResult(intent,300);
                overridePendingTransition(R.anim.anim_slide_in_right,R.anim.anim_slide_out_left);
                break;
            case R.id.butUser:
                intent = new Intent(this, ListUserActivityAdmin.class);
                intent.putExtra("src", getString(R.string.menu_admin2));
                startActivityForResult(intent,300);
                overridePendingTransition(R.anim.anim_slide_in_right,R.anim.anim_slide_out_left);
                break;
            case R.id.butStatistic:
                intent = new Intent(this, MenuStatisticActivity.class);
                intent.putExtra("src", getString(R.string.menu_admin3));
                startActivityForResult(intent,300);
                overridePendingTransition(R.anim.anim_slide_in_right,R.anim.anim_slide_out_left);
                break;
            case R.id.butRekap:
                startActivity(new Intent(this, RekapRkbActivity.class).putExtra("src", getString(R.string.menu_admin4)));
                overridePendingTransition(R.anim.anim_slide_in_right,R.anim.anim_slide_out_left);
                break;
            case R.id.butLogout:
                cd.dialog_exit2();
                break;
            case R.id.imgAvatar:
                intent = new Intent(this, ProfileActivity.class);
                //intent.putExtra("data", profileData);
                startActivity(intent);
                overridePendingTransition(R.anim.anim_slide_in_right,R.anim.anim_slide_out_left);
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        setContentView(R.layout.activity_main_admin);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            View decor = getWindow().getDecorView();
                decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        ButterKnife.bind(this);
        initClass();
        initView();

        Log.e("ticketNumber",db.getTicket());

    }

    void initClass() {
        anim = new MotionAnim(this);
        cd = new CustomDialog(this);
        db = new DatabaseHelper(this);
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
        if(db.getCount()>0){
            isLogin = true;
        }
    }

    void initView() {
        Intent i = getIntent();
        String src = i.getStringExtra("src");
        getInstance().clearLightStatusBar(this);
        txtTanggal.setText(Html.fromHtml(getInstance().getDateTime()));
        appVersionCode = getInstance().appVersionCode();
    }

    void reqHomeData(){
        cd.show_p_Dialog();
        Call<HomeResponse> homeData = mNetworkService.homeData(db.getUser().getUser_token(),new ProfileParam(db.getUser().getUser_id()));
        homeData.enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {
                int code = response.code();
                if(code==200){
                    int success = response.body().getSuccess();
                    if(success==1){
                        Home home = response.body().getHome().get(0);
                        serverVersionCode = response.body().getVersioncode();
                        initHome(home);
                    }else {
                        cd.toastError("Terjadi kesalahan pada koneksi");
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi),R.drawable.ic_cloud_off,R.color.merah);
                }
            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==300){
            if(resultCode==RESULT_OK){
                reqHomeData();
            }
        }
    }

    void initHome(Home home) {
        initPermission();
        txtNama.setText(", "+home.getUser_name());
        txtValue.get(0).setText(home.getTotal_kunjungan());
        txtValue.get(1).setText(home.getTotal_pelatihan());
        txtValue.get(2).setText(home.getTotal_event());
        txtValue.get(3).setText(home.getTotal_adm());

        RequestOptions options;
        /*if (userData.getGender().contains("laki")) {
            options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.avatar_man)
                    .error(R.drawable.avatar_man);
        } else {
            options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.avatar_girl)
                    .error(R.drawable.avatar_girl);
        }*/
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.avatar_man)
                .error(R.drawable.avatar_man);
        Glide.with(imgAvatar)
                .load(getInstance().urlFixer(home.getUser_photo()))
                .centerCrop()
                .apply(options)
                .into(imgAvatar);
        cd.hide_p_Dialog();
        int appVC = Integer.parseInt(appVersionCode);
        //appVC = 191001;
        int serverVC = Integer.parseInt(serverVersionCode);
        if(serverVC-1 > appVC){
            dialogUpdate();
        }


    }

    void setBlackText(TextView v) {
        v.setTextColor(blackColor);
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        cd.toastWarning("Tekan kembali untuk keluar");

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void initPermission() {

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.CAMERA
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
            }
        }).check();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isLogin) {
            reqHomeData();
        }
    }

    public void dialogUpdate() {
        if(dialog==null||!dialog.isShowing()) {
            dialog = new Dialog(MainActivityAdmin.this, R.style.DialogSlideAnim);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_one_button);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER_HORIZONTAL;
            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setAttributes(lp);


            dialog.setCanceledOnTouchOutside(false);
            dialog.setOnKeyListener(new Dialog.OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface arg0, int keyCode,
                                     KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        finish();
                    }
                    return true;
                }
            });
            Drawable img = MainActivityAdmin.this.getResources().getDrawable(R.drawable.ic_close);
            int color = MainActivityAdmin.this.getResources().getColor(R.color.red_600);
            ((LinearLayout) dialog.findViewById(R.id.header_dialog)).setBackgroundColor(color);
            ((ImageView) dialog.findViewById(R.id.img_dialog)).setImageDrawable(img);
            ((TextView) dialog.findViewById(R.id.txt_message)).setText("Terdapat permbaharuan Aplikasi !\nSilahkan perbarui untuk dapat melanjutkan");
            ((Button) dialog.findViewById(R.id.bt_positive)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dialog.dismiss();
                    final String appPackageName = "id.triwikrama.rkbfasilitator";//getPackageName();
                    // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }

                }
            });
        }
    }
}
