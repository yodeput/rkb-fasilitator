package id.triwikrama.rkbfasilitator.activity.admin.stat;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.formatter.ColorFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.model.GradientColor;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.textfield.TextInputEditText;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import github.nisrulz.screenshott.ScreenShott;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.Profile;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.param.RekapParam;
import id.triwikrama.rkbfasilitator.model.statistik.KelasResponse;
import id.triwikrama.rkbfasilitator.model.statistik.Perbulan;
import id.triwikrama.rkbfasilitator.model.statistik.Treg;
import id.triwikrama.rkbfasilitator.model.statistik.TregResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.Apps;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import kotlin.random.Random;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PerTregActivity extends AppCompatActivity {

    private User userData;
    private DatabaseHelper db;
    private CustomDialog cd;
    private Profile profileData;
    private NetworkService mNetworkService;
    private String bulan, tahun;


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rootView)
    LinearLayout rootView;
    @BindView(R.id.editTahun)
    TextInputEditText editTahun;
    @BindView(R.id.barchart)
    BarChart barChart;

    @OnClick(R.id.editTahun)
    void pilihTahun() {
        showYearPicker();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.statistik_pertreg);
        ButterKnife.bind(this);
        initClass();
        initToolbar();

        Calendar today = Calendar.getInstance();
        DateFormat yearFormat = new SimpleDateFormat("yyyy");
        DateFormat monthFormat = new SimpleDateFormat("MM");
        Date date = new Date();

        tahun = yearFormat.format(date);
        bulan = monthFormat.format(date);
        editTahun.setText(tahun);
        createParam();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_screenshot, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuScreenshot:
                Bitmap bitmap = ScreenShott.getInstance().takeScreenShotOfView(rootView);
                saveScreenshot(bitmap);
                break;
            default:
                break;
        }
        return true;
    }

    private void saveScreenshot(Bitmap bitmap) {
        String filename = "statistik_per_treg" + tahun + ".jpg";
        File file = Apps.getInstance().storeImage(bitmap, filename);
        cd.toastSuccess("Screenshot tersimpan\n" + file.getAbsolutePath());
    }

    void initClass() {
        cd = new CustomDialog(this);
        db = new DatabaseHelper(this);
        userData = db.getUser();
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
    }

    void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Stats Kelas RKB");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Stats Kelas RKB");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }

    private void createParam() {
        RekapParam param = new RekapParam();
        param.setTahun(editTahun.getText().toString());
        reqData(param);
    }

    private void reqData(RekapParam param) {
        cd.show_p_Dialog();
        barChart.setVisibility(View.GONE);
        Call<TregResponse> reqKelasStat = mNetworkService.statTreg(userData.getUser_token(), param);
        reqKelasStat.enqueue(new Callback<TregResponse>() {
            @Override
            public void onResponse(Call<TregResponse> call, Response<TregResponse> response) {
                cd.hide_p_Dialog();
                int code = response.code();
                if (code == 200) {
                    if (response.body().getSuccess() == 1) {
                        generateView(response.body().getData());
                    } else {

                    }
                } else {

                    cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

                }
            }

            @Override
            public void onFailure(Call<TregResponse> call, Throwable t) {
                cd.hide_p_Dialog();
                cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }

    private void generateView(List<Treg> dataList) {


        barChart.setVisibility(View.VISIBLE);
        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);
        barChart.setTouchEnabled(true);
        barChart.setPinchZoom(true);
        barChart.setDrawGridBackground(false);
        barChart.setHorizontalScrollBarEnabled(true);

        List<Perbulan> perbulan = dataList.get(0).getData();
        final String[] bulan = new String[perbulan.size()];
        List<String> treg = new ArrayList<>();
        for (int i = 0; i < dataList.size(); i++) {
            treg.add(dataList.get(i).getTr());
        }

        for (int i = 0; i < perbulan.size(); i++) {
            bulan[i]=perbulan.get(i).getBulan();
        }


        int blue = ContextCompat.getColor(this, R.color.blue_600);
        int orange = ContextCompat.getColor(this, R.color.orange_600);
        int grey = ContextCompat.getColor(this, R.color.grey_400);
        int green = ContextCompat.getColor(this, R.color.green_600);
        int cyan = ContextCompat.getColor(this, R.color.cyan_200);
        int brown = ContextCompat.getColor(this, R.color.brown_400);
        int purple = ContextCompat.getColor(this, R.color.deep_purple_400);
        int yellow = ContextCompat.getColor(this, R.color.yellow_400);
        int red = ContextCompat.getColor(this, R.color.red_600);
        int green2 = ContextCompat.getColor(this, R.color.light_green_500);
        int indigo = ContextCompat.getColor(this, R.color.indigo_200);
        int teal = ContextCompat.getColor(this, R.color.teal_300);

        int[] colors = {blue, orange, grey, green, cyan, purple, yellow, teal, red, indigo, brown, green2};


        List<BarEntry>[] barValues = new ArrayList[bulan.length];
        BarDataSet[] barSets = new BarDataSet[bulan.length];
        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        for (int i = 0; i < bulan.length; i++) {
            barValues[i] = new ArrayList<>();
            List<Perbulan> perbulan2 = dataList.get(i).getData();
            for (int a = 0; a < treg.size(); a++) {
                barValues[i].add(new BarEntry(a, Integer.parseInt(perbulan2.get(i).getValue())));

            }
            barSets[i] = new BarDataSet(barValues[i], bulan[i]);
            barSets[i].setColors(colors[i]);
            dataSets.add(barSets[i]);
        }

        float groupCount = (float) treg.size();
        float groupChild = (float) bulan.length;
        float groupSpace = 0.1f;
        float barSpace = 0.0f;
        float barWidth = ((1f - groupSpace - barSpace) / groupChild);
        Log.e("wkwkwkwk-barWidth", "" + barWidth);

        BarData data = new BarData(dataSets);
        barChart.setData(data);
        barChart.getBarData().setBarWidth(barWidth);
        barChart.getXAxis().setAxisMaximum(barChart.getBarData().getGroupWidth(groupSpace, barSpace) * groupCount);
        barChart.getXAxis().setAxisMinimum(0);
        barChart.groupBars(0, groupSpace, barSpace);
        barChart.getDescription().setEnabled(false);
        barChart.getLegend().setEnabled(true);

        YAxis yAxis = barChart.getAxisLeft();
        yAxis.setDrawGridLines(false);
        yAxis.setSpaceTop(20f);
        yAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true
        barChart.getAxisRight().setEnabled(false);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setCenterAxisLabels(true);
        xAxis.setDrawGridLines(true);
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setAxisMinValue(0f);
        xAxis.setAxisMaximum(groupCount);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(treg));

        Legend legend = barChart.getLegend();
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        legend.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        legend.setDrawInside(true);
        legend.setYOffset(20f);
        legend.setXOffset(0f);
        legend.setYEntrySpace(0f);
        legend.setTextSize(10f);
        barChart.animateX(1200, Easing.EaseInBounce);
        barChart.invalidate();

    }

    private void showYearPicker() {
        Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(PerTregActivity.this,
                new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) {
                        tahun = Integer.toString(selectedYear);
                        editTahun.setText(tahun);
                        createParam();
                    }
                }, today.get(Calendar.YEAR), Integer.parseInt(bulan) - 1);
        builder.setMinYear(2019)
                .setActivatedYear(Integer.parseInt(tahun))
                .setMaxYear(2030)
                .showYearOnly()
                .build()
                .show();

    }
}
