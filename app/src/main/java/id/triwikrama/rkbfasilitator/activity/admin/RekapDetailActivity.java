package id.triwikrama.rkbfasilitator.activity.admin;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.rmondjone.locktableview.LockTableView;
import com.rmondjone.xrecyclerview.ProgressStyle;
import com.rmondjone.xrecyclerview.XRecyclerView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.Profile;
import id.triwikrama.rkbfasilitator.model.Rekap;
import id.triwikrama.rkbfasilitator.model.RekapDetail;
import id.triwikrama.rkbfasilitator.model.RekapExcel;
import id.triwikrama.rkbfasilitator.model.param.RekapParam;
import id.triwikrama.rkbfasilitator.model.response.RekapDetailResponse;
import id.triwikrama.rkbfasilitator.model.response.RekapExcelResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import id.triwikrama.rkbfasilitator.utils.PrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RekapDetailActivity extends AppCompatActivity {

    private DatabaseHelper db;
    private CustomDialog cd;
    private PrefManager pref;
    private NetworkService mNetworkService;
    private Intent intent;
    private Rekap rekap;
    private String bulan, tahun;

    @BindView(R.id.contentView)
    LinearLayout contentView;
    @BindView(R.id.txtRkbName)
    TextView txtRkbName;
    @BindView(R.id.txtTotal)
    TextView txtTotal;
    @BindView(R.id.txtBulanTahun)
    TextView txtBulanTahun;

    @OnClick(R.id.bt_close)void close(){
        finish();
        overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);

    }

    @OnClick(R.id.but_download)void download(){
        RekapParam param = new RekapParam();
        param.setBulan(bulan);
        param.setTahun(tahun);
        param.setRkb_id(rekap.getRkbId());
        downloadExcel(param);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rekap_detail);
        ButterKnife.bind(this);
        initClass();
        //initToolbar();
        Intent i = getIntent();
        bulan = i.getStringExtra("bulan");
        tahun = i.getStringExtra("tahun");
        rekap = (Rekap) i.getSerializableExtra("data");
        txtBulanTahun.setText(bulan+" / "+tahun);
        txtRkbName.setText(rekap.getRkbName());
        txtTotal.setText(rekap.getTotalKegiatan());
        createParam(bulan,tahun,rekap);

    }

    void initClass() {
        cd = new CustomDialog(this);
        pref = new PrefManager(this);
        db = new DatabaseHelper(this);
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
    }

    private void createParam(String bulan, String tahun, Rekap rekap){
        RekapParam param = new RekapParam();
        param.setBulan(bulan);
        param.setTahun(tahun);
        param.setRkb_id(rekap.getRkbId());
        reqData(param);
    }

    private void reqData(RekapParam param){
        cd.show_p_Dialog();
        Call<RekapDetailResponse> reqDetail = mNetworkService.rekapDetail(db.getUser().getUser_token(),param);
        reqDetail.enqueue(new Callback<RekapDetailResponse>() {
            @Override
            public void onResponse(Call<RekapDetailResponse> call, Response<RekapDetailResponse> response) {
                cd.hide_p_Dialog();
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        generateData(response.body().getData());
                    } else {
                        cd.toastError("Rekap Kegiatan tidak ditemukan");
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }
            }

            @Override
            public void onFailure(Call<RekapDetailResponse> call, Throwable t) {
                cd.hide_p_Dialog();
            }
        });
    }

    private void generateData(List<RekapDetail> detail){
        ArrayList<ArrayList<String>> mTableDatas = new ArrayList<ArrayList<String>>();
        ArrayList<String> mfristData = new ArrayList<String>();
        mfristData.add("Tanggal");
        mfristData.add("Kunjungan");
        mfristData.add("Pelatihan");
        mfristData.add("Event");
        mfristData.add("Administrasi");
        mTableDatas.add(mfristData);

        for (int i = 0; i < detail.size(); i++) {
            ArrayList<ArrayList<String>> tglDatas = new ArrayList<ArrayList<String>>();
            tglDatas.add(detail.get(i).getList());
            mTableDatas.add(tglDatas.get(0));
        }
        final LockTableView mLockTableView = new LockTableView(this, contentView, mTableDatas);
        mLockTableView.setLockFristColumn(true)
                .setLockFristRow(true)
                .setColumnWidth( 0 , 30 )
                .setMinRowHeight(20)
                .setMaxRowHeight(30)
                .setTextViewSize(10)
                .setFristRowBackGroudColor(R.color.grey_100)
                .setTableHeadTextColor(R.color.black)
                .setTableContentTextColor(R.color.border_color)
                .setCellPadding(5)
                .setNullableString("N/A")
                .setTableViewListener(new LockTableView.OnTableViewListener() {
                    @Override
                    public void onTableViewScrollChange(int x, int y) {
//                     \
                    }
                })
                .setTableViewRangeListener(new LockTableView.OnTableViewRangeListener() {
                    @Override
                    public void onLeft(HorizontalScrollView view) {

                    }

                    @Override
                    public void onRight(HorizontalScrollView view) {

                    }
                })
                .setOnItemClickListenter(new LockTableView.OnItemClickListenter() {
                    @Override
                    public void onItemClick(View item, int position) {

                    }
                })
                .setOnItemLongClickListenter(new LockTableView.OnItemLongClickListenter() {
                    @Override
                    public void onItemLongClick(View item, int position) {

                    }
                })
                .setOnItemSeletor(R.color.dashline_color)//设置Item被选中颜色
                .show();
        mLockTableView.getTableScrollView().setPullRefreshEnabled(true);
        mLockTableView.getTableScrollView().setLoadingMoreEnabled(false);
        mLockTableView.getTableScrollView().setRefreshProgressStyle(ProgressStyle.SquareSpin);
    }

    private void downloadExcel(RekapParam param){
        Call<RekapExcelResponse> download = mNetworkService.rekapExcel(db.getUser().getUser_token(),param);
        download.enqueue(new Callback<RekapExcelResponse>() {
            @Override
            public void onResponse(Call<RekapExcelResponse> call, Response<RekapExcelResponse> response) {
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        List<RekapExcel> file = response.body().getData();
                        for(int i=0;i<file.size();i++) {
                            downloadFile(file.get(i).getName_file());
                        }
                    } else {
                        cd.toastError("Rekap Kegiatan tidak ditemukan");
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }
            }

            @Override
            public void onFailure(Call<RekapExcelResponse> call, Throwable t) {

            }
        });
    }

    private void downloadFile(String url) {
        final ProgressDialog progressBarDialog = new ProgressDialog(this);
        progressBarDialog.setTitle("Unduh Jenis Rekap");

        progressBarDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressBarDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                cd.toastSuccess("Unduh Jenis Rekap berhasil");
            }
        });
        progressBarDialog.setCanceledOnTouchOutside(false);
        progressBarDialog.setProgress(0);

        DownloadManager downloadmanager = (DownloadManager) this.getSystemService(Context.DOWNLOAD_SERVICE);

        Log.e("wkwkwkww", url);
        //namaKegiatan = namaKegiatan.replace(" ","_");
        Uri uri = Uri.parse(url);
        String fileName = url.substring(url.lastIndexOf('/') + 1);
        fileName = db.getDateShort()+"-"+fileName;
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setTitle("Download Excel " + rekap.getRkbName());
        request.setDescription("Downloading");
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setVisibleInDownloadsUi(false);
        request.setAllowedOverMetered(true);

        String folder = Environment.getExternalStorageDirectory() + File.separator + "RKB" + File.separator;

        //Create androiddeft folder if it does not exist
        File directory = new File(folder);
        if (!directory.exists()) {
            directory.mkdirs();
        }

        File file = new File(folder + fileName);
        if (file.exists()) {
            file.delete();
        }

        request.setDestinationUri(Uri.parse("file://" + folder + fileName));

        final long downloadId = downloadmanager.enqueue(request);
        new Thread(new Runnable() {

            @Override
            public void run() {

                boolean downloading = true;

                DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                while (downloading) {

                    DownloadManager.Query q = new DownloadManager.Query();
                    q.setFilterById(downloadId); //filter by id which you have receieved when reqesting download from download manager
                    Cursor cursor = manager.query(q);
                    cursor.moveToFirst();
                    int bytes_downloaded = cursor.getInt(cursor
                            .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                    int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));

                    if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                        downloading = false;
                    }

                    final int dl_progress = (int) ((bytes_downloaded * 100l) / bytes_total);

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            progressBarDialog.setProgress((int) dl_progress);
                            if (dl_progress==100){
                                progressBarDialog.dismiss();
                            }
                        }
                    });

                    cursor.close();
                }

            }

        }).start();

        progressBarDialog.show();

    }

    /*void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Rincian RekapExcel RKB");
        toolbar.setBackgroundColor(getResources().getColor(R.color.white));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Rincian RekapExcel RKB");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }*/

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);

    }
}
