package id.triwikrama.rkbfasilitator.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.adapter.AddImageAdapter;
import id.triwikrama.rkbfasilitator.fragment.ViewImageFragment;
import id.triwikrama.rkbfasilitator.model.AddImage;
import id.triwikrama.rkbfasilitator.model.Photo;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.param.AddKegiatanParam;
import id.triwikrama.rkbfasilitator.model.response.GlobalResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.utils.Apps;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import id.triwikrama.rkbfasilitator.utils.GpsUtils;
import id.triwikrama.rkbfasilitator.utils.PrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class EntryActivity extends AppCompatActivity implements AddImageAdapter.ImageListListener ,AddImageAdapter.ImageDeleted {

    private User userData;
    private CustomDialog cd;
    private DatabaseHelper db;
    private PrefManager pref;
    private NetworkService mNetworkService;

    private static final int RC_CAMERA = 3000;
    private static RecyclerView.Adapter adapter;
    private static ArrayList<AddImage> data;


    @BindView(R.id.editNamaKunjungan)
    AppCompatEditText editNamaKunjungan;
    @BindView(R.id.editDeskripsi)
    AppCompatEditText editDeskripsi;
    @BindView(R.id.editInstansi)
    AppCompatEditText editInstansi;
    @BindView(R.id.editJumlah)
    AppCompatEditText editJumlah;
    @BindView(R.id.editLokasi)
    AppCompatEditText editLokasi;
    @BindView(R.id.txt_nb)
    TextView txt_nb;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txtNoImage)
    TextView txtNoImage;
    @BindView(R.id.imgNoImages)
    ImageView imgNoImages;
    @BindView(R.id.recyclerView)RecyclerView recyclerView;

    private GoogleMap googleMap;
    private FusedLocationProviderClient fusedLocationClient;
    private SupportMapFragment mapFragment;
    private int PLACE_PICKER_REQUEST = 0;
    private LatLng latlong = new LatLng(0, 0);
    //private LatLng latlong = new LatLng(-6.9276784, 107.5950101);//Bandung
    private String src;
    private boolean isContinue = false;
    private boolean isGPS = false;


    private ArrayList<Image> images = new ArrayList<>();
    private ArrayList<Image> images2 = new ArrayList<>();
    private AddKegiatanParam param = new AddKegiatanParam();
    private List<Photo> photo;
    private int IMAGE_PICKER_REQUEST = 553;
    private RecyclerView.LayoutManager layoutManager;
    private boolean submitButton=true;

    @OnClick(R.id.fabAddImage) void addImage(){
        captureImage();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_entry);
        ButterKnife.bind(this);
        initClass();
        initToolbar();

        new GpsUtils(this).turnGPSOn(new GpsUtils.onGpsListener() {
            @Override
            public void gpsStatus(boolean isGPSEnable) {
                isGPS = isGPSEnable;
                initMap();
            }
        });

    }

    void initClass() {
        cd = new CustomDialog(this);
        pref = new PrefManager(this);
        db = new DatabaseHelper(this);
        userData = db.getUser();
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
    }

    void initToolbar(){
        Intent i = getIntent();
        src = i.getStringExtra("src");
        this.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Entry "+src);
        editNamaKunjungan.setHint("Nama "+src);
        if (getSupportActionBar() != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Entry "+src);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });

        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if(src.equals("Event")){
            editInstansi.setVisibility(View.VISIBLE);
            editJumlah.setVisibility(View.VISIBLE);
            txt_nb.setVisibility(View.VISIBLE);
        } else if(src.equals("Pelatihan")){
            editJumlah.setVisibility(View.VISIBLE);
            txt_nb.setVisibility(View.VISIBLE);
        }
    }


    private void initMap() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                EntryActivity.this.googleMap = googleMap;
                fusedLocationClient = LocationServices.getFusedLocationProviderClient(EntryActivity.this);
                fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            showMarker(location);
                        }
                    }
                });
            }
        });
    }

    private void showMarker(@NonNull Location currentLocation) {
        latlong = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        Marker pinnedMarker = googleMap.addMarker(new MarkerOptions().position(latlong).title("Lokasi Anda").draggable(true));
        googleMap.getUiSettings().setScrollGesturesEnabled(false);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latlong));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlong, 15));

    }


    private void setParam(){
        if((latlong.latitude==0)||(latlong.longitude==0)) {
            cd.oneButtonDialog("Sepertinya Anda mematikan fitur GPS\nSilahkan hidupkan untuk melanjutkan Entri Kegiatan",R.drawable.ic_location_on_black_24dp, R.color.red);
        } else {
            if ((!getInstance().isEmpty(editNamaKunjungan)) || (!getInstance().isEmpty(editDeskripsi)) || (!getInstance().isEmpty(editLokasi))) {

                if (images2.size() > 0) {
                    photo = new ArrayList<>();
                    for (int i = 0; i < images2.size(); i++) {
                        String base64JPG = Apps.getInstance().JpgToString(images2.get(i).getPath());
                        Photo p = new Photo(base64JPG);
                        photo.add(p);
                    }
                    param.setType(getInstance().getType(src));
                    param.setRkb_id(userData.getRkb_id());
                    param.setUser_id(userData.getUser_id());
                    param.setName(editNamaKunjungan.getText().toString());
                    param.setDesc(editDeskripsi.getText().toString());
                    param.setLokasi(editLokasi.getText().toString());
                    param.setLat(Double.toString(latlong.latitude));
                    param.setLng(Double.toString(latlong.longitude));
                    param.setTicket(db.getTicket());
                    param.setPhoto(photo);
                    //Log.e("wkwkwkw", param.toString());
                    addKegiatan(param);
                } else {
                    cd.toastWarning("Anda belum menambahkan Foto Kegiatan");
                }
            } else {
                cd.toastWarning("Isi Form dengan lenkap");
            }
        }
    }

    private void setParamEvent(){
        if ((!getInstance().isEmpty(editNamaKunjungan)) || (!getInstance().isEmpty(editDeskripsi)) || (!getInstance().isEmpty(editLokasi))|| (!getInstance().isEmpty(editInstansi)))
        {

            if (images2.size() > 0) {
                photo = new ArrayList<>();
                for (int i = 0; i < images2.size(); i++) {
                    String base64JPG = Apps.getInstance().JpgToString(images2.get(i).getPath());
                    Photo p = new Photo(base64JPG);
                    photo.add(p);
                }
                param.setType(getInstance().getType(src));
                param.setRkb_id(userData.getRkb_id());
                param.setUser_id(userData.getUser_id());
                param.setName(editNamaKunjungan.getText().toString());
                param.setDesc(editDeskripsi.getText().toString());
                param.setLokasi(editLokasi.getText().toString());
                param.setInstansi(editInstansi.getText().toString());
                param.setLat(Double.toString(latlong.latitude));
                param.setLng(Double.toString(latlong.longitude));
                param.setPhoto(photo);
                param.setTicket(db.getTicket());
                param.setJml_peserta(editJumlah.getText().toString());
                //Log.e("wkwkwkw", param.toString());
                    addKegiatan(param);

            } else {
                cd.toastWarning("Anda belum menambahkan Foto Kegiatan");
            }
        }else {
            cd.toastWarning("Isi Form dengan lenkap");
        }
    }

    private void setParamPelatihan(){
        if ((!getInstance().isEmpty(editNamaKunjungan)) || (!getInstance().isEmpty(editDeskripsi)) || (!getInstance().isEmpty(editLokasi))|| (!getInstance().isEmpty(editInstansi)))
        {

            if (images2.size() > 0) {
                photo = new ArrayList<>();
                for (int i = 0; i < images2.size(); i++) {
                    String base64JPG = Apps.getInstance().JpgToString(images2.get(i).getPath());
                    Photo p = new Photo(base64JPG);
                    photo.add(p);
                }
                param.setType(getInstance().getType(src));
                param.setRkb_id(userData.getRkb_id());
                param.setUser_id(userData.getUser_id());
                param.setName(editNamaKunjungan.getText().toString());
                param.setDesc(editDeskripsi.getText().toString());
                param.setLokasi(editLokasi.getText().toString());
                param.setLat(Double.toString(latlong.latitude));
                param.setLng(Double.toString(latlong.longitude));
                param.setPhoto(photo);
                param.setTicket(db.getTicket());
                param.setJml_peserta(editJumlah.getText().toString());
                //Log.e("wkwkwkw", param.toString());
                addKegiatan(param);

            } else {
                cd.toastWarning("Anda belum menambahkan Foto Kegiatan");
            }
        }else {
            cd.toastWarning("Isi Form dengan lenkap");
        }
    }

    private void addKegiatan(AddKegiatanParam param){
        Log.e("addKegiatan","true");
        String token = userData.getUser_token();
        Call<GlobalResponse> addKegiatan = mNetworkService.addKegiatan(token,param);;
        addKegiatan.enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                cd.hide_p_Dialog();
                submitButton=true;
                int code = response.code();
                if(code==200){
                    int success = response.body().getSuccess();
                    if(success==1){
                        cd.oneButtonDialog_finish(response.body().getMessage(),R.drawable.ic_check_black_24dp,R.color.green_700);
                    }else {
                        cd.oneButtonDialog(response.body().getMessage(),R.drawable.ic_user,R.color.merah);
                    }
                } else {
                    cd.oneButtonDialog(getString(R.string.error_koneksi),R.drawable.ic_cloud_off,R.color.merah);
                }
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                submitButton=true;
                cd.hide_p_Dialog();
                cd.oneButtonDialog(getString(R.string.error_koneksi),R.drawable.ic_cloud_off,R.color.merah);

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.form_submit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSubmit:
                    if (submitButton = true) {
                        submitButton = false;
                        cd.show_p_Dialog();
                        if (src.equals("Event")) {
                            setParamEvent();
                        } else if (src.equals("Pelatihan")) {
                            setParamPelatihan();
                        } else {
                            setParam();
                        }
                    } else {
                        cd.toastWarning("Pengiriman data sedang dalam proses");
                    }
                break;
            default:
                break;
        }
        return true;
    }

    public void dialog_pilih_sumber() {
        final Dialog dialog = new Dialog(this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_pick_image_option);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER_HORIZONTAL;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);


        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss();
                }
                return true;
            }
        });

        ((CardView) dialog.findViewById(R.id.butCamera)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        ((CardView) dialog.findViewById(R.id.butGallery)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openImageSelect();
            }
        });
    }

    void openImageSelect(){
        ImagePicker.create(this)
                //.returnMode(ReturnMode.ALL)
                .folderMode(true)
                .toolbarFolderTitle("Folder")
                .toolbarImageTitle("Tap to select")
                .toolbarArrowColor(Color.BLACK)
                .includeVideo(false)
                .limit(10)
                .multi()
                .imageDirectory("Camera")
                .origin(images)
                .exclude(images)
                .theme(R.style.ImagePickerTheme)
                .enableLog(false)
                .start();
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        Log.e("onActivityResult",""+requestCode);
        if (requestCode == IMAGE_PICKER_REQUEST){
            if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
                images = (ArrayList<Image>) ImagePicker.getImages(data);
                images2.add(images.get(0));
                printImages(images2);
                return;
            }
        } else if (requestCode == 150) {
            isGPS = true;
            initMap();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void printImages(List<Image> images) {
        if (images == null) return;
        txtNoImage.setVisibility(View.GONE);
        imgNoImages.setVisibility(View.GONE);

        adapter = new AddImageAdapter(this,images,this,this);
        recyclerView.setAdapter(adapter);

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RC_CAMERA) {
            if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                captureImage();
            }
        } else if(requestCode==150){
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                initMap();
            } else {
                Toast.makeText(EntryActivity.this, "Location Permission denied", Toast.LENGTH_SHORT).show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    private void captureImage() {
        ImagePicker.cameraOnly().start(this);
    }

    @Override
    public void onImageListSelected(Image image) {
        /*ViewImageFragment newFragment = new ViewImageFragment();
        Bundle bundle = new Bundle();
        bundle.putString("url", image.getPath());
        newFragment.setArguments(bundle);
        newFragment.show(getSupportFragmentManager(), newFragment.getTag());*/

    }

    @Override
    public void onImageDeleted(Image image) {
        images2.remove(image);
        printImages(images2);
    }

}
