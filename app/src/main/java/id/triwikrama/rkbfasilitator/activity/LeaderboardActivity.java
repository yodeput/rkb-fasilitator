package id.triwikrama.rkbfasilitator.activity;

import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.triwikrama.rkbfasilitator.activity.admin.RekapRkbActivity;
import id.triwikrama.rkbfasilitator.model.Profile;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.adapter.LeaderboardAdapter;
import id.triwikrama.rkbfasilitator.model.Leaderboard;
import id.triwikrama.rkbfasilitator.model.param.ProfileParam;
import id.triwikrama.rkbfasilitator.model.response.LeaderboardResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.utils.Apps;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import id.triwikrama.rkbfasilitator.utils.PrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class LeaderboardActivity extends AppCompatActivity implements LeaderboardAdapter.LeaderboardListListener {

    private CustomDialog cd;
    private DatabaseHelper db;
    private PrefManager pref;
    private LeaderboardAdapter mAdapter;

    @BindView(R.id.txtTanggal)
    TextView txtTanggal;
    @BindView(R.id.motionLayout)
    MotionLayout motionLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindViews({R.id.layoutLeaderBoard,R.id.layoutContainer, R.id.layoutPresentase})
    List<LinearLayout> ll_layouts;
    @BindView(R.id.layoutLabel)
    ConstraintLayout layoutLabel;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.imgAvatar)
    CircularImageView imgAvatar;
    @BindView(R.id.txtUserName)
    TextView txtUserName;
    @BindViews({R.id.imgStar1, R.id.imgStar2, R.id.imgStar3, R.id.imgStar4, R.id.imgStar5})
    List<ImageView> imgStars;
    private NetworkService mNetworkService;
    private List<Leaderboard> leaderboardList;
    private String bulan, tahun;
    private int juara;

    @OnClick(R.id.txtTanggal) void picker(){
        showPicker();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_leaderboard);
        ButterKnife.bind(this);
        initClass();
        initToolbar();
        initAvatar();

        DateFormat yearFormat = new SimpleDateFormat("yyyy");
        DateFormat monthFormat = new SimpleDateFormat("MM");
        Date date = new Date();

        tahun = yearFormat.format(date);
        bulan = monthFormat.format(date);
        createPram();
        txtTanggal.setText(Html.fromHtml(Apps.getInstance().getMonth()));
    }

    void initClass() {
        cd = new CustomDialog(this);
        pref = new PrefManager(this);
        db = new DatabaseHelper(this);
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
    }

    private void showPicker() {
        Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(LeaderboardActivity.this,
                new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) {
                        bulan = Integer.toString(selectedMonth+1);
                        bulan = "0"+bulan;
                        tahun = Integer.toString(selectedYear);
                        createPram();
                        txtTanggal.setText(Html.fromHtml(Apps.getInstance().getMonthLeaderboard(selectedMonth+1, selectedYear)));
                    }
                },today.get(Calendar.YEAR),Integer.parseInt(bulan)-1);
        builder.setMinYear(2019)
                .setActivatedYear(Integer.parseInt(tahun))
                .setMaxYear(2030)
                .build()
                .show();
    }

    void createPram(){
        ProfileParam param = new ProfileParam();
        param.setUser_id(db.getUser().getUser_id());
        param.setBulan(bulan);
        param.setTahun(tahun);

        reqData(param);
    }

    void reqData(ProfileParam param) {
        Call<LeaderboardResponse> leaderboard = mNetworkService.leaderboard_user(db.getUser().getUser_token(),param);
        leaderboard.enqueue(new Callback<LeaderboardResponse>() {
            @Override
            public void onResponse(Call<LeaderboardResponse> call, Response<LeaderboardResponse> response) {
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        juara = response.body().getRanking_saya();
                        generateData(response.body().getData(), response.body().getRanking_saya());
                        visible();
                    } else {
                        gone();
                        cd.oneButtonDialog("Rekap tidak ditemukan",R.drawable.ic_close, R.color.red_600);
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }
            }

            @Override
            public void onFailure(Call<LeaderboardResponse> call, Throwable t) {
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
            }
        });
    }

    void initAvatar(){
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.avatar_leaderboard)
                .error(R.drawable.avatar_leaderboard);
        Glide.with(imgAvatar)
                .load(R.drawable.avatar_leaderboard)
                .centerCrop()
                .apply(options)
                .into(imgAvatar);
    }

    void generateData(List<Leaderboard> leaderboardList, int juara) {
        txtUserName.setText(leaderboardList.get(0).getUser_name());
        mAdapter = new LeaderboardAdapter(this, leaderboardList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        if (juara == 1) {
            juara1Star();
        } else {
            juara2Star();
        }
    }

    void juara1Star()
    {
        for(int i=0;i<imgStars.size();i++){
            imgStars.get(i).setVisibility(View.VISIBLE);
        }
        imgStars.get(0).setImageDrawable(getResources().getDrawable(R.drawable.ic_star1));
    }

    void juara2Star()
    {
        for(int i=1;i<imgStars.size();i++){
            imgStars.get(i).setVisibility(View.GONE);
        }
        imgStars.get(0).setImageDrawable(getResources().getDrawable(R.drawable.ic_star2));
    }

    void gone()
    {
        for(int i=0;i<imgStars.size();i++){
            imgStars.get(i).setVisibility(View.GONE);
        }
        for(int i=0;i<ll_layouts.size();i++){
            ll_layouts.get(i).setVisibility(View.GONE);
        }
        txtUserName.setVisibility(View.GONE);
        layoutLabel.setVisibility(View.GONE);
    }


    void visible()
    {
        for(int i=0;i<imgStars.size();i++){
            imgStars.get(i).setVisibility(View.VISIBLE);
        }
        for(int i=0;i<ll_layouts.size();i++){
            ll_layouts.get(i).setVisibility(View.VISIBLE);
        }
        txtUserName.setVisibility(View.VISIBLE);
        layoutLabel.setVisibility(View.VISIBLE);
    }
    void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Leaderboards");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Leaderboards");
            ;
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }

    @Override
    public void onLeaderboardSelected(Leaderboard leaderboard) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
    }
}
