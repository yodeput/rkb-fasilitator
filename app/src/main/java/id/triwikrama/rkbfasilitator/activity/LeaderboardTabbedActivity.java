package id.triwikrama.rkbfasilitator.activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.adapter.ViewPagerAdapter;
import id.triwikrama.rkbfasilitator.fragment.LeaderboardUserFragment;
import id.triwikrama.rkbfasilitator.fragment.LeaderboardRkbFragment;
import id.triwikrama.rkbfasilitator.model.Profile;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;

public class LeaderboardTabbedActivity extends AppCompatActivity {

    private DatabaseHelper db;
    private CustomDialog cd;
    private Profile profileData;
    private NetworkService mNetworkService;

    //private Toolbar toolbar;
    private TabLayout tabs;
    private ViewPager viewPager;

    @BindView(R.id.toolbar)Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board);
        ButterKnife.bind(this);
        //toolbar = (Toolbar) findViewById(R.id.toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabs = (TabLayout) findViewById(R.id.tabs);
        initToolbar();
        initViewPager();
    }

    void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Leaderboard");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Leaderboard");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }

    void initViewPager(){
        tabs.setupWithViewPager(viewPager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new LeaderboardUserFragment(), "User");
        adapter.addFragment(new LeaderboardRkbFragment(), "RKB");
        viewPager.setAdapter(adapter);
    }

}
