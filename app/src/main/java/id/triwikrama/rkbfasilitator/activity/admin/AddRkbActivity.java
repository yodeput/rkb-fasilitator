package id.triwikrama.rkbfasilitator.activity.admin;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;

import com.toptoche.searchablespinnerlibrary.SearchableListDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.RkbList;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.param.AddRkb;
import id.triwikrama.rkbfasilitator.model.response.GlobalResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import id.triwikrama.rkbfasilitator.utils.PrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddRkbActivity extends AppCompatActivity {

    private User userData;
    private CustomDialog cd;
    private DatabaseHelper db;
    private PrefManager pref;
    private NetworkService mNetworkService;
    private String src, sKelas, sTr;
    private int spinnerPos = -1;
    private ArrayList<String> kelas =  new ArrayList<>();
    private ArrayList<String> tr =  new ArrayList<>();
    private RkbList rkb = new RkbList();
    private int spinner1Pos = 0, spinner2Pos = 0;
    private boolean submitButton=true;

    @BindView(R.id.editNama)
    AppCompatEditText editNama;
    @BindView(R.id.editKelas)
    AppCompatEditText editKelas;
    @BindView(R.id.editTR)
    AppCompatEditText editTR;
    @BindView(R.id.editGmv)
    AppCompatEditText editGmv;


    @BindView(R.id.toolbar)
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_rkb);
        ButterKnife.bind(this);
        initClass();
        Intent i = getIntent();
        src = i.getStringExtra("src");
        initToolbar();

        kelas = i.getStringArrayListExtra("kelas");
        tr = i.getStringArrayListExtra("tr");

        if(src.contains("Edit")){
            rkb = (RkbList) i.getSerializableExtra("data");
            editNama.setText(rkb.getRkb_name());
            editGmv.setText(rkb.getRkb_gmv());
            editKelas.setText(rkb.getRkb_kelas());
            editTR.setText(rkb.getRkb_treg());
        }

        editKelas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    SearchableListDialog list = SearchableListDialog.newInstance(kelas);
                    list.setOnSearchableItemClickListener(new SearchableListDialog.SearchableItem() {
                        @Override
                        public void onSearchableItemClicked(Object item, int position) {
                            editKelas.setText(item.toString());
                        }
                    });
                    list.setTitle("Pilih Kelas");
                    list.show(getFragmentManager(),"");
            }
        });
        editTR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchableListDialog list = SearchableListDialog.newInstance(tr);
                list.setOnSearchableItemClickListener(new SearchableListDialog.SearchableItem() {
                    @Override
                    public void onSearchableItemClicked(Object item, int position) {
                        editTR.setText(item.toString());
                    }
                });
                list.setTitle("Pilih TREG");
                list.show(getFragmentManager(),"");
            }
        });
    }

    void initClass() {
        cd = new CustomDialog(this);
        pref = new PrefManager(this);
        db = new DatabaseHelper(this);
        userData = db.getUser();
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
    }

    void initToolbar() {
        this.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(src);
        if (getSupportActionBar() != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(src);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.form_submit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSubmit:
                if(submitButton=true) {
                    submitButton = false;
                    createParam();
                }
                break;
            default:
                break;
        }
        return true;
    }

    private void createParam(){
        AddRkb param = new AddRkb();
        param.setRkb_name(editNama.getText().toString());
        param.setRkb_kelas(sKelas);
        param.setRkb_treg(sTr);
        param.setRkb_gmv(editGmv.getText().toString());
        if(src.contains("Edit")){
            param.setRkb_id(rkb.getRkb_id());
            editRkb(param);
        } else {
            addRkb(param);
        }

    }

    private void addRkb(AddRkb param){
        cd.show_p_Dialog();
        Call<GlobalResponse> addRkb = mNetworkService.addRkb(db.getUser().getUser_token(), param);
        addRkb.enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                cd.hide_p_Dialog();
                submitButton=true;
                int code = response.code();
                if (code == 200) {
                    if (response.body().getSuccess()==1) {
                        cd.oneButtonDialog_finish(response.body().getMessage(), R.drawable.ic_user, R.color.green_700);

                    } else {
                        cd.oneButtonDialog(response.body().getMessage(), R.drawable.ic_user, R.color.red_700);
                    }
                } else {
                    cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

                }
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                cd.hide_p_Dialog();
                submitButton=true;
                cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }
    private void editRkb(AddRkb param){
        cd.show_p_Dialog();
        Call<GlobalResponse> addRkb = mNetworkService.editRkb(db.getUser().getUser_token(), param);
        addRkb.enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                cd.hide_p_Dialog();
                int code = response.code();
                if (code == 200) {
                    if (response.body().getSuccess()==1) {
                        cd.oneButtonDialog_finish(response.body().getMessage(), R.drawable.ic_user, R.color.green_700);

                    } else {
                        cd.oneButtonDialog(response.body().getMessage(), R.drawable.ic_user, R.color.red_700);
                    }
                } else {
                    cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

                }
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                cd.hide_p_Dialog();
                cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }
}
