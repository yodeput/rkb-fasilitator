package id.triwikrama.rkbfasilitator.activity.admin;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.JenisUser;
import id.triwikrama.rkbfasilitator.model.RkbList;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.UserList;
import id.triwikrama.rkbfasilitator.model.param.AddUser;
import id.triwikrama.rkbfasilitator.model.response.GlobalResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.Apps;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import id.triwikrama.rkbfasilitator.utils.PrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddUserActivity extends AppCompatActivity {

    private User userData;
    private CustomDialog cd;
    private DatabaseHelper db;
    private PrefManager pref;
    private NetworkService mNetworkService;
    private String src;
    private int spinnerPos = -1;
    private List<JenisUser> jenisUsers = new ArrayList<>();
    private AddUser userParam = new AddUser();
    private UserList userEdit = new UserList();
    private String username, email, tempatLahir, tglLahir, alamat, telepon, tingkat, pendidikan, userType;
    private Calendar calendar;
    private int year, month, day;
    private RkbList rkb;
    private boolean submitButton=true;

    @BindView(R.id.spinnerJenis)
    MaterialSpinner spinnerJenis;
    @BindView(R.id.editUsername)
    AppCompatEditText editUsername;
    @BindView(R.id.editEmail)
    AppCompatEditText editEmail;
    @BindView(R.id.editTempatLahir)
    AppCompatEditText editTempatLahir;
    @BindView(R.id.editTanggalLahir)
    AppCompatEditText editTanggalLahir;
    @BindView(R.id.editAlamat)
    AppCompatEditText editAlamat;
    @BindView(R.id.editTelepon)
    AppCompatEditText editTelepon;
    @BindView(R.id.editTingkat)
    AppCompatEditText editTingkat;
    @BindView(R.id.editPendidikan)
    AppCompatEditText editPendidikan;

    @BindView(R.id.expandableLayout)
    ExpandableRelativeLayout expandableLayout;

    @BindView(R.id.btPilihRkb)
    RelativeLayout btPilihRkb;
    @BindView(R.id.lytRkb)
    LinearLayout lytRkb;
    @BindView(R.id.editRkb)
    AppCompatEditText editRkb;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @OnClick(R.id.editTanggalLahir) void pilihTanggal(){
        DatePickerDialog d = new DatePickerDialog(AddUserActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int arg1, int arg2, int arg3) {
                        String date = Integer.toString(arg1) + "-" + Integer.toString(arg2+1) + "-" + Integer.toString(arg3);
                        editTanggalLahir.setText(date);
                    }
                }, year, month, day);
        d.show();
    }
    @OnClick(R.id.btPilihRkb) void pilih(){
        Intent i = new Intent(AddUserActivity.this, ListRkbActivityAdmin.class);
        i.putExtra("src","addUser");
        startActivityForResult(i,300);
    }

    @OnClick(R.id.editRkb) void editRkb(){
        Intent i = new Intent(AddUserActivity.this, ListRkbActivityAdmin.class);
            i.putExtra("src", "user");
        startActivityForResult(i,300);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_user);
        ButterKnife.bind(this);
        initClass();
        initToolbar();
        initSpinner();
        initDatePicker();
        Intent i = getIntent();
        if(src.equals("Edit User")){
            userEdit = (UserList) i.getSerializableExtra("data");
            initData();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==300){
            if(resultCode==RESULT_OK){
                rkb = (RkbList) data.getSerializableExtra("data");
                lytRkb.setVisibility(View.VISIBLE);
                btPilihRkb.setVisibility(View.GONE);
                editRkb.setText(rkb.getRkb_name());
            }
        }
    }

    void initData(){
        spinnerJenis.setSelection(Apps.getInstance().getUserType(userEdit.getUser_type()));
        editUsername.setText(userEdit.getUser_name());
        editTelepon.setText(userEdit.getUser_notelp());
        editAlamat.setText(userEdit.getUser_alamat());
        editEmail.setText(userEdit.getUser_email());
        editPendidikan.setText(userEdit.getUser_pendidikan());
        editTanggalLahir.setText(userEdit.getUser_tanggallahir());
        editTempatLahir.setText(userEdit.getUser_tempatlahir());
        editTingkat.setText(userEdit.getUser_tingkat());

    }

    void initClass() {
        cd = new CustomDialog(this);
        pref = new PrefManager(this);
        db = new DatabaseHelper(this);
        userData = db.getUser();
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
    }

    void initToolbar() {
        Intent i = getIntent();
        src = i.getStringExtra("src");
        this.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(src);
        if (getSupportActionBar() != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(src);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }

    private void initSpinner(){
        Gson gson = new Gson();
        Type type = new TypeToken<List<JenisUser>>(){}.getType();
        jenisUsers =  gson.fromJson(Apps.getInstance().readJenisUser(), type);
        ArrayList<String> jenisArray = new ArrayList<>();
        for (int i = 0; i < jenisUsers.size(); i++) {
            jenisArray.add(jenisUsers.get(i).getNama());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, jenisArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerJenis.setAdapter(adapter);
        spinnerJenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (spinnerPos == position) {
                    return;
                }
                spinnerPos = position;

                if (spinnerPos == -1) {
                expandableLayout.collapse();
                } else  if (spinnerPos == -0) {
                    expandableLayout.collapse();
                } else{
                    userType=jenisUsers.get(position).getId();
                    expandableLayout.expand();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initDatePicker(){
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
    }

    private boolean validateEdittext(){
        boolean isValid=true;
        username = editUsername.getText().toString();
        email = editEmail.getText().toString();
        tempatLahir = editTempatLahir.getText().toString();
        tglLahir = editTanggalLahir.getText().toString();
        alamat = editAlamat.getText().toString();
        telepon = editTelepon.getText().toString();
        tingkat = editTingkat.getText().toString();
        pendidikan = editPendidikan.getText().toString();

        if ((username.isEmpty())||(email.isEmpty())||(tempatLahir.isEmpty())
                ||(tglLahir.isEmpty())||(alamat.isEmpty())||(telepon.isEmpty())
                ||(tingkat.isEmpty())||(pendidikan.isEmpty())) {
            cd.toastError("Mohon lengkapi data User");
            isValid = false;
        }

        if(!isValidEmail(email)){
            editEmail.setError("Format Email salah!");
            cd.toastError("Format Email salah!");
            isValid = false;
        }

        if(spinnerJenis.getSelectedItemPosition()==0){
            cd.toastError("Silahkan pilih Role User");
            isValid = false;
        }

        return isValid;
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private void setParam() {
        if(userType.equals(1)){
            userParam.setUser_name(username);
            userParam.setUser_email(email);
            userParam.setUser_notelp(telepon);
            userParam.setUser_alamat(alamat);
            userParam.setUser_tempatlahir(tempatLahir);
            userParam.setUser_tanggallahir(tglLahir);
            userParam.setUser_pendidikan(pendidikan);
            userParam.setUser_tingkat(tingkat);
            userParam.setUser_type(userType);
            userParam.setUser_status("1");
            if(src.equals("Edit User")){
                userParam.setUser_id(userEdit.getUser_id());
                postEditUser();
            } else{
                postUser();
            }

        } else {
            userParam.setRkb_id(rkb.getRkb_id());
            userParam.setUser_name(username);
            userParam.setUser_email(email);
            userParam.setUser_notelp(telepon);
            userParam.setUser_alamat(alamat);
            userParam.setUser_tempatlahir(tempatLahir);
            userParam.setUser_tanggallahir(tglLahir);
            userParam.setUser_pendidikan(pendidikan);
            userParam.setUser_tingkat(tingkat);
            userParam.setUser_type(userType);
            userParam.setUser_status("1");
            if(src.equals("Edit User")){
                userParam.setUser_id(userEdit.getUser_id());
                postEditUser();
            } else {
                postUser();
            }

        }
    }

    private void postUser(){
        cd.show_p_Dialog();
        Call<GlobalResponse> req = mNetworkService.addUser(db.getUser().getUser_token(),userParam);
        req.enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                cd.hide_p_Dialog();
                submitButton=true;
                int code = response.code();
                if (code == 200) {
                    if (response.body().getSuccess()==1) {
                        cd.oneButtonDialog_finish(response.body().getMessage(), R.drawable.ic_user, R.color.green_700);

                    } else {
                        cd.oneButtonDialog(response.body().getMessage(), R.drawable.ic_user, R.color.red_700);
                    }
                } else {
                    cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

                }
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                cd.hide_p_Dialog();
                submitButton=true;
                cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }

    private void postEditUser(){
        cd.show_p_Dialog();
        Call<GlobalResponse> req = mNetworkService.editUser(db.getUser().getUser_token(),userParam);
        req.enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                cd.hide_p_Dialog();
                submitButton=true;
                int code = response.code();
                if (code == 200) {
                    if (response.body().getSuccess()==1) {
                        cd.oneButtonDialog_finish(response.body().getMessage(), R.drawable.ic_user, R.color.green_700);

                    } else {
                        cd.oneButtonDialog(response.body().getMessage(), R.drawable.ic_user, R.color.red_700);
                    }
                } else {
                    cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

                }
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                cd.hide_p_Dialog();
                submitButton=true;
                cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.form_submit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSubmit:
                if (validateEdittext()){
                    if(submitButton=true) {
                        submitButton = false;
                        setParam();
                    }
                }
                break;
            default:
                break;
        }
        return true;
    }

}
