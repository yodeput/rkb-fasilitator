package id.triwikrama.rkbfasilitator.activity.admin;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.triwikrama.rkbfasilitator.MainActivity;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.adapter.RekapListAdapter;
import id.triwikrama.rkbfasilitator.model.Rekap;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.param.RekapParam;
import id.triwikrama.rkbfasilitator.model.response.RekapResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.Apps;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import id.triwikrama.rkbfasilitator.utils.PrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RekapRkbActivity extends AppCompatActivity {

    private User userData;
    private CustomDialog cd;
    private DatabaseHelper db;
    private PrefManager pref;
    private NetworkService mNetworkService;
    private String src;
    private RekapListAdapter mAdapter;
    private int spinnerPos = -1;
    private String bulan, tahun;
    private int logPage=1;
    private RekapParam param = new RekapParam();

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txtTotal)
    TextView txtTotal;
    @BindView(R.id.txtBulan)
    TextView txtBulan;
    @BindView(R.id.txtTahun)
    TextView txtTahun;
    @BindView(R.id.txtNamaBulan)
    TextView txtNamaBulan;

    @OnClick(R.id.lytBulan)
    void filterbulan() {
        showPicker();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_rekap);
        ButterKnife.bind(this);
        initClass();
        initToolbar();
        Intent i = getIntent();


        Calendar today = Calendar.getInstance();
        DateFormat yearFormat = new SimpleDateFormat("yyyy");
        DateFormat monthFormat = new SimpleDateFormat("MM");
        Date date = new Date();

        tahun = yearFormat.format(date);
        bulan = monthFormat.format(date);
        createParam(tahun, bulan);

    }

    void initClass() {
        cd = new CustomDialog(this);
        pref = new PrefManager(this);
        db = new DatabaseHelper(this);
        userData = db.getUser();
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
    }

    void initToolbar() {
        Intent i = getIntent();
        src = "Rekap RKB";
        this.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(src);
        if (getSupportActionBar() != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(src);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.form_submit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSubmit:

                break;
            default:
                break;
        }
        return true;
    }*/

    private void showPicker() {
        Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(RekapRkbActivity.this,
                new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) {
                    bulan = Integer.toString(selectedMonth+1);

                    if(bulan.length()<2){
                        bulan = "0"+bulan;
                    }

                    tahun = Integer.toString(selectedYear);
                    createParam(tahun, bulan);
                    }
                    },today.get(Calendar.YEAR),Integer.parseInt(bulan)-1);
        builder.setMinYear(2019)
                .setActivatedYear(Integer.parseInt(tahun))
                .setMaxYear(2030)
                .build()
                .show();
    }

    private void createParam(String tahun, String bulan) {
        Log.e("bulan tahun", bulan +" / "+tahun);
        txtTahun.setText(tahun);
        txtBulan.setText(Apps.getMonthNameShort(Integer.parseInt(bulan)));
        txtNamaBulan.setText(Apps.getMonthNameByInt(Integer.parseInt(bulan)));

        param.setTahun(tahun);
        param.setBulan(bulan);
        param.setPage("1");
        reqData(param);
    }

    private void reqData(RekapParam param) {
        cd.show_p_Dialog();
        logPage=1;
        Call<RekapResponse> reqRekap = mNetworkService.rekapList(db.getUser().getUser_token(), param);
        reqRekap.enqueue(new Callback<RekapResponse>() {
            @Override
            public void onResponse(Call<RekapResponse> call, Response<RekapResponse> response) {
                cd.hide_p_Dialog();
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        txtTotal.setText(response.body().getJumlahKegiatan());
                        generateList(response.body().getRekap());
                    } else {
                        cd.toastError("Rekap tidak ditemukan");
                        txtTotal.setText("-");
                        List<Rekap> rekap = new ArrayList<>();
                        generateList(rekap);
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }
            }

            @Override
            public void onFailure(Call<RekapResponse> call, Throwable t) {
                cd.hide_p_Dialog();
            }
        });
    }

    private void generateList(List<Rekap> rekapList) {
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setHasFixedSize(true);
        mAdapter = new RekapListAdapter(this, rekapList.size(), rekapList);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new RekapListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Rekap rekap, int position) {
            Intent i = new Intent(RekapRkbActivity.this,RekapDetailActivity.class);
            i.putExtra("data",rekap);
            i.putExtra("bulan", bulan);
            i.putExtra("tahun", tahun);
            startActivity(i);
            overridePendingTransition(R.anim.anim_slide_in_right,R.anim.anim_slide_out_left);
            }
        });

            mAdapter.setOnLoadMoreListener(new RekapListAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore(int current_page) {
                    logPage++;
                    nextPage(logPage);
                }
            });

    }

    void nextPage(int current_page){
        if(current_page>1) {
            mAdapter.setLoading();
            param.setPage(Integer.toString(current_page));
            reqNextData(param);
        }
    }

    private void reqNextData(RekapParam param) {
        Call<RekapResponse> reqRekap = mNetworkService.rekapList(db.getUser().getUser_token(), param);
        reqRekap.enqueue(new Callback<RekapResponse>() {
            @Override
            public void onResponse(Call<RekapResponse> call, Response<RekapResponse> response) {
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        mAdapter.insertData(response.body().getRekap());
                    } else {
                        mAdapter.setLoaded();
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }
            }

            @Override
            public void onFailure(Call<RekapResponse> call, Throwable t) {
                cd.hide_p_Dialog();
            }
        });
    }
}
