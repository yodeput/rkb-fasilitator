package id.triwikrama.rkbfasilitator.activity;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.github.aakira.expandablelayout.Utils;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.Profile;
import id.triwikrama.rkbfasilitator.model.param.EditProfileParam;
import id.triwikrama.rkbfasilitator.model.param.EditPwdParam;
import id.triwikrama.rkbfasilitator.model.response.GlobalResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class ProfileEditActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.editName)
    AppCompatEditText editName;
    @BindView(R.id.editTempatLahir)
    AppCompatEditText editTempatLahir;
    @BindView(R.id.editTanggalLahir)
    AppCompatEditText editTanggalLahir;
    @BindView(R.id.editEmail)
    AppCompatEditText editEmail;
    @BindView(R.id.editTelepon)
    AppCompatEditText editTelepon;
    @BindView(R.id.editAlamat)
    AppCompatEditText editAlamat;
    @BindView(R.id.editTingkat)
    AppCompatEditText editTingkat;
    @BindView(R.id.editPendidikan)
    AppCompatEditText editPendidikan;
    @BindView(R.id.editLokasi)
    AppCompatEditText editLokasi;
    @BindView(R.id.expandableLayout)
    ExpandableRelativeLayout expandableLayout;
    @BindView(R.id.btCancel)
    Button btCancel;
    @BindView(R.id.btSave)
    Button btSave;
    @BindView(R.id.imgArrow)
    ImageView imgArrow;
    @BindView(R.id.txtLytPwdLama)
    TextInputLayout txtLytPwdLama;
    @BindView(R.id.txtLytPwdBaru)
    TextInputLayout txtLytPwdBaru;
    @BindView(R.id.txtLytPwdBaru2)
    TextInputLayout txtLytPwdBaru2;
    @BindView(R.id.editPwdLama)
    AppCompatEditText editPwdLama;
    @BindView(R.id.editPwdBaru)
    AppCompatEditText editPwdBaru;
    @BindView(R.id.editPwdBaru2)
    AppCompatEditText editPwdBaru2;

    private Profile profile;
    private CustomDialog cd;
    private DatabaseHelper db;
    private EditPwdParam pwdParam = new EditPwdParam();
    private Dialog dialog = null;
    private int IMAGE_PICKER_REQUEST = 553;
    private Calendar calendar;
    private int year, month, day;
    private NetworkService mNetworkService;
    private String tgllahir;

    @OnClick(R.id.butEditPwd)
    void gantiPwd() {
        if (expandableLayout.isExpanded()) {
            createRotateAnimator(imgArrow, 180f, 0f).start();
            expandableLayout.collapse();
        } else {
            createRotateAnimator(imgArrow, 0f, 180f).start();
            expandableLayout.expand();
        }
    }

    @OnClick(R.id.btCancel)
    void cancelChangePwd() {
        createRotateAnimator(imgArrow, 180f, 0f).start();
        expandableLayout.collapse();
    }

    @OnClick(R.id.btSave)
    void ChangePwd() {
        String oldPwd = editPwdLama.getText().toString();
        String newPwd = editPwdBaru.getText().toString();
        String newPwd2 = editPwdBaru2.getText().toString();
        if (!newPwd.equals(newPwd2)) {
            txtLytPwdBaru2.setError("Password Baru tidak sesuai");
            clearEditPwd();
        } else {
            reqChangePwd(oldPwd, newPwd, newPwd2);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_profile_edit);
        ButterKnife.bind(this);
        initClass();
        readData();
        initToolbar();
    }

    void initClass() {
        cd = new CustomDialog(this);
        db = new DatabaseHelper(this);

        mNetworkService = NetworkModule.getClient().create(NetworkService.class);

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
    }

    void readData() {
        Intent i = getIntent();
        profile = (Profile) i.getSerializableExtra("data");
        editName.setText(profile.getUser_name());
        editTempatLahir.setText(profile.getUser_tempatlahir());
        editTanggalLahir.setText(profile.getUser_tanggallahir());
        editEmail.setText(profile.getUser_email());
        editTelepon.setText(profile.getUser_notelp());
        editAlamat.setText(profile.getUser_alamat());
        editTingkat.setText(profile.getUser_tingkat());
        editPendidikan.setText(profile.getUser_pendidikan());
        editLokasi.setText(profile.getRkb_name());

        editTanggalLahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(999);
            }
        });
    }

    void setParam() {
        cd.show_p_Dialog();
        EditProfileParam param = new EditProfileParam();
        param.setUser_id(db.getUser().getUser_id());
        param.setName(editName.getText().toString());
        param.setAlamat(editAlamat.getText().toString());
        param.setNotelp(editTelepon.getText().toString());
        param.setTanggallahir(tgllahir);
        param.setTempatlahir(editTempatLahir.getText().toString());
        param.setTingkat(editTingkat.getText().toString());
        param.setPendidikan(editPendidikan.getText().toString());
        Log.e("wwkwkwkwkwk", param.toString());
        saveEditProfile(param);
    }

    void saveEditProfile(EditProfileParam ediProfileParam) {
        Call<GlobalResponse> saveProfile = mNetworkService.editProfile(db.getUser().getUser_token(), ediProfileParam);
        saveProfile.enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                cd.hide_p_Dialog();
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        ProfileEditActivity.this.setResult(RESULT_OK);
                        finish();
                    } else {
                        ProfileEditActivity.this.setResult(Activity.CONTEXT_RESTRICTED);
                        finish();
                    }
                } else {
                    cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                cd.hide_p_Dialog();
                ProfileEditActivity.this.setResult(Activity.CONTEXT_RESTRICTED);
                finish();
            }
        });
    }

    void reqChangePwd(String oldPwd, String newPwd, String newPwd2) {
        EditPwdParam pwdParam = new EditPwdParam(db.getUser().getUser_id(), oldPwd, newPwd, newPwd2);
        Call<GlobalResponse> changePwd = mNetworkService.editPassword(db.getUser().getUser_token(), pwdParam);
        changePwd.enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    String messsage = response.body().getMessage();
                    if (success == 1) {

                    } else {
                        if (messsage.equals("Old Password Not Match")) {
                            txtLytPwdLama.setError("Password lama tidak sesuai");
                            clearEditPwd();
                        }
                    }
                } else {
                    cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }

    void clearEditPwd() {
        editPwdLama.setText("");
        editPwdBaru.setText("");
        editPwdBaru2.setText("");
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    String date = Integer.toString(arg3);
                    String month = getInstance().getMonthNameByInt(arg2);
                    String year = Integer.toString(arg1);
                    String tgl = date + " " + month + " " + year;
                    tgllahir = year + "-" + Integer.toString(arg2) + "-" + date;
                    Log.e("wkwkwwkwk", "" + arg3 + arg2 + arg1);

                    editTanggalLahir.setText(tgl);
                }
            };


    void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Edit Profile");
        if (getSupportActionBar() != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Edit Profile");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileEditActivity.this.setResult(Activity.RESULT_CANCELED);
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.form_submit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSubmit:
                setParam();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ProfileEditActivity.this.setResult(Activity.RESULT_CANCELED);
        finish();
        overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
    }

    public ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
        return animator;
    }

}
