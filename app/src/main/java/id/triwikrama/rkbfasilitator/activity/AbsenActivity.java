package id.triwikrama.rkbfasilitator.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.AddImage;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.param.AbsenParam;
import id.triwikrama.rkbfasilitator.model.response.GlobalResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.Apps;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import id.triwikrama.rkbfasilitator.utils.PrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class AbsenActivity extends AppCompatActivity {

    private User userData;
    private CustomDialog cd;
    private DatabaseHelper db;
    private PrefManager pref;
    private NetworkService mNetworkService;

    private static final int RC_CAMERA = 3000;
    private static RecyclerView.Adapter adapter;
    private static ArrayList<AddImage> data;


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txtTanggal)
    TextView txtTanggal;
    @BindView(R.id.txtNoImage)
    TextView txtNoImage;
    @BindView(R.id.imgFoto)
    ImageView imgFoto;

    private GoogleMap googleMap;
    private FusedLocationProviderClient fusedLocationClient;
    private SupportMapFragment mapFragment;
    private int PLACE_PICKER_REQUEST = 0;
    private LatLng latlong = new LatLng(0, 0);
    //private LatLng latlong = new LatLng(-6.9276784, 107.5950101);//Bandung
    private String src;

    private ArrayList<Image> images = new ArrayList<>();
    private int IMAGE_PICKER_REQUEST = 553;
    private RecyclerView.LayoutManager layoutManager;
    private Dialog dialog;
    private boolean submitButton = true;

    @OnClick(R.id.fabAddImage)
    void addImage() {
        captureImage();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_absen);
        ButterKnife.bind(this);
        initClass();
        initToolbar();
        initMap();
    }

    void initClass() {
        cd = new CustomDialog(this);
        pref = new PrefManager(this);
        db = new DatabaseHelper(this);
        userData = db.getUser();
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
    }

    void initToolbar() {
        txtTanggal.setText(Html.fromHtml(getInstance().getDateTime()));
        Intent i = getIntent();
        src = i.getStringExtra("src");
        this.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("" + src);
        if (getSupportActionBar() != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("" + src);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }

    private void initMap() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                AbsenActivity.this.googleMap = googleMap;
                fusedLocationClient = LocationServices.getFusedLocationProviderClient(AbsenActivity.this);
                fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            showMarker(location);
                        }
                    }
                });
                fusedLocationClient.getLocationAvailability().addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("getLoc Failed", e.getMessage());
                    }
                });
            }
        });
    }

    private void showMarker(@NonNull Location currentLocation) {
        latlong = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        Marker pinnedMarker = googleMap.addMarker(new MarkerOptions().position(latlong).title("Lokasi Anda").draggable(true));
        googleMap.getUiSettings().setScrollGesturesEnabled(false);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latlong));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlong, 15));

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.form_submit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSubmit:
                if (submitButton) {
                    submitButton = false;
                    createParam();
                }
                break;
            default:
                break;
        }
        return true;
    }

    void openImageSelect() {
        ImagePicker.create(this)
                //.returnMode(ReturnMode.ALL)
                .folderMode(true)
                .toolbarFolderTitle("Folder")
                .toolbarImageTitle("Tap to select")
                .toolbarArrowColor(Color.BLACK)
                .includeVideo(false)
                .limit(10)
                .multi()
                .imageDirectory("Camera")
                .origin(images)
                .exclude(images)
                .theme(R.style.ImagePickerTheme)
                .enableLog(false)
                .start();
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        Log.e("onActivityResult", "" + requestCode);
        images.clear();
        if (requestCode == IMAGE_PICKER_REQUEST) {
            if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
                images = (ArrayList<Image>) ImagePicker.getImages(data);
                printImages(images.get(0));
                return;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void printImages(Image images) {
        if (images == null) return;
        txtNoImage.setVisibility(View.GONE);
        Glide.with(imgFoto)
                .load(images.getPath())
                .fitCenter()
                .into(imgFoto);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RC_CAMERA) {
            if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                captureImage();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void captureImage() {
        ImagePicker.cameraOnly().start(this);
    }

    private void createParam() {
        if((latlong.latitude==0)||(latlong.longitude==0)) {
            cd.oneButtonDialog("Sepertinya Anda mematikan fitur GPS\nSilahkan hidupkan untuk melanjutkan Absen",R.drawable.ic_location_on_black_24dp, R.color.red);
        } else {
            AbsenParam param = new AbsenParam();
            if (images.size() > 0) {
                param.setLat(Double.toString(latlong.latitude));
                param.setLng(Double.toString(latlong.longitude));
                param.setUser_id(db.getUser().getUser_id());
                param.setPhoto(Apps.getInstance().JpgToString(images.get(0).getPath()));
                absen(param);
            } else {
                cd.toastWarning("Anda belum menambahkan Foto Absen  ");
            }
        }
    }

    private void absen(AbsenParam param) {
        cd.show_p_Dialog();
        Call<GlobalResponse> sendAbsen = mNetworkService.absenFasilitator(db.getUser().getUser_token(), param);
        sendAbsen.enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                cd.hide_p_Dialog();
                submitButton = true;
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        oneButtonDialog(response.body().getMessage(), R.drawable.ic_check_black_24dp, R.color.green_700, RESULT_OK);
                    } else {
                        oneButtonDialog(response.body().getMessage(), R.drawable.ic_user, R.color.merah, RESULT_CANCELED);
                    }
                } else {
                    cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                cd.hide_p_Dialog();
                cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                submitButton = true;

            }
        });
    }

    public void oneButtonDialog(String message, int img_header, int color_header, int resultCode) {
        if (dialog == null || !dialog.isShowing()) {
            dialog = new Dialog(AbsenActivity.this, R.style.DialogSlideAnim);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // b// efore
            dialog.setContentView(R.layout.dialog_one_button);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER_HORIZONTAL;
            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setAttributes(lp);


            dialog.setCanceledOnTouchOutside(false);
            dialog.setOnKeyListener(new Dialog.OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface arg0, int keyCode,
                                     KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                    }
                    return true;
                }
            });
            Drawable img = AbsenActivity.this.getResources().getDrawable(img_header);
            int color = AbsenActivity.this.getResources().getColor(color_header);

            ((LinearLayout) dialog.findViewById(R.id.header_dialog)).setBackgroundColor(color);
            ((ImageView) dialog.findViewById(R.id.img_dialog)).setImageDrawable(img);
            ((TextView) dialog.findViewById(R.id.txt_message)).setText(message);
            ((Button) dialog.findViewById(R.id.bt_positive)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (resultCode == RESULT_OK) {
                        Intent i = new Intent();
                        AbsenActivity.this.setResult(resultCode, i);
                        AbsenActivity.this.finish();
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                    }
                }
            });
        }
    }


}
