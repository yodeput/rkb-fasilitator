package id.triwikrama.rkbfasilitator.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.Profile;
import id.triwikrama.rkbfasilitator.model.param.EditPhotoParam;
import id.triwikrama.rkbfasilitator.model.param.ProfileParam;
import id.triwikrama.rkbfasilitator.model.response.GlobalResponse;
import id.triwikrama.rkbfasilitator.model.response.ProfileResponse;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class ProfileActivity extends AppCompatActivity {

    private DatabaseHelper db;
    private CustomDialog cd;
    private Profile profileData;
    private NetworkService mNetworkService;
    private static final int IMAGE_PICKER_REQUEST = 553;
    private boolean lockAspectRatio = false, setBitmapMaxWidthHeight = false;
    private int ASPECT_RATIO_X = 1, ASPECT_RATIO_Y = 1, bitmapMaxWidth = 1000, bitmapMaxHeight = 1000;
    private int IMAGE_COMPRESSION = 70;
    public static String fileName;
    public static final int REQUEST_IMAGE = 100;
    public static final int EDIT_PROFILE= 123;
    private ArrayList<Image> images = new ArrayList<>();
    private String extension;

    @BindView(R.id.imgAvatar2)
    CircularImageView imgAvatar2;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindViews({R.id.txtNama,R.id.txtEmail, R.id.txtProfile1, R.id.txtProfile2
            , R.id.txtProfile3, R.id.txtProfile4, R.id.txtProfile5, R.id.txtProfile6
            , R.id.txtProfile7})
    List<TextView> textViews;

    @OnClick(R.id.fab_edit) void edit(){
        Intent i = new Intent(this, ProfileEditActivity.class);
        i.putExtra("data", profileData);
        startActivityForResult(i,EDIT_PROFILE);
        overridePendingTransition(R.anim.anim_slide_in_right,R.anim.anim_slide_out_left);
    }

    @OnClick(R.id.fabEditPhoto) void rubahProfileImage(){
        dialog_pilih_sumber();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        db = new DatabaseHelper(this);
        cd = new CustomDialog(this);
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
        initToolbar();
        reqProfileData();

    }

    void reqProfileData(){
        Call<ProfileResponse> profileReq = mNetworkService.userProfile(db.getUser().getUser_token(),new ProfileParam(db.getUser().getUser_id()));
        profileReq.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                int code = response.code();
                if(code==200){
                    int success = response.body().getSuccess();
                    if(success==1){
                        profileData = response.body().getProfile().get(0);
                        readData(profileData);
                    }else {
                        cd.oneButtonDialog_finish(getString(R.string.error_koneksi),R.drawable.ic_cloud_off,R.color.merah);

                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi),R.drawable.ic_cloud_off,R.color.merah);
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi),R.drawable.ic_cloud_off,R.color.merah);

            }
        });
    }

    void reqEditPP(String path){
        cd.show_p_Dialog();
        Call<GlobalResponse> editPhoto = mNetworkService.editPhoto(db.getUser().getUser_token(), new EditPhotoParam(db.getUser().getUser_id(),getInstance().JpgToString(path)));
        editPhoto.enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                cd.hide_p_Dialog();
                if(response.body().getSuccess()==1){
                    Glide.with(imgAvatar2)
                            .load(path)
                            .centerCrop()
                            .into(imgAvatar2);
                } else {
                    cd.hide_p_Dialog();
                }
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                cd.hide_p_Dialog();
            }
        });


    }

    void readData(Profile profile){
        textViews.get(0).setText(profile.getUser_name());
        textViews.get(1).setText(profile.getUser_email());
        textViews.get(2).setText(profile.getUser_name());
        textViews.get(3).setText(profile.getRkb_name());
        textViews.get(4).setText( profile.getUser_tingkat()+" - "+ profile.getUser_pendidikan());
        textViews.get(5).setText(profile.getUser_usia());
        textViews.get(6).setText(profile.getUser_tempatlahir()+", "+profile.getUser_tanggallahir());
        textViews.get(7).setText(profile.getUser_notelp());
        textViews.get(8).setText(profile.getUser_alamat());
        String urlFoto = getInstance().urlFixer(profile.getUser_photo());
        RequestOptions options;
            options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.avatar_man)
                    .error(R.drawable.avatar_man);
        Glide.with(imgAvatar2)
                .load(urlFoto)
                .centerCrop()
                .apply(options)
                .into(imgAvatar2);
    }

    void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }



    public void dialog_pilih_sumber() {
        final Dialog dialog = new Dialog(this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_pick_image_option);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER_HORIZONTAL;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);


        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss();
                }
                return true;
            }
        });

        ((CardView) dialog.findViewById(R.id.butCamera)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                captureImage();
            }
        });
        ((CardView) dialog.findViewById(R.id.butGallery)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openImageSelect();
            }
        });
    }

    private void captureImage() {
        ImagePicker.cameraOnly().start(this);
    }

    void openImageSelect(){
        ImagePicker.create(this)
                //.returnMode(ReturnMode.ALL)
                .folderMode(true)
                .toolbarFolderTitle("Folder")
                .toolbarImageTitle("Tap to select")
                .toolbarArrowColor(Color.BLACK)
                .includeVideo(false)
                .single()
                .imageDirectory("Camera")
                .origin(images)
                .exclude(images)
                .theme(R.style.ImagePickerTheme)
                .enableLog(false)
                .start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case IMAGE_PICKER_REQUEST:
                if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
                    images = (ArrayList<Image>) ImagePicker.getImages(data);
                    cropImage(Uri.fromFile(new File(images.get(0).getPath())));
                     extension= images.get(0).getPath().substring(images.get(0).getPath().lastIndexOf("."));
                    return;
                }
                break;
            case EDIT_PROFILE:
                if (resultCode == Activity.RESULT_OK) {
                    reqProfileData();
                    cd.toastSuccess("Perubahan data berhasil");
                } else if (resultCode == Activity.RESULT_CANCELED) {

                } else {
                    cd.oneButtonDialog("Terjadi kesalahan", R.drawable.ic_user, R.color.merah);
                }
                break;
            case UCrop.REQUEST_CROP:
                if (resultCode == RESULT_OK) {
                    handleUCropResult(data);

                } else {
                    cd.oneButtonDialog("Terjadi kesalahan", R.drawable.ic_user, R.color.merah);
                }
                break;
            case UCrop.RESULT_ERROR:
                final Throwable cropError = UCrop.getError(data);
                cd.oneButtonDialog("Terjadi kesalahan", R.drawable.ic_user, R.color.merah);
                break;
        }
    }

    private void handleUCropResult(Intent data) {
        if (data == null) {
            return;
        }
        final Uri resultUri = UCrop.getOutput(data);
        Log.e("wkwkwkwkwk",resultUri.toString());
        reqEditPP(resultUri.getPath().toString());
    }

    private void cropImage(Uri sourceUri) {
        Uri destinationUri = sourceUri;
        UCrop.Options options = new UCrop.Options();
        options.setCompressionQuality(IMAGE_COMPRESSION);

        // applying UI theme
        options.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        options.setActiveWidgetColor(ContextCompat.getColor(this, R.color.colorPrimary));

        if (lockAspectRatio)
            options.withAspectRatio(ASPECT_RATIO_X, ASPECT_RATIO_Y);

        if (setBitmapMaxWidthHeight)
            options.withMaxResultSize(bitmapMaxWidth, bitmapMaxHeight);

        UCrop.of(sourceUri, destinationUri)
                .withOptions(options)
                .start(this);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
    }
}
