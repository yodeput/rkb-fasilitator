package id.triwikrama.rkbfasilitator.activity.admin;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.activity.EntryActivity;
import id.triwikrama.rkbfasilitator.adapter.ListKegiatanAdminAdapter;
import id.triwikrama.rkbfasilitator.fragment.DetailFragment;
import id.triwikrama.rkbfasilitator.fragment.MyDialogCloseListener;
import id.triwikrama.rkbfasilitator.model.JenisKeg;
import id.triwikrama.rkbfasilitator.model.Kegiatan;
import id.triwikrama.rkbfasilitator.model.RkbList;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.param.ListSearchParam;
import id.triwikrama.rkbfasilitator.model.param.ListFilterParam;
import id.triwikrama.rkbfasilitator.model.response.ListResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.Apps;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class ListKegiatanActivityAdmin extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.shimmer)
    ShimmerFrameLayout shimmer;
    @BindView(R.id.recylerview)
    RecyclerView recyclerView;
    @BindView(R.id.search_view)
    FloatingSearchView searchView;
    @BindView(R.id.txtDataKosong)
    TextView txtDataKosong;
    @BindView(R.id.fab_add)
    FloatingActionButton fab_add;

    private String src;
    private User userData;
    private NetworkService mNetworkService;
    private CustomDialog cd;
    private DatabaseHelper db;
    private DetailFragment detailFragment = null;
    private final AlertDialog.Builder builder = null;

    private ListKegiatanAdminAdapter mAdapter;
    private List<Kegiatan> kegiatans = new ArrayList<>();
    private ListSearchParam param = new ListSearchParam();
    private RkbList rkb = new RkbList();
    private String jenisFilter="";

    private int logPage=1;
    private Calendar calendar;
    private int year, month, day;
    private int mLastSpinnerPosition = 0;
    private String typeFilter="", dateFrom="",dateTo="", rkbname="";

    @OnClick(R.id.fab_add)
    void addData() {
        Intent i = new Intent(this, EntryActivity.class);
        i.putExtra("src", src);
        startActivity(i);
        overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_list);
        ButterKnife.bind(this);
        initClass();
        initToolbar();
        param.setPage("1");
        param.setType("0");
        reqData(param);

        searchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {

            }

            @Override
            public void onSearchAction(String currentQuery) {
                param.setPage("1");
                param.setSearch(currentQuery);
                reqData(param);
            }
        });
        searchView.setOnClearSearchActionListener(new FloatingSearchView.OnClearSearchActionListener() {
            @Override
            public void onClearSearchClicked() {
                logPage = 1;
                param.setPage("1");
                param.setSearch("");
                reqData(param);
            }
        });

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

    }

    void initClass() {
        cd = new CustomDialog(this);
        db = new DatabaseHelper(this);
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
        userData = db.getUser();
    }

    private void reqData(ListSearchParam param) {
        logPage = 1;
        shimmer.setVisibility(View.VISIBLE);
        shimmer.startShimmer();
        Call<ListResponse> reqData = mNetworkService.listKegiatan(userData.getUser_token(), param);;

        reqData.enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                kegiatans.clear();
                shimmer.stopShimmer();
                shimmer.setVisibility(View.GONE);

                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        recyclerView.setVisibility(View.VISIBLE);
                        kegiatans.addAll(response.body().getKegiatans());
                        generateList(kegiatans);
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        txtDataKosong.setVisibility(View.VISIBLE);
                        txtDataKosong.setText("Data tidak ditemukan");
                        shimmer.setVisibility(View.GONE);
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }

            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                shimmer.stopShimmer();
                shimmer.setVisibility(View.GONE);
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }


    private void generateList(List<Kegiatan> kegiatanList) {
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setHasFixedSize(true);
        mAdapter = new ListKegiatanAdminAdapter(this, kegiatanList.size(), kegiatanList);
        recyclerView.setAdapter(mAdapter);
        if(kegiatanList.size()>7) {
            mAdapter.setOnLoadMoreListener(new ListKegiatanAdminAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore(int current_page) {
                    logPage++;
                    nextPage(logPage);
                }
            });
        }
        mAdapter.setOnItemClickListener(new ListKegiatanAdminAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Kegiatan obj, int position) {
                if (detailFragment == null || !detailFragment.isAdded()) {
                    detailFragment = new DetailFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("src", "admin");
                    bundle.putSerializable("data", obj);
                    detailFragment.setArguments(bundle);
                    detailFragment.show(getSupportFragmentManager(), detailFragment.getTag());
                    MyDialogCloseListener closeListener = new MyDialogCloseListener() {
                        @Override
                        public void handleDialogClose(DialogInterface dialog) {
                            reqData(param);
                        }
                    };
                    detailFragment.DismissListener(closeListener);
                }
            }
        });
        txtDataKosong.setVisibility(View.GONE);

    }

    void nextPage(int current_page){
        if(current_page>1) {
                shimmer.setVisibility(View.GONE);
                mAdapter.setLoading();
                param.setPage(Integer.toString(current_page));
                reqNextData(param);
        }
    }

    private void reqNextData(ListSearchParam param) {
        Call<ListResponse> reqData =mNetworkService.listKegiatan(userData.getUser_token(), param);;
        Log.e("onLoadmore", "reqNextData");
        reqData.enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                shimmer.stopShimmer();
                shimmer.setVisibility(View.GONE);
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        mAdapter.insertData(response.body().getKegiatans());
                    } else {
                        mAdapter.setLoaded();
                        txtDataKosong.setVisibility(View.GONE);
                        shimmer.setVisibility(View.GONE);
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }

            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                shimmer.stopShimmer();
                shimmer.setVisibility(View.GONE);
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }



    void initToolbar() {
        fab_add.hide();
        txtDataKosong.setVisibility(View.GONE);
        Intent i = getIntent();
        src = i.getStringExtra("src");
        this.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Daftar " + src);
        if (getSupportActionBar() != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Daftar " + src);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.data_filter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menuFilter:
                dialog_filter();
                break;
            default:
                break;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
    }


    public void dialog_filter() {
        if (builder == null) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.DialogSlideAnim);
            final View viewInflated = LayoutInflater.from(this).inflate(R.layout.dialog_filter_kegiatan,
                    (ViewGroup) this.findViewById(android.R.id.content), false);
            final MaterialSpinner spinner = viewInflated.findViewById(R.id.spinnerJenis);
            final AppCompatEditText editRkb = viewInflated.findViewById(R.id.editRkb);
            final AppCompatEditText editTglDari = viewInflated.findViewById(R.id.editTglDari);
            final AppCompatEditText editTglKe = viewInflated.findViewById(R.id.editTglKe);
            editRkb.setText(param.getNama_rkb());
            editTglDari.setText(param.getDari_tanggal());
            editTglKe.setText(param.getKe_tanggal());
            Button but_filter = viewInflated.findViewById(R.id.but_filter);
            Button but_clear_filter = viewInflated.findViewById(R.id.but_clear_filter);
            ImageButton but_close = viewInflated.findViewById(R.id.bt_close);

            builder.setOnKeyListener(new Dialog.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface arg0, int keyCode,
                                     KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    }
                    return true;
                }
            });
            List<JenisKeg> jenisKegs = readJson();

            ArrayList<String> jenisArray = new ArrayList<>();
            for (int i = 0; i < jenisKegs.size(); i++) {
                jenisArray.add(jenisKegs.get(i).getNama());
                Log.e("wkwkwkwk","ah"+jenisKegs.get(i).getNama());

            }
            ArrayList<String> jenisArrayId = new ArrayList<>();
            for (int i = 0; i < jenisKegs.size(); i++) {
                jenisArrayId.add(jenisKegs.get(i).getId());
                Log.e("wkwkwkwk","ah"+jenisKegs.get(i).getNama());

            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, jenisArray);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            spinner.setSelection(jenisArrayId.indexOf(param.getJenis_kegiatan())+1);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    if (mLastSpinnerPosition == i) {
                        return; //do nothing
                    }
                    mLastSpinnerPosition = i;

                    if (mLastSpinnerPosition == -1) {

                    } else {
                        jenisFilter = jenisKegs.get(i).getId();

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            builder.setView(viewInflated);
            final AlertDialog dialog_alert = builder.create();
            dialog_alert.show();
            dialog_alert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager.LayoutParams wmlp = dialog_alert.getWindow().getAttributes();
            wmlp.gravity = Gravity.CENTER_HORIZONTAL;

            but_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog_alert.dismiss();

                }
            });
            editRkb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(ListKegiatanActivityAdmin.this, ListRkbActivityAdmin.class);
                    i.putExtra("src","listKegiatan");
                    startActivityForResult(i,300);
                    dialog_alert.dismiss();
                }
            });
            editTglDari.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DatePickerDialog d = new DatePickerDialog(ListKegiatanActivityAdmin.this,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker datePicker, int arg1, int arg2, int arg3) {
                                    dateFrom = Integer.toString(arg1) + "-" + Integer.toString(arg2+1) + "-" + Integer.toString(arg3);
                                    editTglDari.setText(dateFrom);
                                    param.setDari_tanggal(dateFrom);

                                }
                            }, year, month, day);
                    d.show();
                }
            });

            editTglKe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DatePickerDialog d = new DatePickerDialog(ListKegiatanActivityAdmin.this,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker datePicker, int arg1, int arg2, int arg3) {
                                   dateTo = Integer.toString(arg1) + "-" + Integer.toString(arg2+1) + "-" + Integer.toString(arg3);
                                    editTglKe.setText(dateTo);
                                    param.setKe_tanggal(dateTo);
                                }
                            }, year, month, day);
                    d.show();

                }
            });


            but_filter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    param.setPage("1");
                    param.setDari_tanggal(editTglDari.getText().toString());
                    param.setKe_tanggal(editTglKe.getText().toString());
                    param.setSearch(searchView.getQuery());
                    param.setJenis_kegiatan(jenisFilter);
                    param.setNama_rkb(editRkb.getText().toString());
                    reqData(param);
                    dialog_alert.dismiss();
                }
            });
            but_clear_filter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    param = new ListSearchParam();
                    param.setPage("1");
                    reqData(param);
                    dialog_alert.dismiss();
                }
            });
        }
    }

    private List<JenisKeg> readJson() {
        List<JenisKeg> jenisKeg = new ArrayList<>();
        InputStream is = getResources().openRawResource(R.raw.jenis_kegiatan);
        try {
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String json = new String(buffer, "UTF-8");
            Gson gson = new Gson();
            Type type = new TypeToken<List<JenisKeg>>(){}.getType();
            jenisKeg =  gson.fromJson(json, type);
        } catch (IOException e) {

        }
        return jenisKeg;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==300){
            if(resultCode==RESULT_OK){
                rkb = (RkbList) data.getSerializableExtra("data");
                param.setNama_rkb(rkb.getRkb_name());
                dialog_filter();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }



}
