package id.triwikrama.rkbfasilitator.activity.admin;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.activity.EntryActivity;
import id.triwikrama.rkbfasilitator.adapter.ListKegiatanAdapter;
import id.triwikrama.rkbfasilitator.adapter.ListUserAdapter;
import id.triwikrama.rkbfasilitator.fragment.DetailFragment;
import id.triwikrama.rkbfasilitator.fragment.DetailRkbFragment;
import id.triwikrama.rkbfasilitator.fragment.DetailUserFragment;
import id.triwikrama.rkbfasilitator.fragment.MyDialogCloseListener;
import id.triwikrama.rkbfasilitator.model.JenisKeg;
import id.triwikrama.rkbfasilitator.model.Kegiatan;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.UserList;
import id.triwikrama.rkbfasilitator.model.param.AddRkb;
import id.triwikrama.rkbfasilitator.model.param.ListFilterParam;
import id.triwikrama.rkbfasilitator.model.param.ListSearchParam;
import id.triwikrama.rkbfasilitator.model.response.GlobalResponse;
import id.triwikrama.rkbfasilitator.model.response.ListResponse;
import id.triwikrama.rkbfasilitator.model.response.UserListResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class ListUserActivityAdmin extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.shimmer)
    ShimmerFrameLayout shimmer;
    @BindView(R.id.recylerview)
    RecyclerView recyclerView;
    @BindView(R.id.search_view)
    FloatingSearchView searchView;
    @BindView(R.id.txtDataKosong)
    TextView txtDataKosong;
    @BindView(R.id.fab_add)
    FloatingActionButton fab_add;

    private String src;
    private User userData;
    private NetworkService mNetworkService;
    private CustomDialog cd;
    private DatabaseHelper db;
    private DetailFragment detailFragment = null;
    private final AlertDialog.Builder builder = null;
    private Dialog dialog;
    private ListSearchParam param = new ListSearchParam();
    private DetailUserFragment detailUserFragment;

    private ListUserAdapter mAdapter;
    private List<UserList> userLists = new ArrayList<>();

    private boolean isNormal = true, isSearch = false, isFilter = false, isRequested = false;

    private Calendar calendar;
    private int year, month, day;
    private int mLastSpinnerPosition = 0;
    private String typeFilter = "";
    private int logPage = 1;

    @OnClick(R.id.fab_add)
    void addData() {
        isRequested = false;
        Intent i = new Intent(this, AddUserActivity.class);
        i.putExtra("src", "Tambah User");
        startActivity(i);
        overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_list);
        ButterKnife.bind(this);
        initClass();
        initToolbar();
        param.setPage("1");
        reqData(param);
        searchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {

            }

            @Override
            public void onSearchAction(String currentQuery) {
                param.setPage("1");
                param.setSearch(currentQuery);
                reqData(param);
            }
        });
        searchView.setOnClearSearchActionListener(new FloatingSearchView.OnClearSearchActionListener() {
            @Override
            public void onClearSearchClicked() {
                param.setPage("1");
                param.setSearch("");
                reqData(param);
            }
        });
    }

    void initClass() {
        cd = new CustomDialog(this);
        db = new DatabaseHelper(this);
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
        userData = db.getUser();
    }

    private void reqData(ListSearchParam param) {
        logPage = 1;
        isRequested = true;
        userLists.clear();
        shimmer.startShimmer();
        Call<UserListResponse> reqData = mNetworkService.listUser(userData.getUser_token(), param);
        reqData.enqueue(new Callback<UserListResponse>() {
            @Override
            public void onResponse(Call<UserListResponse> call, Response<UserListResponse> response) {
                shimmer.stopShimmer();
                shimmer.setVisibility(View.GONE);
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        recyclerView.setVisibility(View.VISIBLE);
                        userLists.addAll(response.body().getData());
                        generateList(userLists);
                    } else {
                        isNormal = true;
                        isFilter = false;
                        isSearch = false;
                        recyclerView.setVisibility(View.GONE);
                        txtDataKosong.setVisibility(View.VISIBLE);
                        txtDataKosong.setText("Data tidak ditemukan");
                        shimmer.setVisibility(View.GONE);
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }

            }

            @Override
            public void onFailure(Call<UserListResponse> call, Throwable t) {
                shimmer.stopShimmer();
                shimmer.setVisibility(View.GONE);
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }

    final String GetString(int string) {
        return ListUserActivityAdmin.this.getString(string);
    }


    private void generateList(List<UserList> userLists) {
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setHasFixedSize(true);
        mAdapter = new ListUserAdapter(this, userLists.size(), userLists);
        recyclerView.setAdapter(mAdapter);
        if (userLists.size() > 7) {
            mAdapter.setOnLoadMoreListener(new ListUserAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore(int current_page) {
                    logPage++;
                    nextPage(logPage);
                }
            });
        }
        mAdapter.setOnItemClickListener(new ListUserAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, UserList obj, int position) {
                if (detailUserFragment == null || !detailUserFragment.isAdded()) {
                    detailUserFragment = new DetailUserFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("src", src);
                    bundle.putSerializable("data", obj);
                    detailUserFragment.setArguments(bundle);
                    detailUserFragment.show(getSupportFragmentManager(), detailUserFragment.getTag());
                    MyDialogCloseListener closeListener = new MyDialogCloseListener() {
                        @Override
                        public void handleDialogClose(DialogInterface dialog) {
                            reqData(param);
                        }
                    };
                    detailUserFragment.DismissListener(closeListener);
                }
            }
        });
        txtDataKosong.setVisibility(View.GONE);
    }

    void nextPage(int current_page) {
        if (current_page > 1) {
            shimmer.setVisibility(View.GONE);
            mAdapter.setLoading();
            param.setPage(Integer.toString(current_page));
            reqNextData(param);
        }
    }

    private void reqNextData(ListSearchParam param) {
        Call<UserListResponse> reqData = mNetworkService.listUser(userData.getUser_token(), param);
        reqData.enqueue(new Callback<UserListResponse>() {
            @Override
            public void onResponse(Call<UserListResponse> call, Response<UserListResponse> response) {
                shimmer.stopShimmer();
                shimmer.setVisibility(View.GONE);
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        mAdapter.insertData(response.body().getData());
                    } else {
                        mAdapter.setLoaded();
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }

            }

            @Override
            public void onFailure(Call<UserListResponse> call, Throwable t) {
                shimmer.stopShimmer();
                shimmer.setVisibility(View.GONE);
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }


    void initToolbar() {
        txtDataKosong.setVisibility(View.GONE);
        Intent i = getIntent();
        src = i.getStringExtra("src");
        this.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Daftar " + src);
        if (getSupportActionBar() != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Daftar " + src);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.data_refresh, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuRefresh:
                param = new ListSearchParam();
                param.setPage("1");
                reqData(param);
                break;
            default:
                break;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isRequested) {
            param = new ListSearchParam();
            param.setPage("1");
            reqData(param);
        }
    }
}
