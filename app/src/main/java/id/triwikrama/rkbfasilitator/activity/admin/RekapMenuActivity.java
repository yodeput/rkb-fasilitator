package id.triwikrama.rkbfasilitator.activity.admin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.Profile;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;

public class RekapMenuActivity extends AppCompatActivity {

    private DatabaseHelper db;
    private CustomDialog cd;
    private Profile profileData;
    private NetworkService mNetworkService;
    private Intent intent;

    @BindView(R.id.toolbar)Toolbar toolbar;

    @OnClick({R.id.menu_kelas, R.id.menu_treg, R.id.menu_kunjungan, R.id.menu_pelatihan,
            R.id.menu_event, R.id.menu_admin})
    public void setViewOnClickEvent(View view) {
        switch (view.getId()) {
            case R.id.menu_kelas:
                /*intent = new Intent(this, ListKegiatanActivityAdmin.class);
                intent.putExtra("src", getString(R.string.menu_kunjungan));
                startActivity(intent);
                overridePendingTransition(R.anim.anim_slide_in_right,R.anim.anim_slide_out_left);*/
                break;
            case R.id.menu_treg:

                break;
            case R.id.menu_kunjungan:

                break;
            case R.id.menu_pelatihan:

                break;
            case R.id.menu_event:

                break;
            case R.id.menu_admin:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rekap_menu);
        ButterKnife.bind(this);
        //toolbar = (Toolbar) findViewById(R.id.toolbar);

        initToolbar();
    }

    void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("UserList Rekap");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("UserList Rekap");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }


}
