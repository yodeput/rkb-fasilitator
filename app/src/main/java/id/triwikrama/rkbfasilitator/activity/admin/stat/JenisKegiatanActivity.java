package id.triwikrama.rkbfasilitator.activity.admin.stat;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.model.GradientColor;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.textfield.TextInputEditText;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.whiteelephant.monthpicker.MonthPickerDialog;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import github.nisrulz.screenshott.ScreenShott;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.activity.ProfileEditActivity;
import id.triwikrama.rkbfasilitator.activity.admin.AddUserActivity;
import id.triwikrama.rkbfasilitator.activity.admin.ListRkbActivityAdmin;
import id.triwikrama.rkbfasilitator.activity.admin.RekapRkbActivity;
import id.triwikrama.rkbfasilitator.model.Profile;
import id.triwikrama.rkbfasilitator.model.RkbList;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.param.EditPhotoParam;
import id.triwikrama.rkbfasilitator.model.param.ProfileParam;
import id.triwikrama.rkbfasilitator.model.param.RekapParam;
import id.triwikrama.rkbfasilitator.model.response.GlobalResponse;
import id.triwikrama.rkbfasilitator.model.response.ProfileResponse;
import id.triwikrama.rkbfasilitator.model.statistik.Jenis;
import id.triwikrama.rkbfasilitator.model.statistik.JenisResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.Apps;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import id.triwikrama.rkbfasilitator.utils.PrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class JenisKegiatanActivity extends AppCompatActivity {

    private User userData;
    private DatabaseHelper db;
    private CustomDialog cd;
    private Profile profileData;
    private NetworkService mNetworkService;
    final String[] jnsKegiatan = {"Kunjungan", "Pelatihan", "Event", "Administrasi"};
    private String rkb_id="all";
    private RkbList rkb;
    private String bulan, tahun;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rootView)
    LinearLayout rootView;
    @BindView(R.id.editRkb)
    TextInputEditText editRkb;
    @BindView(R.id.editTahun)
    TextInputEditText editTahun;
    @BindView(R.id.barchart)
    BarChart chart;

    @OnClick(R.id.editRkb)
    void pilih() {
        Intent i = new Intent(JenisKegiatanActivity.this, ListRkbActivityAdmin.class);
        i.putExtra("src", "stat");
        startActivityForResult(i, 300);
    }

    @OnClick(R.id.editTahun)
    void pilihTahun() {
        showYearPicker();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.statistik_jenis_kegiatan);
        ButterKnife.bind(this);
        initClass();
        initToolbar();

        Calendar today = Calendar.getInstance();
        DateFormat yearFormat = new SimpleDateFormat("yyyy");
        DateFormat monthFormat = new SimpleDateFormat("MM");
        Date date = new Date();

        tahun = yearFormat.format(date);
        bulan = monthFormat.format(date);
        editTahun.setText(tahun);
        createParam();

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 300) {
            if (resultCode == RESULT_OK) {
                rkb = (RkbList) data.getSerializableExtra("data");
                rkb_id = rkb.getRkb_id();
                editRkb.setText(rkb.getRkb_name());
                createParam();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_screenshot, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuScreenshot:
                Bitmap bitmap = ScreenShott.getInstance().takeScreenShotOfView(rootView);
                saveScreenshot(bitmap);
                break;
            default:
                break;
        }
        return true;
    }

    private void saveScreenshot(Bitmap bitmap) {
        String filename ="statistik_kegiatan_rkbId_"+rkb_id+"_"+tahun+".jpg";
        File file = Apps.getInstance().storeImage(bitmap,filename);
        cd.toastSuccess("Screenshot tersimpan\n" + file.getAbsolutePath());
    }

    void initClass() {
        cd = new CustomDialog(this);
        db = new DatabaseHelper(this);
        userData = db.getUser();
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
    }

    void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Stats Jenis Kegiatan");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Stats Jenis Kegiatan");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }

    private void createParam() {
        RekapParam param = new RekapParam();
        param.setTahun(editTahun.getText().toString());
        param.setRkb_id(rkb_id);
        reqData(param);
    }

    private void reqData(RekapParam param) {
        cd.show_p_Dialog();
        chart.setVisibility(View.GONE);
        Call<JenisResponse> reqJenisStat = mNetworkService.statJenis(userData.getUser_token(), param);
        reqJenisStat.enqueue(new Callback<JenisResponse>() {
            @Override
            public void onResponse(Call<JenisResponse> call, Response<JenisResponse> response) {
                cd.hide_p_Dialog();
                int code = response.code();
                if (code == 200) {
                    if (response.body().getSuccess() == 1) {
                        generateView(response.body().getData());
                    } else {

                    }
                } else {
                    cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }
            }

            @Override
            public void onFailure(Call<JenisResponse> call, Throwable t) {
            cd.hide_p_Dialog();
                cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
            }
        });
    }

    private void generateView(List<Jenis> dataList) {
        if(dataList.get(0).getKunjungan()!=null) {
            chart.setVisibility(View.VISIBLE);
            chart.setDrawBarShadow(false);
            chart.setDrawValueAboveBar(true);
            chart.getDescription().setEnabled(false);
            chart.setMaxVisibleValueCount(1000);
            chart.setPinchZoom(false);
            chart.setDrawGridBackground(false);
            chart.animateX(1200, Easing.EaseInBounce);

            ArrayList<BarEntry> values = new ArrayList<>();
            values.add(new BarEntry(0, Integer.parseInt(dataList.get(0).getKunjungan())));
            values.add(new BarEntry(1, Integer.parseInt(dataList.get(0).getPelatihan())));
            values.add(new BarEntry(2, Integer.parseInt(dataList.get(0).getEvent())));
            values.add(new BarEntry(3, Integer.parseInt(dataList.get(0).getAdministrasi())));

            BarDataSet set1 = new BarDataSet(values, "");
            set1.setDrawIcons(false);

            int blue = ContextCompat.getColor(this, R.color.blue_600);
            int orange = ContextCompat.getColor(this, R.color.orange_600);
            int red = ContextCompat.getColor(this, R.color.red_600);
            int green = ContextCompat.getColor(this, R.color.green_600);
            List<GradientColor> gradientColors = new ArrayList<>();
            gradientColors.add(new GradientColor(blue, blue));
            gradientColors.add(new GradientColor(orange, orange));
            gradientColors.add(new GradientColor(red, red));
            gradientColors.add(new GradientColor(green, green));
            set1.setGradientColors(gradientColors);
            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            XAxis xAxis = chart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setValueFormatter(new ValueFormatter() {
                @Override
                public String getAxisLabel(float value, AxisBase axis) {
                    return jnsKegiatan[(int) value];
                }
            });

            xAxis.setDrawGridLines(true);
            xAxis.setGranularity(1f);
            xAxis.setGranularityEnabled(true);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setBarWidth(0.9f);
            chart.getLegend().setEnabled(false);
            chart.setData(data);
            chart.setFitBars(true);
        } else {
            chart.setVisibility(View.GONE);
           cd.toastWarning("Data Statistik belum tersedia");
        }
    }


    private void showYearPicker() {
        Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(JenisKegiatanActivity.this,
                new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) {
                        tahun = Integer.toString(selectedYear);
                        editTahun.setText(tahun);
                        createParam();
                    }
                }, today.get(Calendar.YEAR), Integer.parseInt(bulan) - 1);
        builder.setMinYear(2019)
                .setActivatedYear(Integer.parseInt(tahun))
                .setMaxYear(2030)
                .showYearOnly()
                .build()
                .show();

    }

}
