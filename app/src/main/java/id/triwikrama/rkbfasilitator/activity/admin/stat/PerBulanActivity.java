package id.triwikrama.rkbfasilitator.activity.admin.stat;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.textfield.TextInputEditText;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import github.nisrulz.screenshott.ScreenShott;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.activity.admin.ListRkbActivityAdmin;
import id.triwikrama.rkbfasilitator.model.Profile;
import id.triwikrama.rkbfasilitator.model.RkbList;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.param.RekapParam;
import id.triwikrama.rkbfasilitator.model.statistik.JenisResponse;
import id.triwikrama.rkbfasilitator.model.statistik.Perbulan;
import id.triwikrama.rkbfasilitator.model.statistik.PerbulanResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.Apps;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PerBulanActivity extends AppCompatActivity {

    private User userData;
    private DatabaseHelper db;
    private CustomDialog cd;
    private NetworkService mNetworkService;
    final ArrayList<String> bulanList = new ArrayList<>();

    private String rkb_id="all";
    private RkbList rkb;
    private String bulan, tahun;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rootView)
    LinearLayout rootView;
    @BindView(R.id.editRkb)
    TextInputEditText editRkb;
    @BindView(R.id.editTahun)
    TextInputEditText editTahun;
    @BindView(R.id.chart)
    LineChart lineChart;

    @OnClick(R.id.editRkb)
    void pilih() {
        Intent i = new Intent(PerBulanActivity.this, ListRkbActivityAdmin.class);
        i.putExtra("src", "stat");
        startActivityForResult(i, 300);
    }

    @OnClick(R.id.editTahun)
    void pilihTahun() {
        showYearPicker();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.statistik_perbulan);
        ButterKnife.bind(this);
        initClass();
        initToolbar();

        Calendar today = Calendar.getInstance();
        DateFormat yearFormat = new SimpleDateFormat("yyyy");
        DateFormat monthFormat = new SimpleDateFormat("MM");
        Date date = new Date();

        tahun = yearFormat.format(date);
        bulan = monthFormat.format(date);
        editTahun.setText(tahun);
        createParam();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 300) {
            if (resultCode == RESULT_OK) {
                rkb = (RkbList) data.getSerializableExtra("data");
                rkb_id = rkb.getRkb_id();
                editRkb.setText(rkb.getRkb_name());
                createParam();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_screenshot, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuScreenshot:
                Bitmap bitmap = ScreenShott.getInstance().takeScreenShotOfView(rootView);
                saveScreenshot(bitmap);
                break;
            default:
                break;
        }
        return true;
    }

    private void saveScreenshot(Bitmap bitmap) {
        String filename ="statistik_per_bulan_rkbId_"+rkb_id+"_"+tahun+".jpg";
        File file = Apps.getInstance().storeImage(bitmap,filename);
        cd.toastSuccess("Screenshot tersimpan\n" + file.getAbsolutePath());
    }

    void initClass() {
        cd = new CustomDialog(this);
        db = new DatabaseHelper(this);
        userData = db.getUser();
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
    }

    void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Stats Jenis Kegiatan");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Stats Jenis Kegiatan");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }

    private void createParam() {
        RekapParam param = new RekapParam();
        param.setTahun(editTahun.getText().toString());
        param.setRkb_id(rkb_id);
        reqData(param);
    }

    private void reqData(RekapParam param) {
        cd.show_p_Dialog();
        lineChart.setVisibility(View.GONE);
        Call<PerbulanResponse> reqJenisStat = mNetworkService.statPerbulan(userData.getUser_token(), param);
        reqJenisStat.enqueue(new Callback<PerbulanResponse>() {
            @Override
            public void onResponse(Call<PerbulanResponse> call, Response<PerbulanResponse> response) {
                cd.hide_p_Dialog();
                int code = response.code();
                if (code == 200) {
                    if (response.body().getSuccess() == 1) {
                        generateView(response.body().getData());
                    } else {

                    }
                } else {

                    cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

                }
            }

            @Override
            public void onFailure(Call<PerbulanResponse> call, Throwable t) {
                cd.hide_p_Dialog();
                cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }


    private void generateView(List<Perbulan> dataList){
        if(dataList.size()>1||dataList!=null) {
            lineChart.setVisibility(View.VISIBLE);
            lineChart.setTouchEnabled(true);
            lineChart.setPinchZoom(false);
            lineChart.animateX(1200, Easing.EaseInBounce);

            ArrayList<Entry> values = new ArrayList<>();
            for (int i = 0; i < dataList.size(); i++) {
                values.add(new Entry(i, Integer.parseInt(dataList.get(i).getValue())));
                bulanList.add(dataList.get(i).getBulan().toUpperCase());
            }

            LineDataSet set1;
            if (lineChart.getData() != null &&
                    lineChart.getData().getDataSetCount() > 0) {
                set1 = (LineDataSet) lineChart.getData().getDataSetByIndex(0);
                set1.setValues(values);
                lineChart.getData().notifyDataChanged();
                lineChart.notifyDataSetChanged();
            } else {
                set1 = new LineDataSet(values, "Sample UserList");
                set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                set1.setValueTextSize(14f);
                set1.setCubicIntensity(0.05f);
                set1.setDrawFilled(true);
                set1.setDrawCircles(true);
                set1.setLineWidth(1.8f);
                set1.setCircleRadius(4f);
                set1.setCircleColor(Color.RED);
                set1.setHighLightColor(Color.rgb(244, 117, 117));
                set1.setColor(Color.RED);
                set1.setFillColor(Color.RED);
                set1.setFillAlpha(100);
                set1.setDrawHorizontalHighlightIndicator(false);
                set1.setFillFormatter(new IFillFormatter() {
                    @Override
                    public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                        return lineChart.getAxisLeft().getAxisMinimum();
                    }
                });


            /*set1.setDrawIcons(false);
            set1.enableDashedLine(10f, 5f, 0f);
            set1.enableDashedHighlightLine(10f, 5f, 0f);
            set1.setColor(Color.DKGRAY);
            set1.setCircleColor(Color.DKGRAY);
            set1.setLineWidth(1f);
            set1.setCircleRadius(3f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);
            if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(this, R.drawable.grad_red);
                set1.setFillDrawable(drawable);
            } else {
                set1.setFillColor(Color.DKGRAY);
            }*/
                lineChart.getAxisRight().setEnabled(false);
                XAxis xAxis = lineChart.getXAxis();
                xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                xAxis.setValueFormatter(new IndexAxisValueFormatter(bulanList));
                xAxis.setDrawGridLines(true);
                xAxis.setGranularity(1f);
                xAxis.setGranularityEnabled(true);
                lineChart.getLegend().setEnabled(false);
                lineChart.getDescription().setEnabled(false);
                ArrayList<ILineDataSet> dataSets = new ArrayList<>();
                dataSets.add(set1);
                LineData data = new LineData(dataSets);
                lineChart.setData(data);
            }
        } else {
            cd.toastWarning("Data Statistik belum tersedia");
        }
    }

    private void showYearPicker() {
        Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(PerBulanActivity.this,
                new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) {
                        tahun = Integer.toString(selectedYear);
                        editTahun.setText(tahun);
                        createParam();
                    }
                }, today.get(Calendar.YEAR), Integer.parseInt(bulan) - 1);
        builder.setMinYear(2019)
                .setActivatedYear(Integer.parseInt(tahun))
                .setMaxYear(2030)
                .showYearOnly()
                .build()
                .show();

    }

}
