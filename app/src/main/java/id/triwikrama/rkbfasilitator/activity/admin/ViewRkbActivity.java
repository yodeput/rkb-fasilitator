package id.triwikrama.rkbfasilitator.activity.admin;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.RkbList;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.param.AddRkb;
import id.triwikrama.rkbfasilitator.model.response.GlobalResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import id.triwikrama.rkbfasilitator.utils.PrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewRkbActivity extends AppCompatActivity {

    private User userData;
    private CustomDialog cd;
    private DatabaseHelper db;
    private PrefManager pref;
    private NetworkService mNetworkService;
    private String src;
    private int spinnerPos = -1;
    private RkbList rkbList;
    private ArrayList<String> kelas =  new ArrayList<>();
    private ArrayList<String> tr =  new ArrayList<>();
    private Dialog dialog;

    @BindView(R.id.txtNama)
    TextView txtNama;
    @BindView(R.id.txtKelas)
    TextView txtKelas;
    @BindView(R.id.txtTr)
    TextView txtTr;


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @OnClick(R.id.but_edit) void edit(){
        startActivity(new Intent(ViewRkbActivity.this, AddRkbActivity.class)
                .putExtra("data",rkbList)
                .putExtra("src","Edit RKB")
                .putStringArrayListExtra("kelas", kelas)
                .putStringArrayListExtra("tr", tr));
    }

    @OnClick(R.id.but_delete) void delete(){

        AddRkb param = new AddRkb();
        param.setRkb_id(rkbList.getRkb_id());
        dialogDelete(param);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_rkb);
        ButterKnife.bind(this);
        initClass();
        initToolbar();
        Intent i = getIntent();
        src = i.getStringExtra("src");
        rkbList = (RkbList) i.getSerializableExtra("data");
        txtNama.setText(rkbList.getRkb_name());
        txtKelas.setText(rkbList.getRkb_kelas());
        txtTr.setText(rkbList.getRkb_treg());
        kelas = i.getStringArrayListExtra("kelas");
        tr = i.getStringArrayListExtra("tr");


    }

    void initClass() {
        cd = new CustomDialog(this);
        pref = new PrefManager(this);
        db = new DatabaseHelper(this);
        userData = db.getUser();
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
    }

    void initToolbar() {
        Intent i = getIntent();
        src = "KegiatanDetail RKB";
        this.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(src);
        if (getSupportActionBar() != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(src);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.form_submit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSubmit:

                break;
            default:
                break;
        }
        return true;
    }

    public void dialogDelete(AddRkb param) {
        if(dialog==null||!dialog.isShowing()) {
            dialog = new Dialog(ViewRkbActivity.this, R.style.DialogSlideAnim);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_two_button);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER_HORIZONTAL;
            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setAttributes(lp);


            dialog.setCanceledOnTouchOutside(false);
            dialog.setOnKeyListener(new Dialog.OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface arg0, int keyCode,
                                     KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                    }
                    return true;
                }
            });

            ((ImageView) dialog.findViewById(R.id.img_dialog)).setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
            ((TextView) dialog.findViewById(R.id.txt_message)).setText(GetString(R.string.textHapus));
            ((Button) dialog.findViewById(R.id.bt_positive)).setText(GetString(R.string.but_ya));
            ((Button) dialog.findViewById(R.id.bt_positive)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    deleteRkb(param);
                }
            });
            ((Button) dialog.findViewById(R.id.bt_negative)).setText(GetString(R.string.but_nlogout));
            ((Button) dialog.findViewById(R.id.bt_negative)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }
    }

    final String GetString(int string) {
        return ViewRkbActivity.this.getString(string);
    }


    private void deleteRkb(AddRkb param){
        cd.show_p_Dialog();
        Call<GlobalResponse> delRkb = mNetworkService.deleteRkb(db.getUser().getUser_token(), param);
        delRkb.enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                cd.hide_p_Dialog();
                int code = response.code();
                if (code == 200) {
                    if (response.body().getSuccess()==1) {
                        cd.oneButtonDialog_finish(response.body().getMessage(), R.drawable.ic_user, R.color.green_700);

                    } else {
                        cd.oneButtonDialog_finish(response.body().getMessage(), R.drawable.ic_user, R.color.red_700);
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

                }
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                cd.hide_p_Dialog();
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }


}
