package id.triwikrama.rkbfasilitator.activity.admin.stat;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.model.GradientColor;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.google.android.material.textfield.TextInputEditText;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import github.nisrulz.screenshott.ScreenShott;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.Profile;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.param.RekapParam;
import id.triwikrama.rkbfasilitator.model.statistik.JenisResponse;
import id.triwikrama.rkbfasilitator.model.statistik.Kelas;
import id.triwikrama.rkbfasilitator.model.statistik.KelasResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.Apps;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KelasRKBActivity extends AppCompatActivity {

    private User userData;
    private DatabaseHelper db;
    private CustomDialog cd;
    private Profile profileData;
    private NetworkService mNetworkService;
    private String bulan, tahun;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rootView)
    LinearLayout rootView;
    @BindView(R.id.editTahun)
    TextInputEditText editTahun;
    @BindView(R.id.chart)
    PieChart pieChart;

    @OnClick(R.id.editTahun)
    void pilihTahun() {
        showYearPicker();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.statistik_kelas_rkb);
        ButterKnife.bind(this);
        initClass();
        initToolbar();

        Calendar today = Calendar.getInstance();
        DateFormat yearFormat = new SimpleDateFormat("yyyy");
        DateFormat monthFormat = new SimpleDateFormat("MM");
        Date date = new Date();

        tahun = yearFormat.format(date);
        bulan = monthFormat.format(date);
        editTahun.setText(tahun);
        createParam();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_screenshot, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuScreenshot:
                Bitmap bitmap = ScreenShott.getInstance().takeScreenShotOfView(rootView);
                saveScreenshot(bitmap);
                break;
            default:
                break;
        }
        return true;
    }

    private void saveScreenshot(Bitmap bitmap) {
        String filename = "statistik_kelas_"+ tahun + ".jpg";
        File file = Apps.getInstance().storeImage(bitmap, filename);
        cd.toastSuccess("Screenshot tersimpan\n" + file.getAbsolutePath());
    }

    void initClass() {
        cd = new CustomDialog(this);
        db = new DatabaseHelper(this);
        userData = db.getUser();
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
    }

    void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Stats Kelas RKB");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Stats Kelas RKB");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }

    private void createParam() {
        RekapParam param = new RekapParam();
        param.setTahun(editTahun.getText().toString());
        reqData(param);
    }

    private void reqData(RekapParam param) {
        cd.show_p_Dialog();
        pieChart.setVisibility(View.GONE);
        Call<KelasResponse> reqKelasStat = mNetworkService.statKelas(userData.getUser_token(), param);
        reqKelasStat.enqueue(new Callback<KelasResponse>() {
            @Override
            public void onResponse(Call<KelasResponse> call, Response<KelasResponse> response) {
                cd.hide_p_Dialog();
                int code = response.code();
                if (code == 200) {
                    if (response.body().getSuccess() == 1) {
                        generateView(response.body().getData());
                    } else {

                    }
                } else {
                    cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

                }
            }

            @Override
            public void onFailure(Call<KelasResponse> call, Throwable t) {
                cd.hide_p_Dialog();
                cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
            }
        });
    }

    private void generateView(List<Kelas> dataList) {
        pieChart.setVisibility(View.VISIBLE);
        pieChart.setUsePercentValues(false);
        pieChart.getDescription().setEnabled(false);
        pieChart.setRotationEnabled(true);
        pieChart.setExtraOffsets(5, 5, 5, 5);
        pieChart.animateY(1000, Easing.EaseInOutQuad);

        ArrayList<PieEntry> yvalues = new ArrayList<PieEntry>();
        yvalues.add(new PieEntry(Integer.parseInt(dataList.get(0).getKelas_a()), "Kelas A", 0));
        yvalues.add(new PieEntry(Integer.parseInt(dataList.get(0).getKelas_b()), "Kelas B", 1));
        yvalues.add(new PieEntry(Integer.parseInt(dataList.get(0).getKelas_c()), "Kelas C", 2));

        PieDataSet dataSet = new PieDataSet(yvalues, "");

        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(20f);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(13f);
        data.setValueTextColor(Color.BLACK);
        data.setValueTypeface(Typeface.DEFAULT_BOLD);

        pieChart.setData(data);
        pieChart.setDrawEntryLabels(true);
        pieChart.setDrawHoleEnabled(false);
        pieChart.setTransparentCircleRadius(58f);

        int blue = ContextCompat.getColor(this, R.color.blue_500);
        int orange = ContextCompat.getColor(this, R.color.orange_500);
        int red = ContextCompat.getColor(this, R.color.red_500);
        ArrayList<Integer> color = new ArrayList<>();
        color.add(blue);
        color.add(orange);
        color.add(red);

        Legend legend = pieChart.getLegend();
        legend.getEntries();
        legend.setYEntrySpace(10f);
        legend.setWordWrapEnabled(true);
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        LegendEntry l1=new LegendEntry("Kelas A",Legend.LegendForm.CIRCLE,15f,5f,null,color.get(0));
        LegendEntry l2=new LegendEntry("Kelas B", Legend.LegendForm.CIRCLE,15f,5f,null,color.get(1));
        LegendEntry l3=new LegendEntry("Kelas C", Legend.LegendForm.CIRCLE,15f,5f,null,color.get(2));
        LegendEntry[] custom = {l1,l2};
        legend.setCustom(custom);
        legend.setEnabled(false);


        dataSet.setColors(color);
        pieChart.invalidate();

    }

    private void showYearPicker() {
        Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(KelasRKBActivity.this,
                new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) {
                        tahun = Integer.toString(selectedYear);
                        editTahun.setText(tahun);
                        createParam();
                    }
                }, today.get(Calendar.YEAR), Integer.parseInt(bulan) - 1);
        builder.setMinYear(2019)
                .setActivatedYear(Integer.parseInt(tahun))
                .setMaxYear(2030)
                .showYearOnly()
                .build()
                .show();

    }

}
