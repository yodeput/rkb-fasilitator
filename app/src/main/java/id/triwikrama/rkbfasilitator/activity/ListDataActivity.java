package id.triwikrama.rkbfasilitator.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.triwikrama.rkbfasilitator.adapter.ListKegiatanAdapter;
import id.triwikrama.rkbfasilitator.model.JenisKeg;
import id.triwikrama.rkbfasilitator.model.RkbList;
import id.triwikrama.rkbfasilitator.model.param.ListFilterParam;
import id.triwikrama.rkbfasilitator.model.param.ListSearchParam;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.fragment.DetailFragment;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.adapter.KegiatanAdapter;
import id.triwikrama.rkbfasilitator.model.Kegiatan;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.response.ListResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class ListDataActivity extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.shimmer)
    ShimmerFrameLayout shimmer;
    @BindView(R.id.recylerview)
    RecyclerView recyclerView;
    @BindView(R.id.search_view)
    FloatingSearchView searchView;
    @BindView(R.id.txtDataKosong)
    TextView txtDataKosong;
    private String src;
    private User userData;
    private NetworkService mNetworkService;
    private CustomDialog cd;
    private DatabaseHelper db;
    private DetailFragment detailFragment = null;
    private final AlertDialog.Builder builder = null;

    private ListKegiatanAdapter mAdapter;
    private List<Kegiatan> kegiatans = new ArrayList<>();
    private ListSearchParam param = new ListSearchParam();

    private boolean isNormal=true, isSearch=false, isFilter=false;
    private int logPage=1;

    private Calendar calendar;
    private int year, month, day;
    private int mLastSpinnerPosition = 0;
    private String typeFilter="";

    @OnClick(R.id.fab_add)
    void addData() {
        Intent i = new Intent(this, EntryActivity.class);
        i.putExtra("src", src);
        startActivityForResult(i,300);
        overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_list);
        ButterKnife.bind(this);
        initClass();
        initToolbar();
        param.setType(getInstance().getType(src));
        param.setPage("1");
        reqData(param);

        searchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {

            }

            @Override
            public void onSearchAction(String currentQuery) {
                shimmer.setVisibility(View.VISIBLE);
                kegiatans.clear();
                searchParam(currentQuery);

            }
        });
        searchView.setOnClearSearchActionListener(new FloatingSearchView.OnClearSearchActionListener() {
            @Override
            public void onClearSearchClicked() {
                shimmer.setVisibility(View.VISIBLE);
                param= new ListSearchParam();
                param.setType(getInstance().getType(src));
                param.setPage("1");
                reqData(param);
            }
        });

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

    }

    void initClass() {
        cd = new CustomDialog(this);
        db = new DatabaseHelper(this);
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
        userData = db.getUser();
    }

    private void reqData(ListSearchParam param) {
        logPage=1;
        shimmer.startShimmer();
        Call<ListResponse> reqData = mNetworkService.listKegiatan(userData.getUser_token(), param);
        reqData.enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                kegiatans.clear();
                shimmer.stopShimmer();
                shimmer.setVisibility(View.GONE);
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        kegiatans.addAll(response.body().getKegiatans());
                        generateList(kegiatans);
                    } else {
                        isNormal=true;isFilter=false;isSearch=false;
                        txtDataKosong.setVisibility(View.VISIBLE);
                        shimmer.setVisibility(View.GONE);
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }

            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                shimmer.stopShimmer();
                shimmer.setVisibility(View.GONE);
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }


    private void generateList(List<Kegiatan> kegiatanList) {
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setHasFixedSize(true);
        mAdapter = new ListKegiatanAdapter(this, kegiatanList.size(), kegiatanList);
        recyclerView.setAdapter(mAdapter);
        if(kegiatanList.size()>7) {
            mAdapter.setOnLoadMoreListener(new ListKegiatanAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore(int current_page) {
                    logPage++;
                    nextPage(logPage);
                }
            });
        }
        mAdapter.setOnItemClickListener(new ListKegiatanAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Kegiatan obj, int position) {
                if (detailFragment == null || !detailFragment.isAdded()) {
                    detailFragment = new DetailFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("src", src);
                    bundle.putSerializable("data", obj);
                    detailFragment.setArguments(bundle);
                    detailFragment.show(getSupportFragmentManager(), detailFragment.getTag());
                }
            }
        });
        txtDataKosong.setVisibility(View.GONE);

    }

    void nextPage(int current_page){
        if(current_page>1) {
            shimmer.setVisibility(View.GONE);
            mAdapter.setLoading();
            param.setPage(Integer.toString(current_page));
            reqNextData(param);
        }
    }

    private void reqNextData(ListSearchParam param) {
        Call<ListResponse> reqData = mNetworkService.listKegiatan(userData.getUser_token(), param);
        reqData.enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        mAdapter.insertData(response.body().getKegiatans());
                    } else {
                        mAdapter.setLoaded();
                        isNormal=true;isFilter=false;isSearch=false;
                        txtDataKosong.setVisibility(View.VISIBLE);
                        shimmer.setVisibility(View.GONE);
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }

            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }


    void initToolbar() {
        txtDataKosong.setVisibility(View.GONE);
        Intent i = getIntent();
        src = i.getStringExtra("src");
        this.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Daftar " + src);
        if (getSupportActionBar() != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Daftar " + src);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();
                overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.data_refresh, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menuRefresh:
                searchView.setSearchText("");
                shimmer.setVisibility(View.VISIBLE);
                param= new ListSearchParam();
                param.setType(getInstance().getType(src));
                param.setPage("1");
                reqData(param);
                break;
            default:
                break;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_OK);
        finish();
        overridePendingTransition(R.anim.anim_back_slide_in_right, R.anim.anim_back_slide_out_left);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //shimmer.setVisibility(View.VISIBLE);
    }


    private void searchParam(String query) {
        param.setSearch(query);
        param.setType(src);
        param.setPage("1");
        reqData(param);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==300){
            if(resultCode==RESULT_OK){
                ListSearchParam param = new ListSearchParam();
                param.setType(getInstance().getType(src));
                param.setPage("1");
                reqData(param);
            } else if(resultCode==2121){
                Intent i = new Intent(this, EntryActivity.class);
                i.putExtra("src", src);
                startActivityForResult(i,300);
                overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}

