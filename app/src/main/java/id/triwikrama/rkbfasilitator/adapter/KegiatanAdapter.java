package id.triwikrama.rkbfasilitator.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.esafirm.imagepicker.model.Image;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.Kegiatan;
import id.triwikrama.rkbfasilitator.model.KegiatanPhoto;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class KegiatanAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private List<Kegiatan> kegiatanList;
    private List<Kegiatan> kegiatanListFiltered;
    private Context context;
    private KegiatanListListener listener;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public KegiatanAdapter(Context context, List<Kegiatan> kegiatanList, KegiatanListListener listener) {
        this.context = context;
        this.listener = listener;
        this.kegiatanList = kegiatanList;
        this.kegiatanListFiltered = kegiatanList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_kegiatan, parent, false);
            return new ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof ItemViewHolder) {

            populateItemRows((ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) viewHolder, position);
        }

    }

    private void populateItemRows(ItemViewHolder holder, int position) {
        Kegiatan kegiatan = kegiatanListFiltered.get(position);
        holder.shimmer.startShimmer();
        holder.txtNamaKegiatan.setText(kegiatan.getKegiatan_name());
        holder.txtTanggal.setText(kegiatan.getKegiatan_date());

        ImageView imageView = holder.imageView;
        RequestOptions options = new RequestOptions()
                .fitCenter()
                .error(R.drawable.ic_no_image);
        //KegiatanPhoto photo = kegiatan.getKegiatan_photo();
        String img = getInstance().urlFixer(kegiatan.getKegiatan_photo());
        Glide.with(imageView)
                .load(img)
                .fitCenter()
                .apply(options)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.imageView.setAlpha(0.7f);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.shimmer.stopShimmer();
                        holder.shimmer.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(imageView);

    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    kegiatanListFiltered = kegiatanList;
                } else {
                    List<Kegiatan> filteredList = new ArrayList<>();
                    for (Kegiatan row : kegiatanList) {

                        if (row.getKegiatan_name().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    kegiatanListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = kegiatanListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                kegiatanListFiltered = (ArrayList<Kegiatan>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public int getItemViewType(int position) {
        return kegiatanListFiltered.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return kegiatanListFiltered == null ? 0 : kegiatanListFiltered.size();
    }

    public void add(Kegiatan response) {
        kegiatanListFiltered.add(response);
        notifyItemInserted(kegiatanListFiltered.size() - 1);
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    Kegiatan getItem(int position) {
        return kegiatanListFiltered.get(position);
    }

    private void remove(Kegiatan postItems) {
        int position = kegiatanListFiltered.indexOf(postItems);
        if (position > -1) {
            kegiatanListFiltered.remove(position);
            notifyItemRemoved(position);
        }
    }

    public interface KegiatanListListener {
        void onKegiatanSelected(Kegiatan kegiatan);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView txtNamaKegiatan, txtTanggal, txtNamaUser;
        ShimmerFrameLayout shimmer;

        public ItemViewHolder(View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.imgThumb);
            this.txtNamaUser = (TextView) itemView.findViewById(R.id.txtNamaUser);
            this.txtNamaKegiatan = (TextView) itemView.findViewById(R.id.txtNamaKegiatan);
            this.txtTanggal = (TextView) itemView.findViewById(R.id.txtTanggal);
            this.shimmer = (ShimmerFrameLayout) itemView.findViewById(R.id.shimmer);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onKegiatanSelected(kegiatanListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progress);
        }
    }

    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }
}
