package id.triwikrama.rkbfasilitator.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import java.util.ArrayList;
import java.util.List;

import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.Rekap;
import id.triwikrama.rkbfasilitator.utils.CurrencyFormat;

public class RekapListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROGRESS = 0;

    private int item_per_display = 0;
    private List<Rekap> rekapList;
    private List<Rekap> rekapListFiltered;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener = null;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                FilterResults filterResults = new FilterResults();
                filterResults.values = rekapListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                rekapListFiltered = (ArrayList<Rekap>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Rekap obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public RekapListAdapter(Context context, int item_per_display, List<Rekap> items) {
        this.rekapList = items;
        this.rekapListFiltered = items;
        this.item_per_display = item_per_display;
        ctx = context;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public View lyt_parent;
        TextView txtNamaRkb, txtGmv, txtPelatihan, txtKunjungan, txtEvent,txtAdmin;

        public OriginalViewHolder(View v) {
            super(v);
            this.txtNamaRkb = (TextView) itemView.findViewById(R.id.txtNamaRkb);
            this.txtGmv = (TextView) itemView.findViewById(R.id.txtGmv);
            this.txtKunjungan = (TextView) itemView.findViewById(R.id.txtKunjungan);
            this.txtPelatihan = (TextView) itemView.findViewById(R.id.txtPelatihan);
            this.txtEvent = (TextView) itemView.findViewById(R.id.txtEvent);
            this.txtAdmin = (TextView) itemView.findViewById(R.id.txtAdmin);
            lyt_parent = (View) v.findViewById(R.id.lyt_parent);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progress_bar;

        public ProgressViewHolder(View v) {
            super(v);
            progress_bar = (ProgressBar) v.findViewById(R.id.progress);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rekap_rkb, parent, false);
            vh = new OriginalViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Rekap rekap = rekapListFiltered.get(position);
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;
            ((OriginalViewHolder) holder).txtNamaRkb.setText(rekap.getRkbName());
            String gmv = rekap.getGmv();
            if(gmv!=null){
                ((OriginalViewHolder) holder).txtGmv.setText(new CurrencyFormat().currency_format_string(rekap.getGmv()));
            } else {
                ((OriginalViewHolder) holder).txtGmv.setText("0");
            }
            ((OriginalViewHolder) holder).txtKunjungan.setText(rekap.getTotalKunjungan());
            ((OriginalViewHolder) holder).txtPelatihan.setText(rekap.getTotalPelatihan());
            ((OriginalViewHolder) holder).txtEvent.setText(rekap.getTotalEvent());
            ((OriginalViewHolder) holder).txtAdmin.setText(rekap.getTotalAdm());


            view.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener == null) return;
                    mOnItemClickListener.onItemClick(view, rekap, position);
                }
            });
        } else {
            ((ProgressViewHolder) holder).progress_bar.setIndeterminate(true);
        }

        if (rekap.progress) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        } else {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(false);
        }
    }

    @Override
    public int getItemCount() {
        return rekapListFiltered.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.rekapListFiltered.get(position).progress ? VIEW_PROGRESS : VIEW_ITEM;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        lastItemViewDetector(recyclerView);
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void insertData(List<Rekap> items) {
        if(!this.rekapListFiltered.contains(items)){
            setLoaded();
            int positionStart = getItemCount();
            int itemCount = items.size();
            this.rekapListFiltered.addAll(items);
            notifyItemRangeInserted(positionStart, itemCount);
        }

    }

    public void setLoaded() {
        loading = false;
        for (int i = 0; i < getItemCount(); i++) {
            if (rekapListFiltered.get(i).progress) {
                rekapListFiltered.remove(i);
                notifyItemRemoved(i);
            }
        }
    }

    public void setLoading() {
        if (getItemCount() != 0) {
            this.rekapListFiltered.add(new Rekap(true));
            notifyItemInserted(getItemCount() - 1);
            loading = true;
        }
    }

    public void resetListData() {
        this.rekapList = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    private void lastItemViewDetector(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof StaggeredGridLayoutManager) {
            final StaggeredGridLayoutManager layoutManager = (StaggeredGridLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastPos = getLastVisibleItem(layoutManager.findLastVisibleItemPositions(null));
                    if (!loading && lastPos == getItemCount() - 1 && onLoadMoreListener != null) {
                        int current_page = getItemCount() / item_per_display;
                        onLoadMoreListener.onLoadMore(current_page);
                        loading = true;
                    }
                }
            });
        }
    }

    public interface OnLoadMoreListener {
        void onLoadMore(int current_page);
    }

    private int getLastVisibleItem(int[] into) {
        int last_idx = into[0];
        for (int i : into) {
            if (last_idx < i) last_idx = i;
        }
        return last_idx;
    }
}