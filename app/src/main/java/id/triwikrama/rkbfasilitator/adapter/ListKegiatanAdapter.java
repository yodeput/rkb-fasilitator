package id.triwikrama.rkbfasilitator.adapter;

import android.content.Context;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.Kegiatan;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class ListKegiatanAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROGRESS = 0;

    private int item_per_display = 0;
    private List<Kegiatan> kegiatanList;
    private List<Kegiatan> kegiatanListFiltered;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener = null;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, Kegiatan obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public ListKegiatanAdapter(Context context, int item_per_display, List<Kegiatan> items) {
        this.kegiatanList = items;
        this.item_per_display = item_per_display;
        ctx = context;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public View lyt_parent;
        TextView txtNamaKegiatan, txtTanggal, txtNamaUser;
        ShimmerFrameLayout shimmer;

        public OriginalViewHolder(View v) {
            super(v);
            this.imageView = (ImageView) itemView.findViewById(R.id.imgThumb);
            this.txtNamaKegiatan = (TextView) itemView.findViewById(R.id.txtNamaKegiatan);
            this.txtNamaUser = (TextView) itemView.findViewById(R.id.txtNamaUser);
            this.txtTanggal = (TextView) itemView.findViewById(R.id.txtTanggal);
            this.shimmer = (ShimmerFrameLayout) itemView.findViewById(R.id.shimmer);
            lyt_parent = (View) v.findViewById(R.id.lyt_parent);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progress_bar;

        public ProgressViewHolder(View v) {
            super(v);
            progress_bar = (ProgressBar) v.findViewById(R.id.progress);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_kegiatan, parent, false);
            vh = new OriginalViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Kegiatan kegiatan = kegiatanList.get(position);
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;
            ((OriginalViewHolder) holder).shimmer.startShimmer();
            ((OriginalViewHolder) holder).txtNamaKegiatan.setText(kegiatan.getKegiatan_name());
            ((OriginalViewHolder) holder).txtTanggal.setText(kegiatan.getKegiatan_date());
        ((OriginalViewHolder) holder).txtNamaUser.setText(kegiatan.getUser_name());
            ImageView imageView = ((OriginalViewHolder) holder).imageView;
            RequestOptions options = new RequestOptions()
                    .fitCenter()
                    .error(R.drawable.ic_no_image);
            //KegiatanPhoto photo = kegiatan.getKegiatan_photo();
            String img = getInstance().urlFixer(kegiatan.getKegiatan_photo());
            Glide.with(imageView)
                    .load(img)
                    .fitCenter()
                    .apply(options)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            ((OriginalViewHolder) holder).imageView.setAlpha(0.7f);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            ((OriginalViewHolder) holder).shimmer.stopShimmer();
                            ((OriginalViewHolder) holder).shimmer.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(imageView);
            view.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener == null) return;
                    mOnItemClickListener.onItemClick(view, kegiatan, position);
                }
            });
        } else {
            ((ProgressViewHolder) holder).progress_bar.setIndeterminate(true);
        }

        if (kegiatan.progress) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        } else {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(false);
        }
    }

    @Override
    public int getItemCount() {
        return kegiatanList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.kegiatanList.get(position).progress ? VIEW_PROGRESS : VIEW_ITEM;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        lastItemViewDetector(recyclerView);
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void insertData(List<Kegiatan> items) {
        if(!kegiatanList.contains(items)){
            setLoaded();
            int positionStart = getItemCount();
            int itemCount = items.size();
            this.kegiatanList.addAll(items);

            notifyItemRangeInserted(positionStart, itemCount);
        }

    }

    public void setLoaded() {
        loading = false;
        for (int i = 0; i < getItemCount(); i++) {
            if (kegiatanList.get(i).progress) {
                kegiatanList.remove(i);
                notifyItemRemoved(i);
            }
        }
    }

    public void setLoading() {
        if (getItemCount() != 0) {
            this.kegiatanList.add(new Kegiatan(true));
            notifyItemInserted(getItemCount() - 1);
            loading = true;
        }
    }

    public void resetListData() {
        this.kegiatanList = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    private void lastItemViewDetector(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof StaggeredGridLayoutManager) {
            final StaggeredGridLayoutManager layoutManager = (StaggeredGridLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastPos = getLastVisibleItem(layoutManager.findLastVisibleItemPositions(null));
                    if (!loading && lastPos == getItemCount() - 1 && onLoadMoreListener != null) {
                        int current_page = getItemCount() / item_per_display;
                        onLoadMoreListener.onLoadMore(current_page);
                        loading = true;
                    }
                }
            });
        }
    }

    public interface OnLoadMoreListener {
        void onLoadMore(int current_page);
    }

    private int getLastVisibleItem(int[] into) {
        int last_idx = into[0];
        for (int i : into) {
            if (last_idx < i) last_idx = i;
        }
        return last_idx;
    }
}