package id.triwikrama.rkbfasilitator.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.Leaderboard;

public class LeaderboardAdapter extends RecyclerView.Adapter<LeaderboardAdapter.MyViewHolder> implements Filterable {

    private List<Leaderboard> leaderboardList;
    private List<Leaderboard> leaderboardListFiltered;
    private Context context;
    private LeaderboardListListener listener;

    public LeaderboardAdapter(Context context, List<Leaderboard> leaderboardList, LeaderboardListListener listener) {
        this.context = context;
        this.listener = listener;
        this.leaderboardList = leaderboardList;
        this.leaderboardListFiltered = leaderboardList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_leaderboard, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        Leaderboard leaderboard = leaderboardListFiltered.get(listPosition);
        holder.imgStar.setVisibility(View.GONE);
        int id = leaderboard.getRanking();
        holder.txtId.setText(""+id);
        holder.txtNama.setText(leaderboard.getUser_name());
        holder.txtVisit.setText(leaderboard.getKunj());
        holder.txtAdmin.setText(leaderboard.getAdmin());
        holder.txtPelatihan.setText(leaderboard.getPel());
        holder.txtEvent.setText(leaderboard.getEvent());
        holder.txtJumlah.setText(leaderboard.getJumlah_kegiatan());
        holder.txtPersen.setText(""+leaderboard.getTotal());
        if (id == 1) {
            setBig(holder);
            holder.imgStar.setVisibility(View.VISIBLE);
            holder.txtNama.setTextColor(context.getResources().getColor(R.color.yellow_900));

        } else if (id == 2) {
            setBig(holder);
            holder.imgStar.setVisibility(View.VISIBLE);
            holder.imgStar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_star2));

        } else if (id == 3) {
            setBig(holder);
            holder.imgStar.setVisibility(View.VISIBLE);
            holder.imgStar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_star3));
        }

    }

    void setBig(final MyViewHolder holder) {
        holder.txtId.setTextAppearance(context, android.R.style.TextAppearance_Material_Medium);
        holder.txtNama.setTextAppearance(context, android.R.style.TextAppearance_Material_Medium);
        holder.txtNama.setTextSize(14);
        holder.txtId.setTextColor(context.getResources().getColor(R.color.black));
        holder.txtNama.setTextColor(context.getResources().getColor(R.color.black));
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    leaderboardListFiltered = leaderboardList;
                } else {
                    List<Leaderboard> filteredList = new ArrayList<>();
                    for (Leaderboard row : leaderboardList) {

                        filteredList.add(row);

                    }

                    leaderboardListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = leaderboardListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                leaderboardListFiltered = (ArrayList<Leaderboard>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemCount() {
        return leaderboardListFiltered.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public interface LeaderboardListListener {
        void onLeaderboardSelected(Leaderboard leaderboard);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtId, txtNama, txtVisit, txtEvent, txtPelatihan, txtAdmin, txtJumlah, txtPersen;
        ImageView imgStar;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtId = (TextView) itemView.findViewById(R.id.txtId);
            this.txtNama = (TextView) itemView.findViewById(R.id.txtNama);
            this.txtVisit = (TextView) itemView.findViewById(R.id.txtVisit);
            this.txtPelatihan = (TextView) itemView.findViewById(R.id.txtPelatihan);
            this.txtEvent = (TextView) itemView.findViewById(R.id.txtEvent);
            this.txtAdmin = (TextView) itemView.findViewById(R.id.txtAdmin);
            this.imgStar = (ImageView) itemView.findViewById(R.id.imgStar);
            this.txtJumlah = (TextView) itemView.findViewById(R.id.txtJumlah);
            this.txtPersen = (TextView) itemView.findViewById(R.id.txtPersen);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onLeaderboardSelected(leaderboardListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }
}
