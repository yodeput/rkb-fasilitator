package id.triwikrama.rkbfasilitator.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.jsibbold.zoomage.ZoomageView;

import java.util.List;

import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.KegiatanPhoto;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class SliderAdapter  extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<KegiatanPhoto> sliderImg;


    public SliderAdapter(Context context, List<KegiatanPhoto> sliderImg) {
        this.sliderImg = sliderImg;
        this.context = context;
    }

    @Override
    public int getCount() {
        return sliderImg.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_view_pager, null);

        String photo = getInstance().urlFixer(sliderImg.get(position).getKegiatan_photo());

        ZoomageView imageView = (ZoomageView) view.findViewById(R.id.myZoomageView);
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_no_image)
                .error(R.drawable.ic_no_image);
        Glide.with(imageView)
                .load(photo)
                .apply(options)
                .into(imageView);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }
}