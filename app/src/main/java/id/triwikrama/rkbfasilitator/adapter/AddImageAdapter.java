package id.triwikrama.rkbfasilitator.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.model.Image;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.AddImage;
import id.triwikrama.rkbfasilitator.model.KegiatanPhoto;

public class AddImageAdapter  extends RecyclerView.Adapter<AddImageAdapter.MyViewHolder> {

    private List<Image> images;
    private Context context;
    private ImageListListener listener;
    private ImageDeleted deleted;

    public AddImageAdapter(Context context, List<Image> image, ImageListListener listener, ImageDeleted deleted) {
        this.context = context;
        this.listener = listener;
        this.deleted = deleted;
        this.images = image;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_image, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        ImageView imageView = holder.imageView;
        Glide.with(imageView)
                .load(images.get(listPosition).getPath())
                .fitCenter()
                .into(imageView);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public interface ImageListListener {
        void onImageListSelected(Image image);
    }

    public interface ImageDeleted{
        void onImageDeleted(Image image);
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        FloatingActionButton fabDelete;
        public MyViewHolder(View itemView)  {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.imgThumb);
            this.fabDelete = (FloatingActionButton) itemView.findViewById(R.id.fabDelete);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   listener.onImageListSelected(images.get(getAdapterPosition()));
                }
            });

            fabDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                  deleted.onImageDeleted(images.get(getAdapterPosition()));
                }
            });
        }
    }
}
