package id.triwikrama.rkbfasilitator.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.Kegiatan;
import id.triwikrama.rkbfasilitator.model.RkbList;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class ListRkbAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROGRESS = 0;

    private int item_per_display = 0;
    private List<RkbList> rkbList;
    private List<RkbList> rkbListFiltered;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener = null;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    rkbListFiltered = rkbList;
                } else {
                    List<RkbList> filteredList = new ArrayList<>();
                    for (RkbList row : rkbList) {
                        if (charString.contains(",")) {
                            String[] separated = charString.split(",");
                            String[] kelas = separated[0].split(":");
                            String[] tr = separated[1].split(":");
                            if ((row.getRkb_kelas().toLowerCase().equals(kelas[1].toLowerCase()))
                                    || (row.getRkb_treg().toLowerCase().equals(tr[1].toLowerCase()))) {
                                filteredList.add(row);
                            }
                        } else {
                            if (charString.contains("kelas:")) {
                                String[] kelas = charString.split(":");
                                if (row.getRkb_kelas().toLowerCase().equals(kelas[1].toLowerCase())){
                                    filteredList.add(row);
                                }
                            } else  if (charString.contains("tr:")) {
                                String[] tr = charString.split(":");
                                if (row.getRkb_kelas().toLowerCase().equals(tr[1].toLowerCase())){
                                    filteredList.add(row);
                                }
                            }else {
                                if ((row.getRkb_name().toLowerCase().contains(charString.toLowerCase())) || (row.getRkb_kelas().toLowerCase().contains(charString.toLowerCase()))
                                        || (row.getRkb_treg().toLowerCase().contains(charString.toLowerCase()))) {
                                    filteredList.add(row);
                                }
                            }
                        }
                    }

                    rkbListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = rkbListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                rkbListFiltered = (ArrayList<RkbList>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface OnItemClickListener {
        void onItemClick(View view, RkbList obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public ListRkbAdapter(Context context, int item_per_display, List<RkbList> items) {
        this.rkbList = items;
        this.rkbListFiltered = items;
        this.item_per_display = item_per_display;
        ctx = context;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public View lyt_parent;
        TextView txtRkbName, txtUser, txtTreg, txtKelas;

        public OriginalViewHolder(View v) {
            super(v);
            this.txtRkbName = (TextView) itemView.findViewById(R.id.txtRkbName);
            this.txtUser = (TextView) itemView.findViewById(R.id.txtUser);
            this.txtTreg = (TextView) itemView.findViewById(R.id.txtTreg);
            this.txtKelas = (TextView) itemView.findViewById(R.id.txtKelas);
            lyt_parent = (View) v.findViewById(R.id.lyt_parent);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progress_bar;

        public ProgressViewHolder(View v) {
            super(v);
            progress_bar = (ProgressBar) v.findViewById(R.id.progress);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rkb_admin, parent, false);
            vh = new OriginalViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final RkbList rkb = rkbListFiltered.get(position);
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;
            ((OriginalViewHolder) holder).txtRkbName.setText("RKB " + rkb.getRkb_name());
            if (rkb.getUser().size() > 0) {
                ((OriginalViewHolder) holder).txtUser.setText(Html.fromHtml("<b>USER:</b> " + rkb.getUser().get(0).getUser_name()));
            } else {
                ((OriginalViewHolder) holder).txtUser.setText(Html.fromHtml("<b>USER:</b> -"));
            }
            ((OriginalViewHolder) holder).txtTreg.setText(Html.fromHtml("<b>TR:</b> " + rkb.getRkb_treg()));
            ((OriginalViewHolder) holder).txtKelas.setText(Html.fromHtml("<b>KELAS:</b> " + rkb.getRkb_kelas()));

            view.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener == null) return;
                    mOnItemClickListener.onItemClick(view, rkbListFiltered.get(position), position);
                }
            });
        } else {
            ((ProgressViewHolder) holder).progress_bar.setIndeterminate(true);
        }

        if (rkb.progress) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        } else {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(false);
        }
    }

    @Override
    public int getItemCount() {
        return rkbListFiltered.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.rkbListFiltered.get(position).progress ? VIEW_PROGRESS : VIEW_ITEM;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        lastItemViewDetector(recyclerView);
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void insertData(List<RkbList> items) {
        if(!this.rkbListFiltered.contains(items)){
            setLoaded();
            int positionStart = getItemCount();
            int itemCount = items.size();
            this.rkbListFiltered.addAll(items);
            notifyItemRangeInserted(positionStart, itemCount);
        }

    }

    public void setLoaded() {
        loading = false;
        for (int i = 0; i < getItemCount(); i++) {
            if (rkbListFiltered.get(i).progress) {
                rkbListFiltered.remove(i);
                notifyItemRemoved(i);
            }
        }
    }

    public void setLoading() {
        if (getItemCount() != 0) {
            this.rkbListFiltered.add(new RkbList(true));
            notifyItemInserted(getItemCount() - 1);
            loading = true;
        }
    }

    public void resetListData() {
        this.rkbList = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    private void lastItemViewDetector(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof StaggeredGridLayoutManager) {
            final StaggeredGridLayoutManager layoutManager = (StaggeredGridLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastPos = getLastVisibleItem(layoutManager.findLastVisibleItemPositions(null));
                    if (!loading && lastPos == getItemCount() - 1 && onLoadMoreListener != null) {
                        int current_page = getItemCount() / item_per_display;
                        onLoadMoreListener.onLoadMore(current_page);
                        loading = true;
                    }
                }
            });
        }
    }

    public interface OnLoadMoreListener {
        void onLoadMore(int current_page);
    }

    private int getLastVisibleItem(int[] into) {
        int last_idx = into[0];
        for (int i : into) {
            if (last_idx < i) last_idx = i;
        }
        return last_idx;
    }
}