package id.triwikrama.rkbfasilitator.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.model.Image;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.model.KegiatanPhoto;
import id.triwikrama.rkbfasilitator.model.Photo;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class ShowImageAdapter extends RecyclerView.Adapter<ShowImageAdapter.MyViewHolder> {


    private List<KegiatanPhoto> images;
    private Context context;
    private ImageListListener listener;

    public ShowImageAdapter(Context context, List<KegiatanPhoto> images, ImageListListener listener) {
        this.context = context;
        this.listener = listener;
        this.images = images;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_image, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        holder.fabDelete.hide();
        ImageView imageView = holder.imageView;
        String img = getInstance().urlFixer(images.get(listPosition).getKegiatan_photo());
        Glide.with(imageView)
                .load(img)
                .fitCenter()
                .into(imageView);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public interface ImageListListener {
        void onImageListSelected(List<KegiatanPhoto> images);
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        FloatingActionButton fabDelete;
        public MyViewHolder(View itemView)  {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.imgThumb);
            this.fabDelete = (FloatingActionButton) itemView.findViewById(R.id.fabDelete);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   listener.onImageListSelected(images);
                }
            });
        }
    }
}
