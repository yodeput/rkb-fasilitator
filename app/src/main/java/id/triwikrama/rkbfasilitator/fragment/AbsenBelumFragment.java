package id.triwikrama.rkbfasilitator.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.facebook.shimmer.ShimmerFrameLayout;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.adapter.ListAbsenAdapter;
import id.triwikrama.rkbfasilitator.adapter.ListAbsenBelumAdapter;
import id.triwikrama.rkbfasilitator.model.Absen;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.param.TanggalParam;
import id.triwikrama.rkbfasilitator.model.response.AbsenResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AbsenBelumFragment extends Fragment {

    private View v;
    private Unbinder unbinder;
    @BindView(R.id.shimmer)
    ShimmerFrameLayout shimmer;
    @BindView(R.id.recylerview)
    RecyclerView recyclerView;
    @BindView(R.id.txtDataKosong)
    TextView txtDataKosong;
    @BindView(R.id.txtTgl)
    TextView txtTgl;
    private TanggalParam param = new TanggalParam();
    private ArrayList<Absen> data = new ArrayList<>();

    private String src;
    private User userData;
    private NetworkService mNetworkService;
    private CustomDialog cd;
    private DatabaseHelper db;
    private final AlertDialog.Builder builder = null;

    private ListAbsenBelumAdapter mAdapter;
    private String stringFilter;
    private int spinner1Pos = 0, spinner2Pos = 0;
    private DetailAbsenFragment detailAbsen=null;
    private int logPage=1;

    private Calendar calendar;
    private int year, month, day;
    public static AbsenSudahFragment newInstance(String text) {
        Bundle args = new Bundle();
        args.putString("msg", text);
        AbsenSudahFragment fragment = new AbsenSudahFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_list_absen, container, false);
        unbinder = ButterKnife.bind(this, v);
        initClass();

        data = getArguments().getParcelableArrayList("data");
        if(data.size()<1){
            shimmer.stopShimmer();
            shimmer.setVisibility(View.GONE);
            txtDataKosong.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            txtDataKosong.setText("Belum ada Data Absen\npada tanggal "+getArguments().getString("tgl"));
        } else {
            generateList(data);
        }

        txtTgl.setText(getArguments().getString("tgl"));
        DateFormat dateNow = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();

        param.setTanggal(txtTgl.getText().toString());
        param.setPage("1");
        param.setStatus("B");
        //reqData(param);
        return v;
    }


    void initClass() {
        cd = new CustomDialog(getActivity());
        db = new DatabaseHelper(getActivity());
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
        userData = db.getUser();

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
    }


    private void reqData(TanggalParam param) {
        logPage=1;
        txtTgl.setText(param.getTanggal());
        shimmer.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        shimmer.startShimmer();
        Call<AbsenResponse> reqData =  mNetworkService.absenList(userData.getUser_token(), param);
        reqData.enqueue(new Callback<AbsenResponse>() {
            @Override
            public void onResponse(Call<AbsenResponse> call, Response<AbsenResponse> response) {
                shimmer.stopShimmer();
                shimmer.setVisibility(View.GONE);
                txtDataKosong.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        generateList(response.body().getData());
                    } else {
                        txtDataKosong.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        txtDataKosong.setText("Belum ada Data Absen\npada tanggal "+stringFilter);
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }

            }

            @Override
            public void onFailure(Call<AbsenResponse> call, Throwable t) {
                shimmer.stopShimmer();
                shimmer.setVisibility(View.GONE);
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }


    private void generateList(List<Absen> absenList) {
        shimmer.stopShimmer();
        shimmer.setVisibility(View.GONE);
        txtDataKosong.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setHasFixedSize(true);
        mAdapter = new ListAbsenBelumAdapter(getActivity(), absenList.size(), absenList);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new ListAbsenBelumAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Absen obj, int position) {
                if(detailAbsen==null) {
                   /* detailAbsen = new DetailAbsenFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("data", obj);
                    detailAbsen.setArguments(bundle);
                    detailAbsen.show(getChildFragmentManager(), detailAbsen.getTag());
                    detailAbsen=null;*/
                }
            }
        });
        if(absenList.size()>7) {
            mAdapter.setOnLoadMoreListener(new ListAbsenBelumAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore(int current_page) {
                    logPage++;
                    nextPage(logPage);
                }
            });
        }

    }


    void nextPage(int current_page){
        if(current_page>1) {
            shimmer.setVisibility(View.GONE);
            mAdapter.setLoading();
            param.setPage(Integer.toString(current_page));
            reqNextData(param);
        }
    }


    private void reqNextData(TanggalParam param) {
        Call<AbsenResponse> reqData =  mNetworkService.absenList(userData.getUser_token(), param);
        reqData.enqueue(new Callback<AbsenResponse>() {
            @Override
            public void onResponse(Call<AbsenResponse> call, Response<AbsenResponse> response) {

                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        mAdapter.insertData(response.body().getData());
                    } else {
                        mAdapter.setLoaded();
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }

            }

            @Override
            public void onFailure(Call<AbsenResponse> call, Throwable t) {
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }


}
