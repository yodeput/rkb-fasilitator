package id.triwikrama.rkbfasilitator.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.adapter.LeaderboardAdapter;
import id.triwikrama.rkbfasilitator.model.Leaderboard;
import id.triwikrama.rkbfasilitator.model.param.ProfileParam;
import id.triwikrama.rkbfasilitator.model.response.LeaderboardResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.Apps;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import id.triwikrama.rkbfasilitator.utils.PrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeaderboardFragmentOld extends Fragment {

    private View v;
    private Unbinder unbinder;
    private CustomDialog cd;
    private DatabaseHelper db;
    private PrefManager pref;
    private LeaderboardAdapter mAdapter;

    @BindView(R.id.txtTanggal)
    TextView txtTanggal;
    @BindView(R.id.motionLayout)
    MotionLayout motionLayout;
    @BindView(R.id.layoutLeaderBoard)
    LinearLayout layoutLeaderBoard;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.imgAvatar)
    CircularImageView imgAvatar;
    @BindView(R.id.txtUserName)
    TextView txtUserName;
    @BindViews({R.id.imgStar1, R.id.imgStar2, R.id.imgStar3, R.id.imgStar4, R.id.imgStar5})
    List<ImageView> imgStars;
    private NetworkService mNetworkService;
    private List<Leaderboard> leaderboardList;

    public static LeaderboardFragmentOld newInstance(String text) {
        Bundle args = new Bundle();
        args.putString("msg", text);
        LeaderboardFragmentOld fragment = new LeaderboardFragmentOld();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_leaderboard, container, false);
        unbinder = ButterKnife.bind(this,v);
        initClass();
        initAvatar();
        reqData();
        return v;
    }

    void initClass() {
        cd = new CustomDialog(getActivity());
        pref = new PrefManager(getActivity());
        db = new DatabaseHelper(getActivity());
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
        txtTanggal.setText(Html.fromHtml(Apps.getInstance().getMonth()));
    }
    void reqData() {
        Call<LeaderboardResponse> leaderboard = mNetworkService.leaderboard_user(db.getUser().getUser_token(), new ProfileParam(db.getUser().getUser_id()));
        leaderboard.enqueue(new Callback<LeaderboardResponse>() {
            @Override
            public void onResponse(Call<LeaderboardResponse> call, Response<LeaderboardResponse> response) {
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        generateData(response.body().getData(), response.body().getRanking_saya());
                    } else {

                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }
            }

            @Override
            public void onFailure(Call<LeaderboardResponse> call, Throwable t) {
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
            }
        });
    }

    void initAvatar(){
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.avatar_leaderboard)
                .error(R.drawable.avatar_leaderboard);
        Glide.with(imgAvatar)
                .load(R.drawable.avatar_leaderboard)
                .centerCrop()
                .apply(options)
                .into(imgAvatar);
    }

    void generateData(List<Leaderboard> leaderboardList, int juara) {
        txtUserName.setText(leaderboardList.get(0).getUser_name());
        mAdapter = new LeaderboardAdapter(getActivity(), leaderboardList, new LeaderboardAdapter.LeaderboardListListener() {
            @Override
            public void onLeaderboardSelected(Leaderboard leaderboard) {

            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

            motionLayout.loadLayoutDescription(R.xml.scene_leaderboard_first);
            motionLayout.setTransition(R.id.start, R.id.end);
            motionLayout.transitionToStart();
            motionLayout.transitionToEnd();
            Handler h = new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    motionLayout.loadLayoutDescription(R.xml.scene_leaderboard_first_star);
                    motionLayout.setTransition(R.id.start, R.id.end);
                    motionLayout.transitionToStart();
                    motionLayout.transitionToEnd();
                    Log.e("wkwkwkwkwk", "" + motionLayout.getCurrentState());
                }
            }, 750);


    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
