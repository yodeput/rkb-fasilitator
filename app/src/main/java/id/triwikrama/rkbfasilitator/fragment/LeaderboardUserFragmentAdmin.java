package id.triwikrama.rkbfasilitator.fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.toptoche.searchablespinnerlibrary.SearchableListDialog;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.adapter.LeaderboardAdapter;
import id.triwikrama.rkbfasilitator.model.JenisKeg;
import id.triwikrama.rkbfasilitator.model.Leaderboard;
import id.triwikrama.rkbfasilitator.model.param.ProfileParam;
import id.triwikrama.rkbfasilitator.model.response.LeaderboardResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.Apps;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import id.triwikrama.rkbfasilitator.utils.PrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;

public class LeaderboardUserFragmentAdmin extends Fragment {


    private View v;
    private Unbinder unbinder;
    private CustomDialog cd;
    private DatabaseHelper db;
    private PrefManager pref;
    private LeaderboardAdapter mAdapter;
    private final AlertDialog.Builder builder = null;
    private ProfileParam param = new ProfileParam();

    protected boolean mIsVisibleToUser;
    private int failedCount=0;
    private int spinner1Pos = 0;

    @BindView(R.id.motionLayout)
    ConstraintLayout motionLayout;
    @BindView(R.id.lytProgress)
    CardView lytProgress;
    @BindView(R.id.txtTanggal)
    TextView txtTanggal;
    @BindView(R.id.txtKelas)
    TextView txtKelas;
    @BindViews({R.id.layoutLeaderBoard,R.id.layoutContainer, R.id.layoutPresentase})
    List<LinearLayout> ll_layouts;
    @BindView(R.id.layoutLabel)
    ConstraintLayout layoutLabel;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.imgAvatar)
    CircularImageView imgAvatar;
    @BindView(R.id.txtUserName)
    TextView txtUserName;
    @BindView(R.id.llKelas)
    LinearLayout llKelas;
    @BindViews({R.id.imgStar1, R.id.imgStar2, R.id.imgStar3, R.id.imgStar4, R.id.imgStar5})
    List<ImageView> imgStars;
    private NetworkService mNetworkService;
    private List<Leaderboard> leaderboardList;
    private String bulan, tahun;

    @OnClick(R.id.txtTanggal) void picker(){
        showPicker();
    }

    @OnClick(R.id.llKelas) void pilihKelas(){
        dialog_filter();
    }

    public static LeaderboardUserFragmentAdmin newInstance(String text) {
        Bundle args = new Bundle();
        args.putString("msg", text);
        LeaderboardUserFragmentAdmin fragment = new LeaderboardUserFragmentAdmin();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.activity_leaderboard_admin, container, false);
        unbinder = ButterKnife.bind(this,v);
        initClass();
        initAvatar();

        return v;
    }

    void initClass() {
        cd = new CustomDialog(getActivity());
        pref = new PrefManager(getActivity());
        db = new DatabaseHelper(getActivity());
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
        txtTanggal.setText(Html.fromHtml(Apps.getInstance().getMonth()));
    }

    private void showPicker() {
        Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(getActivity(),
                new MonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int selectedMonth, int selectedYear) {
                        bulan = Integer.toString(selectedMonth+1);
                        if(bulan.length()==1) {
                            bulan = "0" + bulan;
                        }
                        tahun = Integer.toString(selectedYear);
                        createPram();
                        txtTanggal.setText(Html.fromHtml(Apps.getInstance().getMonthLeaderboard(selectedMonth+1, selectedYear)));
                        failedCount=0;
                    }
                },today.get(Calendar.YEAR),Integer.parseInt(bulan)-1);
        builder.setMinYear(2019)
                .setActivatedYear(Integer.parseInt(tahun))
                .setMaxYear(2030)
                .build()
                .show();
    }

    void createPram(){
        param.setUser_id(db.getUser().getUser_id());
        param.setBulan(bulan);
        param.setTahun(tahun);
        param.setKelas("A");
        txtKelas.setText("A");
        reqData(param);
    }

    void reqData(ProfileParam param) {
        viewGone();
        txtKelas.setText(param.getKelas());
        Call<LeaderboardResponse> leaderboard = mNetworkService.leaderboard_user(db.getUser().getUser_token(),param);
        leaderboard.enqueue(new Callback<LeaderboardResponse>() {
            @Override
            public void onResponse(Call<LeaderboardResponse> call, Response<LeaderboardResponse> response) {
                int code = response.code();
                if (code == 200) {
                    int success = response.body().getSuccess();
                    if (success == 1) {
                        generateData(response.body().getData(), response.body().getRanking_saya());
                        viewVisible();
                    } else {
                        viewGone();
                        cd.oneButtonDialog("Data tidak ditemukan pada "+param.getBulan()+"/"+param.getTahun()+"\n",R.drawable.ic_close, R.color.red_600);
                        param.setUser_id(db.getUser().getUser_id());
                        DateFormat yearFormat = new SimpleDateFormat("yyyy");
                        DateFormat monthFormat = new SimpleDateFormat("MM");
                        Date date = new Date();

                        tahun = yearFormat.format(date);
                        bulan = monthFormat.format(date);
                        param.setBulan(bulan);
                        param.setTahun(tahun);
                        txtTanggal.setText(Html.fromHtml(Apps.getInstance().getMonth()));
                        if(failedCount<1) {
                            reqData(param);
                        }
                        failedCount++;
                    }
                } else {
                    cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }
            }

            @Override
            public void onFailure(Call<LeaderboardResponse> call, Throwable t) {
                Log.e("reqData", t.getMessage());
                cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
            }
        });
    }

    void initAvatar(){
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.avatar_leaderboard)
                .error(R.drawable.avatar_leaderboard);
        Glide.with(imgAvatar)
                .load(R.drawable.avatar_leaderboard)
                .centerCrop()
                .apply(options)
                .into(imgAvatar);
    }

    void generateData(List<Leaderboard> leaderboardList, int juara) {
        txtUserName.setText(leaderboardList.get(0).getUser_name());
        mAdapter = new LeaderboardAdapter(getActivity(), leaderboardList, new LeaderboardAdapter.LeaderboardListListener() {
            @Override
            public void onLeaderboardSelected(Leaderboard leaderboard) {

            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        juara1Star();
       /* if (juara == 1) {
            juara1Star();
        } else {
            juara2Star();
        }*/

        String urlFoto = getInstance().urlFixer(leaderboardList.get(0).getUser_photo());
        RequestOptions options;
        options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.avatar_man)
                .error(R.drawable.avatar_man);
        Glide.with(imgAvatar)
                .load(urlFoto)
                .centerCrop()
                .apply(options)
                .into(imgAvatar);
    }

    void juara1Star()
    {
        for(int i=0;i<imgStars.size();i++){
            imgStars.get(i).setVisibility(View.VISIBLE);
        }
        imgStars.get(0).setImageDrawable(getResources().getDrawable(R.drawable.ic_star1));
    }

    void juara2Star()
    {
        for(int i=1;i<imgStars.size();i++){
            imgStars.get(i).setVisibility(View.GONE);
        }
        imgStars.get(0).setImageDrawable(getResources().getDrawable(R.drawable.ic_star2));
    }

    void viewGone()
    {
        motionLayout.setVisibility(View.GONE);
        lytProgress.setVisibility(View.VISIBLE);
    }
    void viewVisible()
    {
        motionLayout.setVisibility(View.VISIBLE);
        lytProgress.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
    @Override
    public void onStart() {
        super.onStart();
        if (mIsVisibleToUser) {
            onVisible();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mIsVisibleToUser) {
            onInVisible();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        mIsVisibleToUser = isVisibleToUser;
        if (isResumed()) { // fragment have created
            if (mIsVisibleToUser) {
                onVisible();
            } else {
                onInVisible();
            }
        }
    }

    public void onVisible() {
       load();
    }

    public void onInVisible() {
        //cd.toastSuccess("Board USER hide");
    }

    private void load(){
        DateFormat yearFormat = new SimpleDateFormat("yyyy");
        DateFormat monthFormat = new SimpleDateFormat("MM");
        Date date = new Date();

        tahun = yearFormat.format(date);
        bulan = monthFormat.format(date);
        txtTanggal.setText(Html.fromHtml(Apps.getInstance().getMonth()));
        createPram();
    }

    public void dialog_filter() {
        List<String> kelas = new ArrayList<>();
        kelas.add("A");
        kelas.add("B");
        kelas.add("C");
        SearchableListDialog list = SearchableListDialog.newInstance(kelas);
        list.setOnSearchableItemClickListener(new SearchableListDialog.SearchableItem() {
            @Override
            public void onSearchableItemClicked(Object item, int position) {
                param.setKelas(item.toString());
                reqData(param);
            }
        });
        list.setTitle("Pilih Kelas");
        list.show(getActivity().getFragmentManager(), "TAG");
    }
}
