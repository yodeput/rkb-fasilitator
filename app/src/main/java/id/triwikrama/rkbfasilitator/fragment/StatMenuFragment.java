package id.triwikrama.rkbfasilitator.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.activity.admin.stat.JenisKegiatanActivity;
import id.triwikrama.rkbfasilitator.activity.admin.stat.KelasRKBActivity;
import id.triwikrama.rkbfasilitator.activity.admin.stat.PerBulanActivity;
import id.triwikrama.rkbfasilitator.activity.admin.stat.PerTregActivity;

public class StatMenuFragment extends Fragment {

    private View v;
    private Unbinder unbinder;

    @BindView(R.id.butKegiatan)CardView butKegiatan;

    @OnClick({R.id.butKegiatan, R.id.butRkb, R.id.butPerbulan, R.id.butTreg})
    public void setViewOnClickEvent(View view) {
        switch (view.getId()) {
            case R.id.butKegiatan:
                startActivity(new Intent(getActivity(), JenisKegiatanActivity.class));
                getActivity().overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
                break;
            case R.id.butRkb:
                startActivity(new Intent(getActivity(), KelasRKBActivity.class));
                getActivity().overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
                break;
            case R.id.butPerbulan:
                startActivity(new Intent(getActivity(), PerBulanActivity.class));
                getActivity().overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
                break;
            case R.id.butTreg:
                startActivity(new Intent(getActivity(), PerTregActivity.class));
                getActivity().overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
                break;
        }
    }


    public static StatMenuFragment newInstance(String text) {
        Bundle args = new Bundle();
        args.putString("msg", text);
        StatMenuFragment fragment = new StatMenuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_menu_stat, container, false);
        unbinder = ButterKnife.bind(this,v);

        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
