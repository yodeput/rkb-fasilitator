package id.triwikrama.rkbfasilitator.fragment;

import android.content.DialogInterface;

public interface MyDialogCloseListener {
    public void handleDialogClose(DialogInterface dialog);
}