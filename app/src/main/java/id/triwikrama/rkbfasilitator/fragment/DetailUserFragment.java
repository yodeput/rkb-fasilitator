package id.triwikrama.rkbfasilitator.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.activity.admin.AddUserActivity;
import id.triwikrama.rkbfasilitator.model.RkbList;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.UserList;
import id.triwikrama.rkbfasilitator.model.response.GlobalResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailUserFragment extends BottomSheetDialogFragment{

    private static RecyclerView.Adapter adapter;
    @BindView(R.id.topLayout)
    RelativeLayout topLayout;
    TextView txtTitle;
    @BindView(R.id.txtNama)
    TextView txtNama;
    @BindView(R.id.txtEmail)
    TextView txtEmail;
    @BindView(R.id.txtRole)
    TextView txtRole;
    @BindView(R.id.txtAlamat)
    TextView txtAlamat;
    @BindView(R.id.txtTelepon)
    TextView txtTelepon;
    @BindView(R.id.txtTtl)
    TextView txtTtl;
    @BindView(R.id.mainLayout)RelativeLayout mainLayout;

    private Unbinder unbind;
    private View v;

    private Dialog dialog;
    private RkbList rkbList;
    private ArrayList<String> kelas =  new ArrayList<>();
    private ArrayList<String> tr =  new ArrayList<>();
    private String src;
    private UserList userData;
    private NetworkService mNetworkService;
    private CustomDialog cd;
    private DatabaseHelper db;
    private MyDialogCloseListener closeListener;

    public DetailUserFragment(){

    }

    @OnClick(R.id.butClose) void close(){
        dismiss();
    }

    @OnClick(R.id.but_edit) void edit(){
        startActivity(new Intent(getActivity(), AddUserActivity.class)
                .putExtra("data",userData)
                .putExtra("src","Edit User"));
       dismiss();
    }

    @OnClick(R.id.but_delete) void delete(){

        User u = new User();
        u.setUser_id(userData.getUser_id());
        dialogDelete(u);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;

                FrameLayout bottomSheet = (FrameLayout) d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        dialog.setCanceledOnTouchOutside(true);

        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_detail_user, container, false);
        unbind = ButterKnife.bind(this, v);
        initClass();
        src = getArguments().getString("src");
        mainLayout.setVisibility(View.VISIBLE);
        userData = (UserList) getArguments().getSerializable("data");
        //txtTitle.setText(rkbList.getRkb_name());
        txtNama.setText(userData.getUser_name());
        txtRole.setText(userData.getUser_type());
        txtAlamat.setText(userData.getUser_alamat());
        txtEmail.setText(userData.getUser_email());
        txtTelepon.setText(userData.getUser_notelp());
        txtTtl.setText(userData.getUser_tempatlahir()+", "+userData.getUser_tanggallahir());

        return v;
    }

    void initClass() {
        cd = new CustomDialog(getActivity());
        db = new DatabaseHelper(getActivity());
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.commitAllowingStateLoss();
    }


    final String GetString(int string) {
        return getActivity().getString(string);
    }

    public void dialogDelete(User user){
        if(dialog==null||!dialog.isShowing()) {
            dialog = new Dialog(getActivity(), R.style.DialogSlideAnim);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_two_button);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER_HORIZONTAL;
            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setAttributes(lp);


            dialog.setCanceledOnTouchOutside(false);
            dialog.setOnKeyListener(new Dialog.OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface arg0, int keyCode,
                                     KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                    }
                    return true;
                }
            });

            ((ImageView) dialog.findViewById(R.id.img_dialog)).setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
            ((TextView) dialog.findViewById(R.id.txt_message)).setText(GetString(R.string.textHapus));
            ((Button) dialog.findViewById(R.id.bt_positive)).setText(GetString(R.string.but_ya));
            ((Button) dialog.findViewById(R.id.bt_positive)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    deleteUser(user);
                }
            });
            ((Button) dialog.findViewById(R.id.bt_negative)).setText(GetString(R.string.but_nlogout));
            ((Button) dialog.findViewById(R.id.bt_negative)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }
    }

    private void deleteUser(User user){
        Call<GlobalResponse> reqData = mNetworkService.deleteUser(db.getUser().getUser_token(), user);

        reqData.enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {

                int code = response.code();
                if (code == 200) {
                    if (response.body().getSuccess()==1) {
                        cd.toastSuccess(response.body().getMessage());
                        dismiss();
                    } else {
                        cd.oneButtonDialog(response.body().getMessage(), R.drawable.ic_user, R.color.red_700);
                    }
                } else {
                    cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }

            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if(closeListener != null) {
            closeListener.handleDialogClose(null);
        }
    }

    public void DismissListener(MyDialogCloseListener closeListener) {
        this.closeListener = closeListener;
    }
}
