package id.triwikrama.rkbfasilitator.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.activity.admin.AddRkbActivity;
import id.triwikrama.rkbfasilitator.model.RkbList;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.Username;
import id.triwikrama.rkbfasilitator.model.param.AddRkb;
import id.triwikrama.rkbfasilitator.model.response.GlobalResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailRkbFragment extends BottomSheetDialogFragment{

    private static RecyclerView.Adapter adapter;
    @BindView(R.id.topLayout)
    RelativeLayout topLayout;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtNama)
    TextView txtNama;
    @BindView(R.id.txtKelas)
    TextView txtKelas;
    @BindView(R.id.txtTr)
    TextView txtTr;
    @BindView(R.id.txtGmv)
    TextView txtGmv;
    @BindView(R.id.mainLayout)RelativeLayout mainLayout;
    @BindView(R.id.llContainer)
    LinearLayout llContainer;

    private Unbinder unbind;
    private View v;

    private Dialog dialog;
    private RkbList rkbList;
    private ArrayList<String> kelas =  new ArrayList<>();
    private ArrayList<String> tr =  new ArrayList<>();
    private String src;
    private User userData;
    private NetworkService mNetworkService;
    private CustomDialog cd;
    private DatabaseHelper db;
    private MyDialogCloseListener closeListener;

    public DetailRkbFragment(){

    }

    @OnClick(R.id.butClose) void close(){
        dismiss();
    }

    @OnClick(R.id.but_edit) void edit(){
        startActivity(new Intent(getActivity(), AddRkbActivity.class)
                .putExtra("data",rkbList)
                .putExtra("src","Edit RKB")
                .putStringArrayListExtra("kelas", kelas)
                .putStringArrayListExtra("tr", tr));
       dismiss();
    }

    @OnClick(R.id.but_delete) void delete(){

        AddRkb param = new AddRkb();
        param.setRkb_id(rkbList.getRkb_id());
        dialogDelete(param);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;

                FrameLayout bottomSheet = (FrameLayout) d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        dialog.setCanceledOnTouchOutside(true);

        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_detail_rkb, container, false);
        unbind = ButterKnife.bind(this, v);
        initClass();
        src = getArguments().getString("src");
        mainLayout.setVisibility(View.VISIBLE);
        rkbList = (RkbList) getArguments().getSerializable("data");
        //txtTitle.setText(rkbList.getRkb_name());
        txtNama.setText(rkbList.getRkb_name());
        txtKelas.setText(rkbList.getRkb_kelas());
        txtTr.setText(rkbList.getRkb_treg());
        kelas = getArguments().getStringArrayList("kelas");
        tr = getArguments().getStringArrayList("tr");
        txtGmv.setText(rkbList.getRkb_gmv());

        List<Username> users = rkbList.getUser();
        for( int i = 0; i < users.size(); i++ )
        {
            TextView textView = new TextView(getActivity());
            textView.setText(users.get(i).getUser_name());
            textView.setTextColor(getResources().getColor(R.color.black));
            textView.setTypeface(textView.getTypeface(), Typeface.BOLD);
            llContainer.addView(textView);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)textView.getLayoutParams();
            params.setMargins(20, 0, 0, 0);
            textView.setLayoutParams(params);
        }

        return v;
    }

    void initClass() {
        cd = new CustomDialog(getActivity());
        db = new DatabaseHelper(getActivity());
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
        userData = db.getUser();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.commitAllowingStateLoss();
    }
    public void dialogDelete(AddRkb param) {
        if(dialog==null||!dialog.isShowing()) {
            dialog = new Dialog(getActivity(), R.style.DialogSlideAnim);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_two_button);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER_HORIZONTAL;
            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setAttributes(lp);


            dialog.setCanceledOnTouchOutside(false);
            dialog.setOnKeyListener(new Dialog.OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface arg0, int keyCode,
                                     KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                    }
                    return true;
                }
            });

            ((ImageView) dialog.findViewById(R.id.img_dialog)).setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
            ((TextView) dialog.findViewById(R.id.txt_message)).setText(GetString(R.string.textHapus));
            ((Button) dialog.findViewById(R.id.bt_positive)).setText(GetString(R.string.but_ya));
            ((Button) dialog.findViewById(R.id.bt_positive)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    deleteRkb(param);
                }
            });
            ((Button) dialog.findViewById(R.id.bt_negative)).setText(GetString(R.string.but_nlogout));
            ((Button) dialog.findViewById(R.id.bt_negative)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }
    }

    final String GetString(int string) {
        return getActivity().getString(string);
    }


    private void deleteRkb(AddRkb param){
        cd.show_p_Dialog();
        Call<GlobalResponse> delRkb = mNetworkService.deleteRkb(db.getUser().getUser_token(), param);
        delRkb.enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                cd.hide_p_Dialog();
                int code = response.code();
                if (code == 200) {
                    if (response.body().getSuccess()==1) {
                        cd.oneButtonDialog(response.body().getMessage(), R.drawable.ic_user, R.color.green_700);
                        DetailRkbFragment.this.dismiss();
                    } else {
                        cd.oneButtonDialog_finish(response.body().getMessage(), R.drawable.ic_user, R.color.red_700);
                    }
                } else {
                    cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

                }
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                cd.hide_p_Dialog();
                cd.oneButtonDialog_finish(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);

            }
        });
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if(closeListener != null) {
            closeListener.handleDialogClose(null);
        }
    }

    public void DismissListener(MyDialogCloseListener closeListener) {
        this.closeListener = closeListener;
    }
}
