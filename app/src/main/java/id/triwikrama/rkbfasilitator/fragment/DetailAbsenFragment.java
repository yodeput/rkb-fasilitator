package id.triwikrama.rkbfasilitator.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.esafirm.imagepicker.model.Image;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.adapter.KegiatanAdapter;
import id.triwikrama.rkbfasilitator.adapter.ShowImageAdapter;
import id.triwikrama.rkbfasilitator.model.Absen;
import id.triwikrama.rkbfasilitator.model.Kegiatan;
import id.triwikrama.rkbfasilitator.model.KegiatanDetail;
import id.triwikrama.rkbfasilitator.model.KegiatanPhoto;
import id.triwikrama.rkbfasilitator.model.Photo;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.param.DetailParam;
import id.triwikrama.rkbfasilitator.model.response.DetailResponse;
import id.triwikrama.rkbfasilitator.model.response.GlobalResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailAbsenFragment extends BottomSheetDialogFragment{

    private static RecyclerView.Adapter adapter;
    @BindView(R.id.topLayout)
    RelativeLayout topLayout;
    @BindView(R.id.recyclerView)RecyclerView recyclerView;
    @BindView(R.id.txtNama)
    TextView txtNama;
    @BindView(R.id.txtRkb)
    TextView txtRkb;
    @BindView(R.id.txtNoImage)
    TextView txtNoImage;
    @BindView(R.id.imgNoImages)
    ImageView imgNoImages;
    @BindView(R.id.mainLayout)LinearLayout mainLayout;

    private Unbinder unbind;
    private View v;
    private GoogleMap googleMap;
    private MapView mapView;
    private Dialog dialog;
    private GoogleApiClient mGoogleApiClient;
    private int PLACE_PICKER_REQUEST = 0;
    private LatLng latlong = new LatLng(-6.1753924, 106.827152);
    private String src, address, latitude, longitude;
    private List<Place.Field> fields;
    private List<Image> images = new ArrayList<>();
    private Absen absenData;
    private RecyclerView.LayoutManager layoutManager;
    private User userData;
    private NetworkService mNetworkService;
    private CustomDialog cd;
    private DatabaseHelper db;
    private KegiatanAdapter mAdapter;

    public DetailAbsenFragment(){

    }

    @OnClick(R.id.butClose) void close(){
        dismiss();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;

                FrameLayout bottomSheet = (FrameLayout) d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        dialog.setCanceledOnTouchOutside(true);

        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_detail_absen, container, false);
        unbind = ButterKnife.bind(this, v);
        initClass();
        src = getArguments().getString("src");
        absenData = (Absen) getArguments().getSerializable("data");

        mapView = (MapView) v.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        showDetail(absenData);

        return v;
    }

    void initClass() {
        cd = new CustomDialog(getActivity());
        db = new DatabaseHelper(getActivity());
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
        userData = db.getUser();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbind.unbind();
    }
    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private void showDetail(Absen absenData){
        mainLayout.setVisibility(View.VISIBLE);
        txtNama.setText(absenData.getUser_name());
        txtRkb.setText(absenData.getRkb_name());
        if(!absenData.getAbsen_lat().isEmpty()&&!absenData.getAbsen_lng().isEmpty()){
            latlong = new LatLng(Double.valueOf(absenData.getAbsen_lat()),Double.valueOf(absenData.getAbsen_lng()));
        }
         mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                googleMap.addMarker(new MarkerOptions().position(latlong).title(absenData.getAbsen_date()).snippet(absenData.getUser_name()));
                CameraPosition cameraPosition = new CameraPosition.Builder().target(latlong).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                googleMap.getUiSettings().setScrollGesturesEnabled(false);
            }
        });
         List<KegiatanPhoto> photoList = new ArrayList<>();
         photoList.add(new KegiatanPhoto(absenData.getAbsen_photo()));
        if(photoList.size()>0) {
            layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            ShowImageAdapter.ImageListListener l = new ShowImageAdapter.ImageListListener() {
                @Override
                public void onImageListSelected(List<KegiatanPhoto> kegiatanPhotos) {

                    FragmentManager fragmentManager = getFragmentManager();
                    ViewImageFragment newFragment = new ViewImageFragment();
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Bundle bundle = new Bundle();
                    //bundle.putString("url", image.getKegiatan_photo());
                    bundle.putString("nama", absenData.getUser_name());
                    bundle.putSerializable("data",(Serializable) kegiatanPhotos);
                    newFragment.setArguments(bundle);
                    newFragment.show(ft, newFragment.getTag());
                }
            };
            adapter = new ShowImageAdapter(getActivity(), photoList, l);
            recyclerView.setAdapter(adapter);
            txtNoImage.setVisibility(View.GONE);
            imgNoImages.setVisibility(View.GONE);

        } else {
            txtNoImage.setVisibility(View.VISIBLE);
            imgNoImages.setVisibility(View.VISIBLE);
        }
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
