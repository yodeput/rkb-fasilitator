package id.triwikrama.rkbfasilitator.fragment;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.adapter.LeaderboardAdapter;
import id.triwikrama.rkbfasilitator.adapter.ViewPagerAdapter;
import id.triwikrama.rkbfasilitator.utils.Apps;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import id.triwikrama.rkbfasilitator.utils.PrefManager;

public class LeaderboardFragmentAdmin extends Fragment {

    protected boolean mIsVisibleToUser;
    private View v;
    private Unbinder unbinder;
    private CustomDialog cd;
    private DatabaseHelper db;
    private PrefManager pref;
    private LeaderboardAdapter mAdapter;

    private TabLayout tabs;
    private ViewPager viewPager;


    public static LeaderboardFragmentAdmin newInstance(String text) {
        Bundle args = new Bundle();
        args.putString("msg", text);
        LeaderboardFragmentAdmin fragment = new LeaderboardFragmentAdmin();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_leaderboard_admin, container, false);
        unbinder = ButterKnife.bind(this,v);
        initClass();

        viewPager = (ViewPager) v.findViewById(R.id.viewPager);
        tabs = (TabLayout) v.findViewById(R.id.tabs);
        //initViewPager();
        return v;
    }

    void initClass() {
        cd = new CustomDialog(getActivity());
        pref = new PrefManager(getActivity());
        db = new DatabaseHelper(getActivity());

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    void initViewPager(){
        tabs.setupWithViewPager(viewPager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(this.getChildFragmentManager());
        adapter.addFragment(new LeaderboardUserFragmentAdmin(), "User");
        adapter.addFragment(new LeaderboardRkbFragmentAdmin(), "RKB");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mIsVisibleToUser) {
            onVisible();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mIsVisibleToUser) {
            onInVisible();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        mIsVisibleToUser = isVisibleToUser;
        if (isResumed()) { // fragment have created
            if (mIsVisibleToUser) {
                onVisible();
            } else {
                onInVisible();
            }
        }
    }

    public void onVisible() {
        initViewPager();
    }

    public void onInVisible() {
        //cd.toastSuccess("Board USER hide");
    }


}
