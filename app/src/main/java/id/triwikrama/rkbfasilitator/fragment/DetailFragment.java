package id.triwikrama.rkbfasilitator.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.esafirm.imagepicker.model.Image;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.toptoche.searchablespinnerlibrary.SearchableListDialog;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.triwikrama.rkbfasilitator.activity.admin.ListKegiatanActivityAdmin;
import id.triwikrama.rkbfasilitator.model.JenisKeg;
import id.triwikrama.rkbfasilitator.model.response.GlobalResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkService;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.adapter.KegiatanAdapter;
import id.triwikrama.rkbfasilitator.adapter.ShowImageAdapter;
import id.triwikrama.rkbfasilitator.model.KegiatanDetail;
import id.triwikrama.rkbfasilitator.model.Kegiatan;
import id.triwikrama.rkbfasilitator.model.KegiatanPhoto;
import id.triwikrama.rkbfasilitator.model.User;
import id.triwikrama.rkbfasilitator.model.param.DetailParam;
import id.triwikrama.rkbfasilitator.model.response.DetailResponse;
import id.triwikrama.rkbfasilitator.networking.NetworkModule;
import id.triwikrama.rkbfasilitator.utils.Apps;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;
import id.triwikrama.rkbfasilitator.utils.DatabaseHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailFragment extends BottomSheetDialogFragment{

    private static RecyclerView.Adapter adapter;
    @BindView(R.id.topLayout)
    RelativeLayout topLayout;
    @BindView(R.id.recyclerView)RecyclerView recyclerView;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtNama)
    TextView txtNama;
    @BindView(R.id.txtNamaLabel)
    TextView txtNamaLabel;
    @BindView(R.id.txtDeskripsi)
    TextView txtDeskripsi;
    @BindView(R.id.txtInstansi)
    TextView txtInstansi;
    @BindView(R.id.txtJumlah)
    TextView txtJumlah;
    @BindView(R.id.txtLokasi)
    TextView txtLokasi;
    @BindView(R.id.txtNoImage)
    TextView txtNoImage;
    @BindView(R.id.imgNoImages)
    ImageView imgNoImages;
    @BindView(R.id.ll_instansi)
    LinearLayout ll_instansi;
    @BindView(R.id.ll_jumlah)
    LinearLayout ll_jumlah;
    @BindView(R.id.shimmer)
    ShimmerFrameLayout shimmer;
    @BindView(R.id.mainLayout)LinearLayout mainLayout;
    @BindView(R.id.butDelete)
    ImageView butDelete;
    @BindView(R.id.llJenis)
    RelativeLayout llJenis;
    @BindView(R.id.txtJenisKegiatan)
    TextView txtJenisKegiatan;

    private Unbinder unbind;
    private View v;
    private GoogleMap googleMap;
    private MapView mapView;
    private Dialog dialog;
    private GoogleApiClient mGoogleApiClient;
    private int PLACE_PICKER_REQUEST = 0;
    private LatLng latlong = new LatLng(-6.1753924, 106.827152);
    private String src, address, latitude, longitude;
    private List<Place.Field> fields;
    private List<Image> images = new ArrayList<>();
    private Kegiatan kegiatan;
    private RecyclerView.LayoutManager layoutManager;
    private User userData;
    private NetworkService mNetworkService;
    private CustomDialog cd;
    private DatabaseHelper db;
    private KegiatanAdapter mAdapter;
    private int spinner1Pos = 0;
    private final AlertDialog.Builder builder = null;
    private DetailParam param = new DetailParam();
    private MyDialogCloseListener closeListener;

    public DetailFragment(){

    }

    @OnClick(R.id.butClose) void close(){
        dismiss();
    }

    @OnClick(R.id.butDelete) void delete(){
        dialogDelete(kegiatan);
    }

    @OnClick(R.id.fabEdit) void edit(){
        List<String> jenis = new ArrayList<>();
        jenis.add("Kunjungan");
        jenis.add("Pelatihan");
        jenis.add("Event");
        jenis.add("Administrasi");
        SearchableListDialog list = SearchableListDialog.newInstance(jenis);
        list.setOnSearchableItemClickListener(new SearchableListDialog.SearchableItem() {
            @Override
            public void onSearchableItemClicked(Object item, int position) {
              param.setType(Integer.toString(position+1));
              editKegiatan(param);
            }
        });
        list.setTitle("Pilih Jenis Kegiatan");
        list.show(getActivity().getFragmentManager(), "TAG");
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;

                FrameLayout bottomSheet = (FrameLayout) d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        dialog.setCanceledOnTouchOutside(true);

        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_detail, container, false);
        unbind = ButterKnife.bind(this, v);
        initClass();
        src = getArguments().getString("src");
        if(src.equals("Event")){
            ll_instansi.setVisibility(View.VISIBLE);
            ll_jumlah.setVisibility(View.VISIBLE);
        }   else if(src.equals("Pelatihan")){
            ll_jumlah.setVisibility(View.VISIBLE);
        }  else if(src.equals("admin")){
           butDelete.setVisibility(View.VISIBLE);
           llJenis.setVisibility(View.VISIBLE);
        }
        kegiatan = (Kegiatan) getArguments().getSerializable("data");
        reqData(kegiatan.getKegiatan_id());
        param.setKegiatan_id(kegiatan.getKegiatan_id());
        mapView = (MapView) v.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }



        return v;
    }

    void initClass() {
        cd = new CustomDialog(getActivity());
        db = new DatabaseHelper(getActivity());
        mNetworkService = NetworkModule.getClient().create(NetworkService.class);
        userData = db.getUser();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if(closeListener != null) {
            closeListener.handleDialogClose(null);
        }
    }

    public void DismissListener(MyDialogCloseListener closeListener) {
        this.closeListener = closeListener;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbind.unbind();
    }
    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private void reqData(String id){
        shimmer.startShimmer();
        mainLayout.setVisibility(View.GONE);
        Log.e("reqData","true");
        String token = userData.getUser_token();
        Call<DetailResponse> reqData = mNetworkService.detailKegiatan(token,new DetailParam(id));
        reqData.enqueue(new Callback<DetailResponse>() {
            @Override
            public void onResponse(Call<DetailResponse> call, Response<DetailResponse> response) {
                shimmer.stopShimmer();
                int code = response.code();
                if (code == 200) {
                    if (response.body().getSuccess() == 1){
                        showDetail(response.body().getDetailList().get(0));
                    } else {
                        cd.oneButtonDialog(response.body().getMessage(),R.drawable.ic_user,R.color.merah);
                    }
                } else {
                    cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }

            }
            @Override
            public void onFailure(Call<DetailResponse> call, Throwable t) {
                cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
            }
        });
    }

    private void editKegiatan(DetailParam param){
        shimmer.startShimmer();
        mainLayout.setVisibility(View.GONE);
        Log.e("reqData","true");
        String token = userData.getUser_token();
        Call<GlobalResponse> reqData = mNetworkService.editKegiatan(token,param);
        reqData.enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                shimmer.stopShimmer();
                int code = response.code();
                if (code == 200) {
                    if (response.body().getSuccess() == 1){
                        //showDetail(response.body().getDetailList().get(0));
                        reqData(param.getKegiatan_id());
                    } else {
                        cd.oneButtonDialog(response.body().getMessage(),R.drawable.ic_user,R.color.merah);
                    }
                } else {
                    cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
                }

            }
            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                cd.oneButtonDialog(getString(R.string.error_koneksi), R.drawable.ic_cloud_off, R.color.merah);
            }
        });
    }

    final String GetString(int string) {
        return getActivity().getString(string);
    }

    public void dialogDelete(Kegiatan kegiatan){
        if(dialog==null||!dialog.isShowing()) {
            dialog = new Dialog(getActivity(), R.style.DialogSlideAnim);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_two_button);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER_HORIZONTAL;
            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setAttributes(lp);


            dialog.setCanceledOnTouchOutside(false);
            dialog.setOnKeyListener(new Dialog.OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface arg0, int keyCode,
                                     KeyEvent event) {
                    // TODO Auto-generated method stub
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                    }
                    return true;
                }
            });

            ((ImageView) dialog.findViewById(R.id.img_dialog)).setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
            ((TextView) dialog.findViewById(R.id.txt_message)).setText(GetString(R.string.textHapus));
            ((Button) dialog.findViewById(R.id.bt_positive)).setText(GetString(R.string.but_ya));
            ((Button) dialog.findViewById(R.id.bt_positive)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    deleteData(kegiatan);
                }
            });
            ((Button) dialog.findViewById(R.id.bt_negative)).setText(GetString(R.string.but_nlogout));
            ((Button) dialog.findViewById(R.id.bt_negative)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }
    }

    private void deleteData(Kegiatan Kegiatan){
        shimmer.startShimmer();
        mainLayout.setVisibility(View.GONE);
        Log.e("deleteData","true");
        String token = userData.getUser_token();
        Call<GlobalResponse> reqData = mNetworkService.deleteKegiatan(token,new DetailParam(kegiatan.getKegiatan_id()));
        reqData.enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                shimmer.stopShimmer();

                if (response.body().getSuccess() == 1){
                    cd.toastSuccess(response.body().getMessage());
                    dismiss();
                } else {
                    cd.oneButtonDialog(response.body().getMessage(),R.drawable.ic_user,R.color.merah);
                }

            }
            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                cd.oneButtonDialog("Koneksi bermasalah",R.drawable.ic_seru,R.color.merah);
            }
        });
    }

    private void showDetail(KegiatanDetail detailKegiatan){
        mainLayout.setVisibility(View.VISIBLE);
        if(db.getUser().getUser_type().equals("1")){
            txtNamaLabel.setText("Nama Kegiatan");
        } else if(db.getUser().getUser_type().equals("2")){
            txtNamaLabel.setText("Nama "+src);
        }
        txtTitle.setText(detailKegiatan.getKegiatan_date());
        txtNama.setText(detailKegiatan.getKegiatan_name());
        txtDeskripsi.setText(detailKegiatan.getKegiatan_desc());
        txtLokasi.setText(detailKegiatan.getKegiatan_lokasi());
        txtJenisKegiatan.setText(detailKegiatan.getKegiatan_type());

        if(detailKegiatan.getKegiatan_type().equals("Event")){
            ll_instansi.setVisibility(View.VISIBLE);
            ll_jumlah.setVisibility(View.VISIBLE);
            txtInstansi.setText(detailKegiatan.getInstansi_terkait());
            txtJumlah.setText(detailKegiatan.getJml_peserta());
        }   else if(detailKegiatan.getKegiatan_type().equals("Pelatihan")){
            ll_jumlah.setVisibility(View.VISIBLE);
            txtJumlah.setText(detailKegiatan.getJml_peserta());
        }
        if(!detailKegiatan.getKegiatan_lat().isEmpty()&&!detailKegiatan.getKegiatan_lng().isEmpty()){
            latlong = new LatLng(Double.valueOf(detailKegiatan.getKegiatan_lat()),Double.valueOf(detailKegiatan.getKegiatan_lng()));
        }
         mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                googleMap.addMarker(new MarkerOptions().position(latlong).title(detailKegiatan.getKegiatan_name()).snippet(detailKegiatan.getKegiatan_desc()));
                CameraPosition cameraPosition = new CameraPosition.Builder().target(latlong).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                googleMap.getUiSettings().setScrollGesturesEnabled(false);
            }
        });
        if(detailKegiatan.getPhotos().size()>0) {
            layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            ShowImageAdapter.ImageListListener l = new ShowImageAdapter.ImageListListener() {
                @Override
                public void onImageListSelected(List<KegiatanPhoto> kegiatanPhotos) {

                    FragmentManager fragmentManager = getFragmentManager();
                    ViewImageFragment newFragment = new ViewImageFragment();
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Bundle bundle = new Bundle();
                    //bundle.putString("url", image.getKegiatan_photo());
                    bundle.putString("nama", detailKegiatan.getKegiatan_name());
                    bundle.putSerializable("data",(Serializable) kegiatanPhotos);
                    newFragment.setArguments(bundle);
                    newFragment.show(ft, newFragment.getTag());
                }
            };
            adapter = new ShowImageAdapter(getActivity(), detailKegiatan.getPhotos(), l);
            recyclerView.setAdapter(adapter);
            txtNoImage.setVisibility(View.GONE);
            imgNoImages.setVisibility(View.GONE);

        } else {
            txtNoImage.setVisibility(View.VISIBLE);
            imgNoImages.setVisibility(View.VISIBLE);
        }
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
