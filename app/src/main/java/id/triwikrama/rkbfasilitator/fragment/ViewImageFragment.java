package id.triwikrama.rkbfasilitator.fragment;

import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.jsibbold.zoomage.ZoomageView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import id.triwikrama.rkbfasilitator.R;
import id.triwikrama.rkbfasilitator.adapter.SliderAdapter;
import id.triwikrama.rkbfasilitator.model.Kegiatan;
import id.triwikrama.rkbfasilitator.model.KegiatanPhoto;
import id.triwikrama.rkbfasilitator.utils.CustomDialog;

import static id.triwikrama.rkbfasilitator.utils.Apps.getInstance;


public class ViewImageFragment extends DialogFragment {

    @BindView(R.id.viewPager)
    ViewPager pager;
    List<Fragment> fragments;
    SliderAdapter adapter;
    private CustomDialog cd;
    private View root_view;
    private TextView txt_judul, txt_tanggal, txt_isi;
    private ZoomageView zoomageView;
    private Unbinder unbind;
    private List<KegiatanPhoto> photoList;
    private String namaKegiatan;

    @OnClick(R.id.bt_close)
    void close() {
        dismiss();
    }

    @OnClick(R.id.bt_download)
    void download() {
        for (int i = 0; i < photoList.size(); i++) {
            progree(getInstance().urlFixer(photoList.get(i).getKegiatan_photo()), i+1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.dialog_view_image, container, false);
        unbind = ButterKnife.bind(this, root_view);
        namaKegiatan = getArguments().getString("nama");
        photoList = (List<KegiatanPhoto>) getArguments().getSerializable("data");
        adapter = new SliderAdapter(getActivity(), photoList);
        pager.setAdapter(adapter);

        txt_judul = (TextView) root_view.findViewById(R.id.txt_judul);
        txt_judul.setVisibility(View.GONE);

        root_view.setFocusableInTouchMode(true);
        root_view.requestFocus();
        root_view.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    dismiss();
                    return true;
                }
                return false;
            }
        });

        return root_view;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cd = new CustomDialog(getActivity());
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setLayout(width, height);
    }

    public void onBackPressed() {
        dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbind.unbind();
    }

    private void downloadImage(String url) {
        cd.show_p_Dialog();
        DownloadManager downloadmanager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);

        Log.e("wkwkwkww", url);
        //namaKegiatan = namaKegiatan.replace(" ","_");
        Uri uri = Uri.parse(url);
        String fileName = url.substring(url.lastIndexOf('/') + 1);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setTitle("Download Foto " + namaKegiatan);
        request.setDescription("Downloading");
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setVisibleInDownloadsUi(false);
        request.setAllowedOverMetered(true);

        String folder = Environment.getExternalStorageDirectory() + File.separator + "Pictures" + File.separator + "RKB" + File.separator + namaKegiatan + File.separator;

        //Create androiddeft folder if it does not exist
        File directory = new File(folder);
        if (!directory.exists()) {
            directory.mkdirs();
        }


        request.setDestinationUri(Uri.parse("file://" + folder + fileName));

        downloadmanager.enqueue(request);

    }

    private void progree(String url, int size) {
        final ProgressDialog progressBarDialog = new ProgressDialog(getActivity());
        progressBarDialog.setTitle("Unduh Foto: "+ size);

        progressBarDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressBarDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                cd.toastSuccess("Unduh foto berhasil");
            }
        });
        progressBarDialog.setCanceledOnTouchOutside(false);
        progressBarDialog.setProgress(0);

        DownloadManager downloadmanager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);

        Log.e("wkwkwkww", url);
        //namaKegiatan = namaKegiatan.replace(" ","_");
        Uri uri = Uri.parse(url);
        String fileName = url.substring(url.lastIndexOf('/') + 1);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setTitle("Download Foto " + namaKegiatan);
        request.setDescription("Downloading");
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setVisibleInDownloadsUi(false);
        request.setAllowedOverMetered(true);

        String folder = Environment.getExternalStorageDirectory() + File.separator + "Pictures" + File.separator + "RKB" + File.separator + namaKegiatan + File.separator;

        //Create androiddeft folder if it does not exist
        File directory = new File(folder);
        if (!directory.exists()) {
            directory.mkdirs();
        }

        File file = new File(folder + fileName);
        if (file.exists()) {
            file.delete();
        }

        request.setDestinationUri(Uri.parse("file://" + folder + fileName));

        final long downloadId = downloadmanager.enqueue(request);
        new Thread(new Runnable() {

            @Override
            public void run() {

                boolean downloading = true;

                DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                while (downloading) {

                    DownloadManager.Query q = new DownloadManager.Query();
                    q.setFilterById(downloadId); //filter by id which you have receieved when reqesting download from download manager
                    Cursor cursor = manager.query(q);
                    cursor.moveToFirst();
                    int bytes_downloaded = cursor.getInt(cursor
                            .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                    int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));

                    if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                        downloading = false;
                    }

                    final int dl_progress = (int) ((bytes_downloaded * 100l) / bytes_total);

                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            progressBarDialog.setProgress((int) dl_progress);
                            if (dl_progress==100){
                                progressBarDialog.dismiss();
                            }
                        }
                    });

                    cursor.close();
                }

            }

        }).start();

        progressBarDialog.show();

    }

}